################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/modules/Includes.cpp \
../src/modules/Top.cpp \
../src/modules/TopUC.cpp 

OBJS += \
./src/modules/Includes.o \
./src/modules/Top.o \
./src/modules/TopUC.o 

CPP_DEPS += \
./src/modules/Includes.d \
./src/modules/Top.d \
./src/modules/TopUC.d 


# Each subdirectory must supply rules for building sources it contributes
src/modules/%.o: ../src/modules/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I"/usr/local/systemc-2.3.1a/include" -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


