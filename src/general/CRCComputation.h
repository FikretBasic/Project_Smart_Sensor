/*
 * CRCComputionation.h
 *
 *  Created on: 7 Jun 2018
 *      Author: its2016
 */

#ifndef GENERAL_CRCCOMPUTATION_H_
#define GENERAL_CRCCOMPUTATION_H_

#include <stdlib.h>
#include <stdint.h>
#include <vector>

namespace crc
{

#define CRC_NAME			"CRC-CCITT"
#define POLYNOMIAL			0x1021
#define INITIAL_REMAINDER	0xFFFF
#define FINAL_XOR_VALUE		0x0000
#define CHECK_VALUE			0x29B1

#define WIDTH    (8 * sizeof(unsigned short))
#define TOPBIT   (1 << (WIDTH - 1))

// TODO: make this class singleton

class CRC_Computionation {
public:
	CRC_Computionation();
	virtual ~CRC_Computionation();

	template <class vectorVal>
	uint16_t calculateCRC(std::vector<vectorVal> message)
	{
	    unsigned short remainder = INITIAL_REMAINDER;
	    unsigned char  data;

	    /*
	     * Divide the message by the polynomial, a byte at a time.
	     */
	    for (size_t i = 0; i < message.size(); ++i)
	    {
	        data = message.at(i) ^ (remainder >> (WIDTH - 8));
	  		remainder = crcTable[data] ^ (remainder << 8);
	    }

	    /*
	     * The final remainder is the CRC.
	     */
	    return (remainder ^ FINAL_XOR_VALUE);
	}

private:
	void initializeCRC();
	unsigned short  crcTable[256];
};

};

#endif /* GENERAL_CRCCOMPUTATION_H_ */
