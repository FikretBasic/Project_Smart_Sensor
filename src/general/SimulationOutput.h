/*
 * SimulationOutput.h
 *
 *  Created on: 21 Oct 2018
 *      Author: its2016
 */

#ifndef GENERAL_SIMULATIONOUTPUT_H_
#define GENERAL_SIMULATIONOUTPUT_H_

#include "systemc.h"
#include "string"
#include <vector>
#include <queue>
#include <fstream>

enum SimStatus {SIM_INFO, SIM_WARNING, SIM_ERROR};

class SimulationOutput {
public:
	virtual ~SimulationOutput();
	static SimulationOutput* instance();

	// Print functions
	void printSimStatus(std::string main_text, sc_core::sc_time timestamp, SimStatus status);
	void printUCStatus(std::string main_text, sc_core::sc_time timestamp);
	void printFramWrite(uint8_t value, int addr, sc_core::sc_time timestamp);

	// for handling power and energy log, as well as SystemC trace
	void setSimOutputFile(short program, short instance = 0, short testbench = 0);
	double setToPowerArray(double power);
	void writeToSimPowerFile(sc_core::sc_time timestamp, double power);
	sc_trace_file* getTraceFile();

private:
	SimulationOutput();
	static SimulationOutput *instance_t;

	// SystemC trace
	sc_trace_file *fp;

	// for handling power and energy log
	std::ofstream simPowerFile;
	std::vector<double> powerArray;
	double calcPowerArrayAverage();
};



#endif /* GENERAL_SIMULATIONOUTPUT_H_ */
