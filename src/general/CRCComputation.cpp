/*
 * CRCComputionation.cpp
 *
 *  Created on: 7 Jun 2018
 *      Author: its2016
 */

#include "CRCComputation.h"

namespace crc
{

CRC_Computionation::CRC_Computionation() {
	initializeCRC();
}

CRC_Computionation::~CRC_Computionation() {}

void CRC_Computionation::initializeCRC()
{
	unsigned short remainder;
	int			   dividend;
	unsigned char  bit;

    /*
     * Compute the remainder of each possible dividend.
     */
    for (dividend = 0; dividend < 256; ++dividend)
    {
        /*
         * Start with the dividend followed by zeros.
         */
        remainder = dividend << (WIDTH - 8);

        /*
         * Perform modulo-2 division, a bit at a time.
         */
        for (bit = 8; bit > 0; --bit)
        {
            /*
             * Try to divide the current data bit.
             */
            if (remainder & TOPBIT)
            {
                remainder = (remainder << 1) ^ POLYNOMIAL;
            }
            else
            {
                remainder = (remainder << 1);
            }
        }

        /*
         * Store the result into the table.
         */
        crcTable[dividend] = remainder;
    }
}

};

