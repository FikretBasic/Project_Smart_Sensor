/*
 * Configuration.cpp
 *
 *  Created on: 7 Jul 2018
 *      Author: its2016
 */

#include "Configuration.h"

Configuration* Configuration::instance_t = 0;
double temp_curr10Harvest[] = {0.1, 0.3, 0.7, 0.9, 1.1, 1.3, 1.5, 2.1, 3.3, 4.5, 4.9};
double temp_curr100Harvest[] = {0.7, 0.7, 0.7, 0.7, 0.9, 0.9, 1.3, 1.7, 1.9, 2.3, 2.7};


Configuration::Configuration()
{
	nfcPowerConsumption = new double[pss::LAST_NFC];
	secPowerConsumption = new double[pss::LAST_SEC];
	framPowerConsumption = new double[pss::LAST_FRAM];
	ucPowerConsumption = new double[pss::LAST_UC];

	act_test_benches = new short[3];

	// <------- setting the constant current harvesting for modul. 10% and 100% ------->
	this->setCurrHarvest();

	// <------- Reading from the Config file ------->

	std::string lin,text;
	std::ifstream in("./config.cfg");

	if(in.is_open())
	{
		while(std::getline(in, lin))
		{
			text += lin + "\n";
		}
	}
	else
	{
		cout << "The config file is not open!" << endl << endl;
	}
	const char* data = text.c_str();

	std::istringstream is_file(data);
	std::string line;

	while( std::getline(is_file, line) )
	{
	  std::istringstream is_line(line);
	  std::string key;

	  if( std::getline(is_line, key, '=') )
	  {
	    std::string value;
	    if( std::getline(is_line, value) )
	    {
	    	assign_value(key, value);
	    }
	  }
	}

	in.close();
}

void Configuration::setCurrHarvest()
{
	curr10Harvest = new double[11];
	curr100Harvest = new double[11];

	// the values are given in mA
	for(int i = 0; i < 11; i++)
	{
		curr10Harvest[i] = -temp_curr10Harvest[i];
		curr100Harvest[i] = -temp_curr100Harvest[i];
	}
}


Configuration::~Configuration()
{
	delete instance_t;
	delete[] curr10Harvest;
	delete[] curr100Harvest;
	delete[] act_test_benches;

	delete[] framPowerConsumption;
	delete[] secPowerConsumption;
	delete[] nfcPowerConsumption;
	delete[] ucPowerConsumption;
}

Configuration* Configuration::instance()
{
    if (!instance_t)
    	instance_t = new Configuration();
    return instance_t;
}

void Configuration::assign_value(std::string key, std::string value)
{
	// input voltage is written in Volts
	// all current consumption is written in miliamperes

	if(!assign_sim_app(key,value) && !assign_trace_param(key,value) && !assign_input_param(key,value))
	{
		// general input VCC
		if(key.compare("VCC") == 0)
		{
			double voltVal = atof(value.c_str());

			if(voltVal < 1.8 || voltVal > 5.5)
				voltVal = 3.3;

			this->VCC = voltVal;
		}
		// FRAM current consumption
		else if(key.compare("clock_fram") == 0)
		{
			this->clock_fram = atof(value.c_str());
		}
		else if(key.compare("sleep_fram") == 0)
		{
			this->framPowerConsumption[pss::sleep_fram] = atof(value.c_str());
		}
		else if(key.compare("idle_fram") == 0)
		{
			this->framPowerConsumption[pss::idle_fram] = atof(value.c_str());
		}
		else if(key.compare("op_standard_fram") == 0)
		{
			this->framPowerConsumption[pss::op_standard_fram] = atof(value.c_str());
		}
		else if(key.compare("op_fast_fram") == 0)
		{
			this->framPowerConsumption[pss::op_fast_fram] = atof(value.c_str());
		}
		else if(key.compare("op_high_fram") == 0)
		{
			this->framPowerConsumption[pss::op_high_fram] = atof(value.c_str());
		}
		// NFC I2C current consumption
		else if(key.compare("clock_nfc") == 0)
		{
			this->clock_nfc = atof(value.c_str());
		}
		else if(key.compare("idle_i2c") == 0)
		{
			this->nfcPowerConsumption[pss::idle_i2c] = atof(value.c_str());
		}
		else if(key.compare("active_e2_read_i2c") == 0)
		{
			this->nfcPowerConsumption[pss::active_e2_read_i2c] = atof(value.c_str());
		}
		else if(key.compare("active_mb_read_i2c") == 0)
		{
			this->nfcPowerConsumption[pss::active_mb_read_i2c] = atof(value.c_str());
		}
		else if(key.compare("active_e2_write_i2c") == 0)
		{
			this->nfcPowerConsumption[pss::active_e2_write_i2c] = atof(value.c_str());
		}
		else if(key.compare("active_mb_write_i2c") == 0)
		{
			this->nfcPowerConsumption[pss::active_mb_write_i2c] = atof(value.c_str());
		}
		// NFC RF current consumption
		else if(key.compare("idle_rf") == 0)
		{
			this->nfcPowerConsumption[pss::idle_rf] = atof(value.c_str());
		}
		else if(key.compare("active_rf") == 0)
		{
			this->nfcPowerConsumption[pss::active_rf] = atof(value.c_str());
		}
		// NFC Energy Harvesting
		else if(key.compare("nfc_H") == 0)
		{
			this->nfc_H = atof(value.c_str());
		}
		else if(key.compare("nfc_modulation") == 0)
		{
			// Setting up the modulation mode, only 10 or 100 are valid values
			if((value.compare("100") == 0) || (value.compare("10") == 0))
			{
				this->nfcMod = atoi(value.c_str());
			}
			else
			{
				this->nfcMod = 100;
			}
		}
		// SECURITY CHIP Consumption
		else if(key.compare("clock_sec") == 0)
		{
			this->clock_sec = atof(value.c_str());
		}
		else if(key.compare("active_sec") == 0)
		{
			this->secPowerConsumption[pss::active_sec] = atof(value.c_str());
		}
		else if(key.compare("idle_sec") == 0)
		{
			this->secPowerConsumption[pss::idle_sec] = atof(value.c_str());
		}
		else if(key.compare("sleep_sec") == 0)
		{
			this->secPowerConsumption[pss::sleep_sec] = atof(value.c_str());
		}
		// MICROCONTROLLER Consumption
		else if(key.compare("clock_uc") == 0)
		{
			this->clock_uc = atof(value.c_str());
		}
		else if(key.compare("active_uc") == 0)
		{
			this->ucPowerConsumption[pss::active_uc] = atof(value.c_str());
			//this->ucPowerConsumption[pss::active_uc] = calculateCurrentUC(atof(value.c_str())*this->clock_uc, pss::active_uc);
		}
		else if(key.compare("sleep_uc") == 0)
		{
			this->ucPowerConsumption[pss::sleep_uc] = atof(value.c_str());
			//this->ucPowerConsumption[pss::sleep_uc] = calculateCurrentUC(atof(value.c_str()), pss::sleep_uc);
		}
		else if(key.compare("deep_sleep_uc") == 0)
		{
			this->ucPowerConsumption[pss::deep_sleep_uc] = atof(value.c_str());
			//this->ucPowerConsumption[pss::deep_sleep_uc] = calculateCurrentUC(atof(value.c_str()), pss::deep_sleep_uc);
		}
		// Determine which microcontroller program is currently run on the sim-time
		else if(key.compare("uc_program") == 0)
		{
			if(atoi(value.c_str()) == 0 || atoi(value.c_str()) == 1)
				this->uc_program = atoi(value.c_str());
			else
				this->uc_program = 0;
		}
		else if(key.compare("security_function") == 0)
		{
			if(atoi(value.c_str()) < 1 || atoi(value.c_str()) > 3)
				this->security_function = atoi(value.c_str());
			else
				this->security_function = 1;
		}
	}
}

bool Configuration::assign_sim_app(std::string key, std::string value)
{
	if(key.compare("simulation_type") == 0)
	{
		simulation_type = atoi(value.c_str());
		if(simulation_type < 1 || simulation_type > 2)
		{
			cout << "[ERROR] Wrong application number for the simulation type!" << endl;
			cout << "[INFO] Setting by default simulation number 2" << endl;
			simulation_type = 2;
		}
		return true;
	}
	else if(key.compare("fram_test_case") == 0)
	{
		act_test_benches[0] = atoi(value.c_str());
		if(act_test_benches[0] > 6 || act_test_benches[0] < 0)
		{
			cout << "[ERROR] Wrong simulation test number for the FRAM test!" << endl;
			cout << "[INFO] Setting by default test number 1" << endl;
			act_test_benches[0] = 1;
		}
		return true;
	}
	else if(key.compare("nfc_test_case") == 0)
	{
		act_test_benches[1] = atoi(value.c_str());
		if(act_test_benches[1] > 17 || act_test_benches[1] < 0)
		{
			cout << "[ERROR] Wrong simulation test number for the NFC test!" << endl;
			cout << "[INFO] Setting by default test number 1" << endl;
			act_test_benches[1] = 1;
		}
		return true;
	}
	else if(key.compare("sec_test_case") == 0)
	{
		act_test_benches[2] = atoi(value.c_str());
		if(act_test_benches[2] > 9 || act_test_benches[2] < 0)
		{
			cout << "[ERROR] Wrong simulation test number for the Security test!" << endl;
			cout << "[INFO] Setting by default test number 1" << endl;
			act_test_benches[2] = 1;
		}
		return true;
	}

	return false;
}

bool Configuration::assign_input_param(std::string key, std::string value)
{
	if(key.compare("encrypt_input") == 0)
	{
		encrypt_input = atoi(value.c_str());
		return true;
	}
	else if(key.compare("aes_key") == 0)
	{
		std::queue<std::string> temp_queue = splitString(value, ",");
		while(!temp_queue.empty())
		{
			aes_key.push_back(atoi(temp_queue.front().c_str()));
			temp_queue.pop();
		}
		return true;
	}
	else if(key.compare("program_input") == 0)
	{
		std::queue<std::string> temp_queue = splitString(value, ",");
		while(!temp_queue.empty())
		{
			program_input.push_back(atoi(temp_queue.front().c_str()));
			temp_queue.pop();
		}
		return true;
	}

	return false;
}

bool Configuration::assign_trace_param(std::string key, std::string value)
{
	if(key.compare("test_bench_trace") == 0)
	{
		test_bench_trace = splitString(value, ",");
		return true;
	}
	else if(key.compare("uc_program_trace") == 0)
	{
		uc_program_trace = splitString(value, ",");
		return true;
	}

	return false;
}

std::queue<std::string> Configuration::splitString(const std::string& str, const std::string& delim)
{
	std::queue<std::string> tokens;
    size_t prev = 0, pos = 0;
    do
    {
        pos = str.find(delim, prev);
        if (pos == std::string::npos) pos = str.length();
        std::string token = str.substr(prev, pos-prev);
        if (!token.empty()) tokens.push(token);
        prev = pos + delim.length();
    } while (pos < str.length() && prev < str.length());
    return tokens;
}

double Configuration::getVCC()
{
	return this->VCC;
}

void Configuration::setVCC(double VCC)
{
	this->VCC = VCC;
}

double Configuration::getFramPowerConsumption(pss::EnergyStates_FRAM state)
{
	if(state == pss::LAST_FRAM)
	{
		return 0;
	}

	return framPowerConsumption[state];
}

double Configuration::getNFCPowerConsumption(pss::EnergyStates_NFC state)
{
	if(state == pss::LAST_NFC)
	{
		return 0;
	}

	return nfcPowerConsumption[state];
}

double Configuration::getNFCPowerHarvesting()
{
	int pos = (int)(this->nfc_H*2);
	if(pos > 11 || pos < 1)
	{
		pos = 11;
	}

	if(this->nfcMod == 10)
		return this->curr10Harvest[pos-1];
	else
		return this->curr100Harvest[pos-1];
}

double Configuration::getSecPowerConsumption(pss::EnergyStates_Security state)
{
	if(state == pss::LAST_SEC)
	{
		return 0;
	}

	return this->secPowerConsumption[state];
}

double Configuration::getUCPowerConsumption(pss::EnergyStates_MicroController state)
{
	if(state == pss::LAST_UC)
	{
		return 0;
	}

	return calculateCurrentUC(this->ucPowerConsumption[state], state);
}

double Configuration::calculateCurrentUC(double input_val, pss::EnergyStates_MicroController type)
{
	if (type == pss::active_uc)
		return ((3.3 / this->VCC) * input_val * this->clock_uc) - (1.31 * (3.3 - this->VCC));
	else if (type == pss::sleep_uc)
		return ((3.3 / this->VCC) * input_val) - (0.0388 * (3.3 - this->VCC));
	else if (type == pss::deep_sleep_uc)
		return ((3.3 / this->VCC) * input_val) - (0.00402 * (3.3 - this->VCC));
	else
		return 0;
}

short Configuration::getSimulation_type()
{
	return this->simulation_type;
}

short* Configuration::getAct_test_benches()
{
	return this->act_test_benches;
}

bool Configuration::getEncrypt_input()
{
	return this->encrypt_input;
}

std::vector<uint8_t> Configuration::getAes_key()
{
	return this->aes_key;
}

std::vector<uint8_t> Configuration::getProgram_input()
{
	return this->program_input;
}

std::queue<std::string> Configuration::getTest_bench_trace()
{
	return this->test_bench_trace;
}
std::queue<std::string> Configuration::getUC_program_trace()
{
	return this->uc_program_trace;
}

float Configuration::getClockUC()
{
	return this->clock_uc;
}

int Configuration::getUCPRogram()
{
	return this->uc_program;
}

int Configuration::getSecFunction()
{
	return this->security_function;
}
