/*
 * SimulationOutput.cpp
 *
 *  Created on: 21 Oct 2018
 *      Author: its2016
 */

#include "SimulationOutput.h"

SimulationOutput* SimulationOutput::instance_t = 0;

SimulationOutput::SimulationOutput()
{
	fp = NULL;
}

SimulationOutput::~SimulationOutput()
{
	this->simPowerFile.close();
	sc_close_vcd_trace_file(fp);
	delete this->instance_t;
}

SimulationOutput* SimulationOutput::instance()
{
	if (!instance_t)
		instance_t = new SimulationOutput();
	return instance_t;
}

void SimulationOutput::printSimStatus(std::string main_text, sc_core::sc_time timestamp, SimStatus status)
{
	if(status == SIM_INFO)
		cout << "[INFO][" << timestamp << "] " << main_text << endl;
	else if(status == SIM_WARNING)
		cout << "[WARNING][" << timestamp << "] " << main_text << endl;
	else if(status == SIM_ERROR)
		cout << "[ERROR][" << timestamp << "] " << main_text << endl;
}

void SimulationOutput::printUCStatus(std::string main_text, sc_core::sc_time timestamp)
{
	cout << "[UC][" << timestamp << "] " << main_text << endl;
}

void SimulationOutput::printFramWrite(uint8_t value, int addr, sc_core::sc_time timestamp)
{
	cout << "[FRAM][" << timestamp << "] Written in FRAM: " << hex << (int)value << "[" << addr << "]" << endl;
}

void SimulationOutput::writeToSimPowerFile(sc_core::sc_time timestamp, double power)
{
	this->simPowerFile << timestamp << " , " << power << " mW\n";
}

void SimulationOutput::setSimOutputFile(short program, short instance, short testbench)
{
	if(program == 1)
	{
		if(testbench == 1)
		{
			switch(instance)
			{
				case 1:
					this->simPowerFile.open("sim_output/test_benches/fram_outputs/testVarWrite1");
					fp = sc_create_vcd_trace_file("sim_output/test_benches/fram_outputs/testVarWrite1");
					break;
				case 2:
					this->simPowerFile.open("sim_output/test_benches/fram_outputs/testVarWrite2");
					fp = sc_create_vcd_trace_file("sim_output/test_benches/fram_outputs/testVarWrite2");
					break;
				case 3:
					this->simPowerFile.open("sim_output/test_benches/fram_outputs/testVarWriteFail");
					fp = sc_create_vcd_trace_file("sim_output/test_benches/fram_outputs/testVarWriteFail");
					break;
				case 4:
					this->simPowerFile.open("sim_output/test_benches/fram_outputs/testVarReadPass");
					fp = sc_create_vcd_trace_file("sim_output/test_benches/fram_outputs/testVarReadPass");
					break;
				case 5:
					this->simPowerFile.open("sim_output/test_benches/fram_outputs/testVarSleepMode");
					fp = sc_create_vcd_trace_file("sim_output/test_benches/fram_outputs/testVarSleepMode");
					break;
				case 6:
					this->simPowerFile.open("sim_output/test_benches/fram_outputs/testVarProgressive");
					fp = sc_create_vcd_trace_file("sim_output/test_benches/fram_outputs/testVarProgressive");
					break;
				default:
					this->simPowerFile.open("sim_output/test_benches/fram_outputs/testVarWrite1");
					fp = sc_create_vcd_trace_file("sim_output/test_benches/fram_outputs/testVarWrite1");
			}
		}
		else if(testbench == 2)
		{
			switch(instance)
			{
				case 1:
					this->simPowerFile.open("sim_output/test_benches/nfc_outputs/testVarWrite1");
					fp = sc_create_vcd_trace_file("sim_output/test_benches/nfc_outputs/testVarWrite1");
					break;
				case 2:
					this->simPowerFile.open("sim_output/test_benches/nfc_outputs/testVarWrite2");
					fp = sc_create_vcd_trace_file("sim_output/test_benches/nfc_outputs/testVarWrite2");
					break;
				case 3:
					this->simPowerFile.open("sim_output/test_benches/nfc_outputs/testVarWriteRead1");
					fp = sc_create_vcd_trace_file("sim_output/test_benches/nfc_outputs/testVarWriteRead1");
					break;
				case 4:
					this->simPowerFile.open("sim_output/test_benches/nfc_outputs/testVarWriteRead2");
					fp = sc_create_vcd_trace_file("sim_output/test_benches/nfc_outputs/testVarWriteRead2");
					break;
				case 5:
					this->simPowerFile.open("sim_output/test_benches/nfc_outputs/testVarSystem");
					fp = sc_create_vcd_trace_file("sim_output/test_benches/nfc_outputs/testVarSystem");
					break;
				case 6:
					this->simPowerFile.open("sim_output/test_benches/nfc_outputs/testVarDynamic");
					fp = sc_create_vcd_trace_file("sim_output/test_benches/nfc_outputs/testVarDynamic");
					break;
				case 7:
					this->simPowerFile.open("sim_output/test_benches/nfc_outputs/testVarRFInventory");
					fp = sc_create_vcd_trace_file("sim_output/test_benches/nfc_outputs/testVarRFInventory");
					break;
				case 8:
					this->simPowerFile.open("sim_output/test_benches/nfc_outputs/testVarRFReadBlock");
					fp = sc_create_vcd_trace_file("sim_output/test_benches/nfc_outputs/testVarRFReadBlock");
					break;
				case 9:
					this->simPowerFile.open("sim_output/test_benches/nfc_outputs/testVarRFWriteBlock");
					fp = sc_create_vcd_trace_file("sim_output/test_benches/nfc_outputs/testVarRFWriteBlock");
					break;
				case 10:
					this->simPowerFile.open("sim_output/test_benches/nfc_outputs/testVarRFReadMultiple");
					fp = sc_create_vcd_trace_file("sim_output/test_benches/nfc_outputs/testVarRFReadMultiple");
					break;
				case 11:
					this->simPowerFile.open("sim_output/test_benches/nfc_outputs/testVarRFWriteMultiple");
					fp = sc_create_vcd_trace_file("sim_output/test_benches/nfc_outputs/testVarRFWriteMultiple");
					break;
				case 12:
					this->simPowerFile.open("sim_output/test_benches/nfc_outputs/testVarRFWriteConfig");
					fp = sc_create_vcd_trace_file("sim_output/test_benches/nfc_outputs/testVarRFWriteConfig");
					break;
				case 13:
					this->simPowerFile.open("sim_output/test_benches/nfc_outputs/testVarRFWriteDynamic");
					fp = sc_create_vcd_trace_file("sim_output/test_benches/nfc_outputs/testVarRFWriteDynamic");
					break;
				case 14:
					this->simPowerFile.open("sim_output/test_benches/nfc_outputs/testVarRFWriteMessage");
					fp = sc_create_vcd_trace_file("sim_output/test_benches/nfc_outputs/testVarRFWriteMessage");
					break;
				case 15:
					this->simPowerFile.open("sim_output/test_benches/nfc_outputs/testVarMailBox1");
					fp = sc_create_vcd_trace_file("sim_output/test_benches/nfc_outputs/testVarMailBox1");
					break;
				case 16:
					this->simPowerFile.open("sim_output/test_benches/nfc_outputs/testVarMailBox2");
					fp = sc_create_vcd_trace_file("sim_output/test_benches/nfc_outputs/testVarMailBox2");
					break;
				case 17:
					this->simPowerFile.open("sim_output/test_benches/nfc_outputs/testVarHarvesting");
					fp = sc_create_vcd_trace_file("sim_output/test_benches/nfc_outputs/testVarHarvesting");
					break;
				default:
					this->simPowerFile.open("sim_output/test_benches/nfc_outputs/testVarWrite1");
					fp = sc_create_vcd_trace_file("sim_output/test_benches/nfc_outputs/testVarWrite1");
			}
		}
		else if(testbench == 3)
		{
			switch(instance)
			{
				case 1:
					this->simPowerFile.open("sim_output/test_benches/security_outputs/testVarWrite1");
					fp = sc_create_vcd_trace_file("sim_output/test_benches/security_outputs/testVarWrite1");
					break;
				case 2:
					this->simPowerFile.open("sim_output/test_benches/security_outputs/testVarWrite2");
					fp = sc_create_vcd_trace_file("sim_output/test_benches/security_outputs/testVarWrite2");
					break;
				case 3:
					this->simPowerFile.open("sim_output/test_benches/security_outputs/testVarWriteRead1");
					fp = sc_create_vcd_trace_file("sim_output/test_benches/security_outputs/testVarWriteRead1");
					break;
				case 4:
					this->simPowerFile.open("sim_output/test_benches/security_outputs/testVarWriteRead2");
					fp = sc_create_vcd_trace_file("sim_output/test_benches/security_outputs/testVarWriteRead2");
					break;
				case 5:
					this->simPowerFile.open("sim_output/test_benches/security_outputs/testVarSystem1");
					fp = sc_create_vcd_trace_file("sim_output/test_benches/security_outputs/testVarSystem1");
					break;
				case 6:
					this->simPowerFile.open("sim_output/test_benches/security_outputs/testVarSystem2");
					fp = sc_create_vcd_trace_file("sim_output/test_benches/security_outputs/testVarSystem2");
					break;
				case 7:
					this->simPowerFile.open("sim_output/test_benches/security_outputs/testVarSecurity1");
					fp = sc_create_vcd_trace_file("sim_output/test_benches/security_outputs/testVarSecurity1");
					break;
				case 8:
					this->simPowerFile.open("sim_output/test_benches/security_outputs/testVarSecurity2");
					fp = sc_create_vcd_trace_file("sim_output/test_benches/security_outputs/testVarSecurity2");
					break;
				case 9:
					this->simPowerFile.open("sim_output/test_benches/security_outputs/testVarSecurity3");
					fp = sc_create_vcd_trace_file("sim_output/test_benches/security_outputs/testVarSecurity3");
					break;
				default:
					this->simPowerFile.open("sim_output/test_benches/security_outputs/testVarWrite1");
					fp = sc_create_vcd_trace_file("sim_output/test_benches/security_outputs/testVarWrite1");
			}
		}
	}
	else if(program == 2)
	{
		if (instance == 0)
		{
			this->simPowerFile.open("sim_output/microcontroller_programs/simulation_app1_power");
			fp = sc_create_vcd_trace_file("sim_output/microcontroller_programs/simulation_app1_power");
		}
		else if (instance == 1)
		{
			this->simPowerFile.open("sim_output/microcontroller_programs/simulation_app2_power");
			fp = sc_create_vcd_trace_file("sim_output/microcontroller_programs/simulation_app2_power");
		}
	}
}

double SimulationOutput::setToPowerArray(double power)
{
	this->powerArray.push_back(power);

	// it has enough points for 1us
	if(this->powerArray.size() == 1000)
	{
		double avgPowerArray = calcPowerArrayAverage();
		this->powerArray.clear();
		return avgPowerArray;
	}
	else
	{
		return 0;
	}
}

double SimulationOutput::calcPowerArrayAverage()
{
	double avgPowerArray = 0;
	for (size_t i = 0; i < this->powerArray.size(); i++)
	{
		avgPowerArray += this->powerArray.at(i);
	}
	return avgPowerArray / 1000;
}

sc_trace_file* SimulationOutput::getTraceFile()
{
	return this->fp;
}
