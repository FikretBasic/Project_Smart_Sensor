/*
 * Configuration.h
 *
 *  Created on: 7 Jul 2018
 *      Author: its2016
 */

#ifndef GENERAL_CONFIGURATION_H_
#define GENERAL_CONFIGURATION_H_

#include "systemc.h"
#include "fstream"
#include <stdlib.h>
#include <queue>

namespace pss {

enum EnergyStates {sleep, stand_by, operation};
enum OperativeStates {start, dev_addr, mem_addr, rd_wr, stop};

enum EnergyStates_FRAM {sleep_fram, idle_fram, op_high_fram, op_fast_fram, op_standard_fram, LAST_FRAM};

enum EnergyStates_NFC {idle_i2c, active_e2_read_i2c, active_mb_read_i2c, active_e2_write_i2c,
						active_mb_write_i2c, idle_rf, active_rf, LAST_NFC};

enum EnergyStates_Security {active_sec, idle_sec, sleep_sec, LAST_SEC};

enum EnergyStates_MicroController {active_uc, sleep_uc, deep_sleep_uc, LAST_UC};

enum RFSendState {start_rf, operation_rf, stop_rf};

}


namespace sec {

enum authScheme {auth1, auth2, auth3};

}

namespace testb {

enum OperativeStates {start, dev_addr, mem_addr, rd_wr, stop};
enum RFSendState {start_rf, operation_rf, stop_rf};

}

class Configuration {
public:

	virtual ~Configuration();
	static Configuration* instance();

	double getVCC();
	void setVCC(double VCC);
	double getFramPowerConsumption(pss::EnergyStates_FRAM state);
	double getNFCPowerConsumption(pss::EnergyStates_NFC state);
	double getNFCPowerHarvesting();
	double getSecPowerConsumption(pss::EnergyStates_Security state);
	double getUCPowerConsumption(pss::EnergyStates_MicroController state);

	short getSimulation_type();
	short* getAct_test_benches();
	bool getEncrypt_input();
	std::vector<uint8_t> getAes_key();
	std::vector<uint8_t> getProgram_input();
	std::queue<std::string> getTest_bench_trace();
	std::queue<std::string> getUC_program_trace();
	float getClockUC();
	int getUCPRogram();
	int getSecFunction();

	std::queue<std::string> splitString(const std::string& str, const std::string& delim);

private:
	Configuration();
	static Configuration *instance_t;

	void assign_value(std::string key, std::string value);
	bool assign_sim_app(std::string key, std::string value);
	bool assign_input_param(std::string key, std::string value);
	bool assign_trace_param(std::string key, std::string value);

	// simulation program parameters
	short simulation_type;
	short *act_test_benches;

	// program input and AES encryption/decryption parameters
	bool encrypt_input;
	std::vector<uint8_t> aes_key;
	std::vector<uint8_t> program_input;

	// trace parameters
	std::queue<std::string> test_bench_trace;
	std::queue<std::string> uc_program_trace;

	// general input voltage
	double VCC;

	// fram current consumption
	double *framPowerConsumption;
	float clock_fram;

	// NFC current consumption
	double *nfcPowerConsumption;
	float clock_nfc;

	// nfc modulation
	int nfcMod;

	// nfc energy harvesting
	double nfc_H;
	double *curr10Harvest;
	double *curr100Harvest;
	void setCurrHarvest();

	// security chip consumption
	double *secPowerConsumption;
	float clock_sec;

	// microcontroller consumption
	double *ucPowerConsumption;
	float clock_uc;
	double calculateCurrentUC(double input_val, pss::EnergyStates_MicroController type);

	// configure the type of the program used
	int uc_program;
	// configure which security function in the main program is used
	int security_function;
};

#endif /* GENERAL_CONFIGURATION_H_ */
