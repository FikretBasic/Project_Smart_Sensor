/*
 * RFScanner.h
 *
 *  Created on: 17 Oct 2018
 *      Author: its2016
 */

#ifndef MODULES_RF_SCANNER_RFSCANNER_H_
#define MODULES_RF_SCANNER_RFSCANNER_H_

#include "../Includes.h"
#include "../nfc/NFCFrame.h"
#include "../nfc/nfc_frame/RFFrameResponse.h"

namespace pss {

enum frame_control {F_SOF, F_EOF};

SC_MODULE(rfscanner)
{
public:
	sc_in<bool> rf_clk_in;
	sc_in<bool> rf_clk_out;
	sc_in<bool> enable_rf_in;
	sc_out<bool> enable_rf_out;
	sc_in<bool> antenna_in;
	sc_out<bool> antenna_out;

	void commScript();
	void rfControl();
	void rfInput();
	void rfResendFrame();

	SC_CTOR(rfscanner)
	{
		controlScript = true;

		rfStates = start_rf;
		rfWrite = false;
		waitForTheResponse = false;

		SC_THREAD(commScript);
		sensitive << rf_clk_out.pos();

		SC_THREAD(rfControl);
		sensitive << rf_clk_out.pos();

		SC_THREAD(rfInput);
		sensitive << rf_clk_out.pos();
	};

private:
	RFSendState rfStates;
	std::queue<byte> rfValues;
	bool rfWrite;
	bool waitForTheResponse;
	std::vector<byte> resultRFValues;

	// to control the start of the writting values at the beginning of the test
	bool controlScript;

	void send_frame(byte &buffer, int &iter);
	void frameDelay();
	void waitForAction();
};

} /* namespace pss */

#endif /* MODULES_RF_SCANNER_RFSCANNER_H_ */
