/*
 * RFScanner.cpp
 *
 *  Created on: 17 Oct 2018
 *      Author: its2016
 */

#include "RFScanner.h"

namespace pss {

void rfscanner::commScript()
{
	while(true)
	{
		wait();

		if(controlScript)
		{
			cout << "[INFO][" << sc_time_stamp() << "] " << "The start of writing to the NFC" << endl;

			// Configuration command MB_MODE set to 0x01 (normally this would be initialized)
			NFCFrame::instance()->writeConfigurationCommand(rfValues, 0x40, 0x0d, 0x01);
			rfWrite = true;
			waitForTheResponse = true;
			waitForAction();

			/*
			// we get the values of the dyn. register EH_CTRL_Dyn and see if the V_ON and RF_ON match
			// here, we assume that they do, in standard application additional steps would be set
			NFCFrame::instance()->readDynamicCommand(rfValues, 0x40, 0x02);
			rfWrite = true;
			waitForTheResponse = true;
			waitForAction();


			// we get the values of the dyn. register MB_CTRL_DYN and see if the if the FTM is reset
			// if it is, then we can write
			NFCFrame::instance()->readDynamicCommand(rfValues, 0x40, 0x06);
			rfWrite = true;
			waitForTheResponse = true;
			waitForAction();
			*/

			// write the value inside the register to set it to be FTM
			NFCFrame::instance()->writeDynamicCommand(rfValues, 0x40, 0x06, 0x01);
			rfWrite = true;
			waitForTheResponse = true;
			waitForAction();

			/*
			// check if the value has been set correctly
			NFCFrame::instance()->readDynamicCommand(rfValues, 0x40, 0x06);
			rfWrite = true;
			waitForTheResponse = true;
			waitForAction();

			// read message length to check if it is correct
			NFCFrame::instance()->readMessageLengthCommand(rfValues, 0x40);
			rfWrite = true;
			waitForTheResponse = true;
			waitForAction();

			cout << "Timestamp: " << sc_time_stamp() << endl;
			// read message to check if it is empty how it should be
			NFCFrame::instance()->readMessageCommand(rfValues, 0x40, 0x00, 0x20);
			rfWrite = true;
			waitForTheResponse = true;
			waitForAction();
			*/

			//cout << "\nTimestamp begin: " << sc_time_stamp() << endl;

			// write from RF to the Mailbox
			std::vector<byte> inputVal;
			if(Configuration::instance()->getProgram_input().size() == 0)
			{
				for(int i = 0x00; i < 0x20; i++)
					inputVal.push_back(i);
			}
			else
			{
				std::vector<byte> temp_input = Configuration::instance()->getProgram_input();
				for(size_t i = 0x00; i < temp_input.size() && i < 256; i++)
					inputVal.push_back(temp_input.at(i));
			}

			NFCFrame::instance()->writeMessageCommand(rfValues, 0x40, inputVal.size() - 1, inputVal);
			rfWrite = true;
			waitForTheResponse = true;
			waitForAction();

			// only run the script one time, afterwards break it
			controlScript = false;

			cout << "[INFO][" << sc_time_stamp() << "] " << "The end of writting to the NFC" << endl;
		}
	}
}

void rfscanner::rfControl()
{
	byte buffer = 0;
	int iter = 7;

	while(true)
	{
		wait();

		if(rfWrite)
		{
			// write op
			if(rfValues.size() > 0)
			{
				send_frame(buffer, iter);
			}
		}
	}
}

void rfscanner::rfInput()
{
	nfc::RF_Frame_Response *frame_resp = NULL;
	std::vector<bool> input;

	int err_flag;

	while(true)
	{
		wait();

		if(enable_rf_in.read())
		{
			if(frame_resp == NULL)
			{
				frame_resp = new nfc::RF_Frame_Response();
				//waitForTheResponse = false;
				input.clear();
			}
			else
			{
				//cout << "Anntena receive: " << antenna_in.read() << endl << endl;
				input.push_back(antenna_in.read());
			}
		}
		else
		{
			if(frame_resp != NULL)
			{

				if(frame_resp->setFrame(input, err_flag))
				{
					// TODO: set a function to process the received frame (make response)

					cout << "\nFrame successfully received!" << endl;

					// Correctly respond depending if it is an error frame or not
					if(frame_resp->getErrChk())
					{
						cout << "An ERROR Frame: " << hex << (int)frame_resp->getErrVal() << endl;
					}
					else
					{
						cout << "A STATUS Frame: ";

						if(frame_resp->getData().size() != 0)
						{
							int j = 0;
							byte tempVal = 0;

							resultRFValues.clear();

							for(size_t k = 0; k < (frame_resp->getData().size() / 8); k++)
							{
								for(int i = 7; i >= 0; i--)
								{
									tempVal |= (frame_resp->getData().at(j) << i);
									j++;
								}

								resultRFValues.push_back(tempVal);
								cout << (int)tempVal << " ";
								tempVal = 0;
							}
							cout << endl;
						}
						else
						{
							cout << " NO ADDITIONAL DATA TO BE SHOWN" << endl;
						}

					}
				}
				else
				{
					// TODO: set a function to process the error (crc or something else...)
					cout << "\nFrame received an error!" << endl;
				}

				frame_resp = NULL;
				waitForTheResponse = false;

				// so-called time t2, before a new request can be sent
				wait(310, SC_US);
			}
		}
	}
}

void rfscanner::send_frame(byte &buffer, int &iter)
{
	switch(rfStates)
	{
		case start_rf:

			// inserting delay for SOF
			frameDelay();

			enable_rf_out.write(1);
			buffer = rfValues.front();

			rfStates = operation_rf;

			break;

		case operation_rf:

			if(iter < 0)
			{
				if (rfValues.size() == 1)
				{
					rfStates = stop_rf;
					buffer = 0;
					iter = 7;
				}
				else
				{
					rfValues.pop();
					iter = 7;
					buffer = rfValues.front();
				}
			}
			else
			{
				//cout << "Anntena send: " << (int)((buffer >> iter) & 1) << endl;
				antenna_out.write((buffer >> iter) & 1);
				iter--;
			}

			break;

		case stop_rf:

			rfValues.pop();
			enable_rf_out.write(0);
			rfStates = start_rf;
			rfWrite = false;

			buffer = 0;
			iter = 7;

			// inserting delay for EOF
			frameDelay();

			rfResendFrame();

			break;

		default:
			rfStates = start_rf;
	}
}


void rfscanner::rfResendFrame()
{
	// taken for the 100% modulation, t1 + tSOF
	wait(320, SC_US);
	frameDelay();

	if(waitForTheResponse)
	{
		//testVarRFSend1 = true;
	}
}

// The delay time is the same for SOF and EOF
void rfscanner::frameDelay()
{
	if(RF_BIT_OUTPUT_RATE == BitRate_26Kbit)
	{
		wait(76, SC_US);
	}
	else
	{
		wait(302, SC_US);
	}
}

void rfscanner::waitForAction()
{
	while(true)
	{
		if(!waitForTheResponse)
			break;
		wait(100, SC_US);
	}
	//wait(700, SC_US);
}

} /* namespace pss */
