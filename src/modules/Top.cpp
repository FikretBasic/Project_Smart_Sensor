/*
 * Top.cpp
 *
 *  Created on: 19 Apr 2018
 *      Author: its2016
 */

#include "Top.h"

namespace pss {

void top::generalI2Cclock()
{
	while(true)
	{
		wait();

		if(++ticks >= 500)
		{
			i2c_clock.write(edge);
			edge = !edge;
			ticks = 0;
		}
	}
}

void top::batteryTest()
{
	while(true)
	{
		wait();

		if(testBatteryVar)
		{
			bat_test_module->batteryTest1 = true;
			testBatteryVar = false;
		}
	}
}

void top::setTestSettings(short* act_test_benches)
{
	if(act_test_benches[0] != 0)
	{
		SimulationOutput::instance()->setSimOutputFile(1, act_test_benches[0], 1);
		switch(act_test_benches[0])
		{
			case 1:
				mem_test_module->testVarWrite1 = true;
				break;
			case 2:
				mem_test_module->testVarWrite2 = true;
				break;
			case 3:
				mem_test_module->testVarWriteFail = true;
				break;
			case 4:
				mem_test_module->testVarReadPass = true;
				break;
			case 5:
				mem_test_module->testVarSleepMode = true;
				break;
			case 6:
				mem_test_module->testVarProgressive = true;
				break;
			default:
				mem_test_module->testVarWrite1 = true;
		}
	}
	else if(act_test_benches[1] != 0)
	{
		SimulationOutput::instance()->setSimOutputFile(1, act_test_benches[1], 2);
		nfc_test_module->rfNextValue = 0;
		switch(act_test_benches[1])
		{
			case 1:
				nfc_test_module->testVarWrite1 = true;
				break;
			case 2:
				nfc_test_module->testVarWrite2 = true;
				break;
			case 3:
				nfc_test_module->testVarWriteRead1 = true;
				break;
			case 4:
				nfc_test_module->testVarWriteRead2 = true;
				break;
			case 5:
				nfc_test_module->testVarSystem = true;
				break;
			case 6:
				nfc_test_module->testVarDynamic = true;
				break;
			case 7:
				nfc_test_module->testVarRFInventory = true;
				break;
			case 8:
				nfc_test_module->testVarRFReadBlock = true;
				break;
			case 9:
				nfc_test_module->testVarRFWriteBlock = true;
				break;
			case 10:
				nfc_test_module->testVarRFReadMultiple = true;
				break;
			case 11:
				nfc_test_module->testVarRFWriteMultiple = true;
				break;
			case 12:
				nfc_test_module->testVarRFWriteConfig = true;
				break;
			case 13:
				nfc_test_module->testVarRFWriteDynamic = true;
				break;
			case 14:
				nfc_test_module->testVarRFWriteMessage = true;
				break;
			case 15:
				nfc_test_module->testVarMailBox1 = true;
				break;
			case 16:
				nfc_test_module->testVarMailBox2 = true;
				break;
			case 17:
				nfc_test_module->testVarHarvesting = true;
				break;
			default:
				nfc_test_module->testVarWrite1 = true;
		}
	}
	else if(act_test_benches[2] != 0)
	{
		SimulationOutput::instance()->setSimOutputFile(1, act_test_benches[2], 3);
		switch(act_test_benches[2])
		{
			case 1:
				sec_test_module->testVarWrite1 = true;
				break;
			case 2:
				sec_test_module->testVarWrite2 = true;
				break;
			case 3:
				sec_test_module->testVarWriteRead1 = true;
				break;
			case 4:
				sec_test_module->testVarWriteRead2 = true;
				break;
			case 5:
				sec_test_module->testVarSystem1 = true;
				break;
			case 6:
				sec_test_module->testVarSystem2 = true;
				break;
			case 7:
				sec_test_module->testVarSecurity1 = true;
				break;
			case 8:
				sec_test_module->testVarSecurity2 = true;
				break;
			case 9:
				sec_test_module->testVarSecurity3 = true;
				break;
			default:
				sec_test_module->testVarWrite1 = true;
		}
	}
}

void top::calculateEnergy()
{
	double avgPower = 0;

	while(true)
	{
		wait();

		if(avgPower != 0)
		{
			SimulationOutput::instance()->writeToSimPowerFile(sc_time_stamp(), avgPower);
		}

		totalCurrentUsed = 0;
		totalCurrentUsed += energyMem;
		totalCurrentUsed += nfc_currentOut_I2C;
		totalCurrentUsed += nfc_currentOut_RF;
		totalCurrentUsed += sec_currentOut;

		load_bat.write(totalCurrentUsed);

		Configuration::instance()->setVCC(voltage_bat.read());
		avgPower = SimulationOutput::instance()->setToPowerArray(totalCurrentUsed * voltage_bat.read());
	}
}

void top::setTraceOptions(sc_trace_file *fp, std::queue<std::string> trace_options)
{
	if(fp != NULL)
	{
		while(!trace_options.empty())
		{
			if(trace_options.front().compare("main_clock") == 0)
			{
				sc_trace(fp, clock, "main_clock");
			}
			else if(trace_options.front().compare("scl_mem") == 0)
			{
				sc_trace(fp, scl_mem, "scl_mem");
			}
			else if(trace_options.front().compare("sda_in_mem") == 0)
			{
				sc_trace(fp, sda_in_mem, "sda_in_mem");
			}
			else if(trace_options.front().compare("sda_out_mem") == 0)
			{
				sc_trace(fp, sda_out_mem, "sda_out_mem");
			}
			else if(trace_options.front().compare("a1_mem") == 0)
			{
				sc_trace(fp, a1_mem, "a1_mem");
			}
			else if(trace_options.front().compare("a2_mem") == 0)
			{
				sc_trace(fp, a2_mem, "a2_mem");
			}
			else if(trace_options.front().compare("wp_mem") == 0)
			{
				sc_trace(fp, wp_mem, "wp_mem");
			}
			else if(trace_options.front().compare("enable_mem") == 0)
			{
				sc_trace(fp, enable_mem, "enable_mem");
			}
			else if(trace_options.front().compare("energyMem") == 0)
			{
				sc_trace(fp, energyMem, "energyMem");
			}
			else if(trace_options.front().compare("enable_bat") == 0)
			{
				sc_trace(fp, enable_bat, "enable_bat");
			}
			else if(trace_options.front().compare("load_bat") == 0)
			{
				sc_trace(fp, load_bat, "load_bat");
			}
			else if(trace_options.front().compare("voltage_bat") == 0)
			{
				sc_trace(fp, voltage_bat, "voltage_bat");
			}
			else if(trace_options.front().compare("signal_rf_clk_in") == 0)
			{
				sc_trace(fp, signal_rf_clk_in, "signal_rf_clk_in");
			}
			else if(trace_options.front().compare("signal_rf_clk_out") == 0)
			{
				sc_trace(fp, signal_rf_clk_out, "signal_rf_clk_out");
			}
			else if(trace_options.front().compare("scl_nfc") == 0)
			{
				sc_trace(fp, scl_nfc, "scl_nfc");
			}
			else if(trace_options.front().compare("sda_in_nfc") == 0)
			{
				sc_trace(fp, sda_in_nfc, "sda_in_nfc");
			}
			else if(trace_options.front().compare("sda_out_nfc") == 0)
			{
				sc_trace(fp, sda_out_nfc, "sda_out_nfc");
			}
			else if(trace_options.front().compare("enable_nfc") == 0)
			{
				sc_trace(fp, enable_nfc, "enable_nfc");
			}
			else if(trace_options.front().compare("gpo_nfc") == 0)
			{
				sc_trace(fp, gpo_nfc, "gpo_nfc");
			}
			else if(trace_options.front().compare("v_eh_nfc") == 0)
			{
				sc_trace(fp, v_eh_nfc, "v_eh_nfc");
			}
			else if(trace_options.front().compare("enable_rf_in") == 0)
			{
				sc_trace(fp, enable_rf_in, "enable_rf_in");
			}
			else if(trace_options.front().compare("enable_rf_out") == 0)
			{
				sc_trace(fp, enable_rf_out, "enable_rf_out");
			}
			else if(trace_options.front().compare("antenna_in") == 0)
			{
				sc_trace(fp, antenna_in, "antenna_in");
			}
			else if(trace_options.front().compare("antenna_out") == 0)
			{
				sc_trace(fp, antenna_out, "antenna_out");
			}
			else if(trace_options.front().compare("nfc_currentOut_I2C") == 0)
			{
				sc_trace(fp, nfc_currentOut_I2C, "nfc_currentOut_I2C");
			}
			else if(trace_options.front().compare("nfc_currentOut_RF") == 0)
			{
				sc_trace(fp, nfc_currentOut_RF, "nfc_currentOut_RF");
			}
			else if(trace_options.front().compare("scl_sec") == 0)
			{
				sc_trace(fp, scl_sec, "scl_sec");
			}
			else if(trace_options.front().compare("sda_in_sec") == 0)
			{
				sc_trace(fp, sda_in_sec, "sda_in_sec");
			}
			else if(trace_options.front().compare("sda_out_sec") == 0)
			{
				sc_trace(fp, sda_out_sec, "sda_out_sec");
			}
			else if(trace_options.front().compare("enable_sec") == 0)
			{
				sc_trace(fp, enable_sec, "enable_sec");
			}
			else if(trace_options.front().compare("sec_currentOut") == 0)
			{
				sc_trace(fp, sec_currentOut, "sec_currentOut");
			}

			trace_options.pop();
		}
	}
	else
	{
		cout << "[ERROR] Cannot write to the trace file, needs to be initiated first!" << endl;
	}
}


} /* namespace pss */
