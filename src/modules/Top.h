/*
 * Top.h
 *
 *  Created on: 19 Apr 2018
 *      Author: its2016
 */

#ifndef MODULES_TOP_H_
#define MODULES_TOP_H_

#include "Includes.h"
#include "fram/Memory.h"
#include "fram/MemoryTestbench.h"
#include "battery/Battery.h"
#include "battery/BatteryTestbench.h"
#include "nfc/NFC.h"
#include "nfc/NFCTestbench.h"
#include "quartz/NfcQuartz.h"
#include "security/SecurityChip.h"
#include "security/SecurityChipTestbench.h"

namespace pss {

struct top : public sc_module
{
public:

	SC_HAS_PROCESS(top);

	sc_in<bool> clock;

	void generalI2Cclock();
	void batteryTest();
	void calculateEnergy();

	void setTraceOptions(sc_trace_file *fp, std::queue<std::string> trace_options);

    top(const sc_module_name& name) : sc_module(name)
	{
		edge = false;
		ticks = 0;

		testBatteryVar = false;
		totalCurrentUsed = 0;

		// ------- Memory -------

		mem_module = new fram("FRAM");
		mem_module->scl(scl_mem);
		mem_module->sda_in(sda_in_mem);
		mem_module->sda_out(sda_out_mem);
		mem_module->a1(a1_mem);
		mem_module->a2(a2_mem);
		mem_module->wp(wp_mem);
		mem_module->enable(enable_mem);
		mem_module->energyOut(energyMem);

		mem_test_module = new testb::fram_testbench("FRAM_TESTBENCH");
		mem_test_module->clk(i2c_clock);
		mem_test_module->wp(wp_mem);
		mem_test_module->a1(a1_mem);
		mem_test_module->a2(a2_mem);
		mem_test_module->scl(scl_mem);
		mem_test_module->sda_in(sda_out_mem);
		mem_test_module->sda_out(sda_in_mem);
		mem_test_module->enable(enable_mem);

		// ------- Battery -------

		bat_module = new battery("BATTERY");
		bat_module->clk(clock);
		bat_module->enable(enable_bat);
		bat_module->current(load_bat);
		bat_module->voltage(voltage_bat);

		bat_test_module = new testb::battery_testbench("BATTERY_TESTBENCH");
		bat_test_module->clk(clock);
		bat_test_module->enable(enable_bat);
		bat_test_module->load(load_bat);
		bat_test_module->voltage(voltage_bat);

		// ------- Quartz NFC -------

		nfcClk = new quartz::nfcQuartz("NFC_Quartz");
		nfcClk->clock_in(clock);
		nfcClk->enabled(enable_bat);
		nfcClk->clock_out_input(signal_rf_clk_in);
		nfcClk->clock_out_output(signal_rf_clk_out);

		// ------- NFC -------

		nfc_module = new nfc_tag("NFC_TAG");
		nfc_module->scl(scl_nfc);
		nfc_module->sda_in(sda_in_nfc);
		nfc_module->sda_out(sda_out_nfc);
		nfc_module->enable(enable_nfc);
		nfc_module->gpo(gpo_nfc);
		nfc_module->v_eh(v_eh_nfc);
		nfc_module->rf_clk_in(signal_rf_clk_in);
		nfc_module->rf_clk_out(signal_rf_clk_out);
		nfc_module->enable_rf_in(enable_rf_in);
		nfc_module->enable_rf_out(enable_rf_out);
		nfc_module->antenna_in(antenna_in);
		nfc_module->antenna_out(antenna_out);
		nfc_module->currentOutI2C(nfc_currentOut_I2C);
		nfc_module->currentOutRF(nfc_currentOut_RF);

		nfc_test_module = new testb::nfc_testbench("NFC_TESTBENCH");
		nfc_test_module->clk(i2c_clock);
		nfc_test_module->scl(scl_nfc);
		nfc_test_module->sda_in(sda_out_nfc);
		nfc_test_module->sda_out(sda_in_nfc);
		nfc_test_module->enable(enable_nfc);
		nfc_test_module->gpo(gpo_nfc);
		nfc_test_module->v_eh(v_eh_nfc);
		nfc_test_module->rf_clk_in(signal_rf_clk_in);
		nfc_test_module->rf_clk_out(signal_rf_clk_out);
		nfc_test_module->enable_rf_in(enable_rf_out);
		nfc_test_module->enable_rf_out(enable_rf_in);
		nfc_test_module->antenna_in(antenna_out);
		nfc_test_module->antenna_out(antenna_in);

		// ------- Security -------

		sec_module = new security("SECURITY_CHIP");
		sec_module->scl(scl_sec);
		sec_module->sda_in(sda_in_sec);
		sec_module->sda_out(sda_out_sec);
		sec_module->enable(enable_sec);
		sec_module->currentOut(sec_currentOut);
		sec_module->operate(enable_bat);

		sec_test_module = new testb::security_testbench("SECURITY_TESTBENCH");
		sec_test_module->clk(i2c_clock);
		sec_test_module->scl(scl_sec);
		sec_test_module->sda_in(sda_out_sec);
		sec_test_module->sda_out(sda_in_sec);
		sec_test_module->enable(enable_sec);

		// ------- THREADS -------

		SC_THREAD(generalI2Cclock);
		sensitive << clock.pos();

		SC_THREAD(batteryTest);
		sensitive << clock.pos();

		SC_THREAD(calculateEnergy);
		sensitive << clock.pos();
	};

//private:

    // I2C Clock
	bool edge;
	unsigned int ticks;
	sc_signal<bool> i2c_clock;

    // Memory
	fram *mem_module;
	testb::fram_testbench *mem_test_module;

	sc_signal<bool> scl_mem;
	sc_signal<bool> sda_in_mem;
	sc_signal<bool> sda_out_mem;
	sc_signal<bool> a1_mem;
	sc_signal<bool> a2_mem;
	sc_signal<bool> wp_mem;
	sc_signal<bool> enable_mem;
	sc_signal<double> energyMem;

	// Battery
	battery *bat_module;
	testb::battery_testbench *bat_test_module;

	sc_signal<bool> enable_bat;
	sc_signal<double> load_bat;
	sc_signal<double> voltage_bat;

	// NFC - Clock-Rate controll
	quartz::nfcQuartz *nfcClk;
	sc_signal<bool> signal_rf_clk_in;
	sc_signal<bool> signal_rf_clk_out;

    // NFC
	nfc_tag *nfc_module;
	testb::nfc_testbench *nfc_test_module;

	sc_signal<bool> scl_nfc;
	sc_signal<bool> sda_in_nfc;
	sc_signal<bool> sda_out_nfc;
	sc_signal<bool> enable_nfc;
	sc_signal<bool> gpo_nfc;
	sc_signal<double> v_eh_nfc;
	sc_signal<double> energyNFC;
	sc_signal<bool> enable_rf_in;
	sc_signal<bool> enable_rf_out;
	sc_signal<bool> antenna_in;
	sc_signal<bool> antenna_out;
	sc_signal<double> nfc_currentOut_I2C;
	sc_signal<double> nfc_currentOut_RF;

	// Security
	security *sec_module;
	testb::security_testbench *sec_test_module;

	sc_signal<bool> scl_sec;
	sc_signal<bool> sda_in_sec;
	sc_signal<bool> sda_out_sec;
	sc_signal<bool> enable_sec;
	sc_signal<double> sec_currentOut;

	// General Test Variables and Functions
	void setTestSettings(short* act_test_benches);
	bool testBatteryVar;

	// Overall Current Used
	double totalCurrentUsed;
};


} /* namespace pss */

#endif /* MODULES_TOP_H_ */
