/*
 * MemoryTestbench.h
 *
 *  Created on: 19 Apr 2018
 *      Author: its2016
 */

#ifndef MODULES_MEMORYTESTBENCH_H_
#define MODULES_MEMORYTESTBENCH_H_

#include "../Includes.h"

namespace testb {

SC_MODULE(fram_testbench)
{
public:

	sc_in<bool> clk;
	sc_in<bool> sda_in;

	sc_out<bool> wp;
	sc_out<bool> scl;
	sc_out<bool> sda_out;
	sc_out<bool> a1;
	sc_out<bool> a2;
	sc_out<bool> enable;

	bool testVarWrite1;
	bool testVarWrite2;
	bool testVarWriteFail;
	bool testVarReadPass;
	bool testVarSleepMode;
	bool testVarProgressive;

	void sclGenerator();
	void memoryControl();

	void runTest();

	SC_CTOR(fram_testbench)
	{
		testVarWrite1 = false;
		testVarWrite2 = false;
		testVarWriteFail = false;
		testVarReadPass = false;
		testVarSleepMode = false;
		testVarProgressive = false;
		numOfTest = 0;

		opStates = start;
		sleepState = false;

		writeOp = false;
		readOp = false;
		sleepOp = false;
		memAddr = 1;
		readValSize = 0;

		SC_THREAD(sclGenerator);
		sensitive << clk;

		SC_THREAD(memoryControl);
		sensitive << clk.pos();

		SC_THREAD(runTest);
		sensitive << clk.pos();
	};

private:

	OperativeStates opStates;
	bool sleepState;
	int numOfTest;

	bool writeOp;
	bool readOp;
	bool sleepOp;
	size_t readValSize;
	int memAddr;
	std::queue<byte> memValues;

	void writeInMemory(byte &memBuffer, int &iter);
	void readFromMemory(byte &memBuffer, int &iter);
	void setMemToSleep(byte &memBuffer, int &iter);

};

} /* namespace pss */

#endif /* MODULES_MEMORYTESTBENCH_H_ */
