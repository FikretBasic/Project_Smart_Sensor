/*
 * MemoryTestbench.cpp
 *
 *  Created on: 19 Apr 2018
 *      Author: its2016
 */

#include "MemoryTestbench.h"

namespace testb {

void fram_testbench::sclGenerator()
{
	while(true)
	{
		wait();

		scl.write(clk);
	}
}

void fram_testbench::runTest()
{
	while(true)
	{
		wait();

		if (testVarWrite1)
		{
			//testWritePass1(memBuffer, memAddr, iter);

			memValues.push(40);
			memAddr = 120;
			writeOp = true;

			testVarWrite1 = false;
		}
		else if (testVarWrite2)
		{
			//testWritePass2(memBuffer, memAddr, iter, writeIter);

			for (int i=0; i < 3500; i++)
			{
				memValues.push(i % 255);
			}

			//memValues.push(40);
			//memValues.push(26);
			//memValues.push(35);
			//memValues.push(120);
			memAddr = 120;
			writeOp = true;

			testVarWrite2 = false;
		}
		else if (testVarReadPass)
		{
			readOp = true;
			memAddr = 120;
			readValSize = 3500;

			testVarReadPass = false;
		}
		else if (testVarSleepMode)
		{
			//testSleepMode(memBuffer, iter);

			sleepOp = true;
			testVarSleepMode = false;

			//wait(50, SC_US);
			wait(50, SC_MS);

			for (int i=0; i < 3500; i++)
			{
				memValues.push(i % 255);
			}

			//memValues.push(40);
			//memValues.push(26);
			//memValues.push(35);
			//memValues.push(120);
			memAddr = 120;
			writeOp = true;
		}
		else if (testVarProgressive)
		{
			// first put to sleep
			sleepOp = true;
			testVarProgressive = false;

			//wait(50, SC_US);
			wait(25, SC_MS);

			for (int i=0; i < 1500; i++)
			{
				memValues.push(i % 255);
			}

			memAddr = 121;
			writeOp = true;
			while(writeOp)
			{
				wait(1, SC_US);
			}

			// wait for additional 25 milliseconds
			wait(60, SC_US);
			sleepOp = true;
			wait(25, SC_MS);

			// read the written values
			readOp = true;
			memAddr = 121;
			readValSize = 1500;
		}

	}
}

void fram_testbench::memoryControl()
{
	byte memBuffer = 0;
	int iter = 7;

	a1.write(1);
	a2.write(1);
	wp.write(0);

	while(true)
	{
		wait();

		if(writeOp)
		{
			// write op
			if(memValues.size() > 0)
			{
				writeInMemory(memBuffer, iter);
			}
		}
		else if(readOp)
		{
			// read op
			readFromMemory(memBuffer, iter);
		}
		else if(sleepOp)
		{
			// sleep op
			setMemToSleep(memBuffer, iter);
		}
	}
}

void fram_testbench::writeInMemory(byte &memBuffer, int &iter)
{
	switch(opStates)
	{
		case start:
			//wait(10, SC_US);
			enable.write(1);
			opStates = dev_addr;

			// 10101100 - device address and instruction
			memBuffer = 0xac;

			break;

		case dev_addr:

			if(iter < 0)
			{
				sda_out.write(1);

				if (sleepState)
				{
					//wait();
					cout << "\nSLEEP STATE WRITE" << endl;
					enable.write(0);
					opStates = start;
					iter = 7;
					sleepState = false;
				}
				else
				{
					opStates = mem_addr;
					memBuffer = 0;
					iter = 16;
				}
			}
			else
			{
				sda_out.write((memBuffer >> iter) & 1);
				iter--;
			}

			break;

		case mem_addr:

			if(iter < 0)
			{
				sda_out.write(1);
				opStates = rd_wr;
				memBuffer = memValues.front();
				iter = 7;
			}
			else
			{
				/*if(iter == 16)
					memAddr = 100;*/

				sda_out.write((memAddr >> iter) & 1);
				iter--;
			}

			break;

		case rd_wr:

			if(iter < 0)
			{
				sda_out.write(1);

				if (memValues.size() == 1)
				{
					opStates = stop;
					memBuffer = 0;
					//memAddr = 0;
					iter = 7;
				}
				else
				{
					//writeIter--;
					memValues.pop();
					iter = 7;
					memBuffer = memValues.front();
				}
			}
			else
			{
				sda_out.write((memBuffer >> iter) & 1);
				iter--;
			}

			break;

		case stop:
			memValues.pop();
			enable.write(0);
			opStates = start;
			writeOp = false;

			break;

		default:
			opStates = start;

	}
}

void fram_testbench::readFromMemory(byte &memBuffer, int &iter)
{
	switch(opStates)
	{
		case start:
			wait();
			//wait();
			enable.write(1);
			opStates = dev_addr;

			// 10101100 - device address and instruction
			memBuffer = 0xad;

			break;

		case dev_addr:

			if(iter < 0)
			{
				sda_out.write(1);

				if (sleepState)
				{
					//wait();
					cout << "\nSLEEP STATE READ" << endl;
					enable.write(0);
					opStates = start;
					iter = 7;
					sleepState = false;
				}
				else
				{
					opStates = mem_addr;
					memBuffer = 0;
					iter = 16;
				}
			}
			else
			{
				sda_out.write((memBuffer >> iter) & 1);
				iter--;
			}

			break;

		case mem_addr:

			if(iter < 0)
			{
				sda_out.write(1);
				opStates = rd_wr;
				memBuffer = 0;
				iter = 7;

				// wait one cycle to synchronise
				wait();
			}
			else
			{
				//if(iter == 16)
					//memAddr = 100;

				sda_out.write((memAddr >> iter) & 1);
				iter--;
			}

			break;

		case rd_wr:

			if(iter < 0)
			{
				sda_out.write(1);
				cout << "\nMemory buffer: " << (int)memBuffer << " [" << memAddr << "]" << endl;
				memValues.push(memBuffer);

				if (memValues.size() == readValSize)
				{
					opStates = stop;
				}
				else
				{
					memAddr++;
				}

				memBuffer = 0;
				iter = 7;
			}
			else
			{
				byte cur_val = sda_in.read();
				memBuffer |= (cur_val << iter);
				iter--;
			}

			break;

		case stop:
			enable.write(0);
			opStates = start;

			readOp = false;

			break;

		default:
			opStates = start;

	}
}

void fram_testbench::setMemToSleep(byte &memBuffer, int &iter)
{
	switch(opStates)
	{
		case start:
			wait();
			enable.write(1);
			opStates = dev_addr;
			// 10101100 - device address and instruction
			memBuffer = 0xf8;

			break;

		case dev_addr:

			if(iter < 0)
			{
				sda_out.write(1);
				opStates = rd_wr;
				memBuffer = 0x86;
				iter = 7;
			}
			else
			{
				sda_out.write((memBuffer >> iter) & 1);
				iter--;
			}

			break;

		case rd_wr:

			if(iter < 0)
			{
				sda_out.write(1);

				if (sda_in.read() == 1)
				{
					sleepState = true;
				}

				opStates = stop;
				memBuffer = 0;
				iter = 7;
			}
			else
			{
				sda_out.write((memBuffer >> iter) & 1);
				iter--;
			}

			break;

		case stop:
			enable.write(0);
			opStates = start;

			/*testVarSleepMode = false;
			if(numOfTest == 0)
				testVarWrite2 = true;*/
			sleepOp = false;

			break;

		default:
			opStates = start;

	}
}


} /* namespace pss */
