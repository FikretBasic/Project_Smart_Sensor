/*
 * Memory.cpp
 *
 *  Created on: 18 Apr 2018
 *      Author: its2016
 */

#include "Memory.h"

namespace pss
{

void fram::i2c()
{
	int iter = 7;
	bool ack = true;
	byte cur_val = 0;
	bool sleep_op = false;

	sda_out.write(1);

	while(true)
	{
		wait();

		switch(opState)
		{
			case start:
				if (enable.read())
				{
					opState = dev_addr;
					enState = operation;
					mem_buffer = 0;
					addressBuffer = 0;
				}
				break;

			case dev_addr:
				cur_val = sda_in.read();
				mem_buffer |= (cur_val << iter);

				if(iter == 0)
				{
					cur_val = checkDeviceAddressValue();

					if (cur_val == 1)
					{
						read_or_write = mem_buffer & 1;
						sda_out.write(0);

						if (sleep_op)
						{
							opState = stop;
							iter = 7;
						}
						else
						{
							opState = mem_addr;
							iter = 16;
						}
						sleep_op = false;
					}
					else if (cur_val == 2)
					{
						sda_out.write(0);

						sleep_op = true;
						read_or_write = false;
						opState = rd_wr;
						mem_buffer = 0;
						ack = true;
						iter = 7;
					}
					else
					{
						opState = start;
						enState = stand_by;
					}
				}
				else
				{
					iter--;
				}

				break;

			case mem_addr:
				if (sda_in.read() == 1 && ack)
				{
					ack = false;
					sda_out.write(1);
				}
				else
				{
					int cur_val = sda_in.read();
					addressBuffer |= (cur_val << iter);

					if (iter == 0)
					{
						checkAddressSize();

						opState = rd_wr;
						mem_buffer = 0;
						sda_out.write(0);
						ack = true;
						iter = 7;
					}
					else
					{
						iter--;
					}
				}

				break;

			case rd_wr:
				if(enable.read())
				{
					if (sda_in.read() == 1 && ack && !read_or_write)
					{
						ack = false;
						sda_out.write(1);
					}
					else
					{
						if(read_or_write)
						{
							sda_out.write((memory[addressBuffer] >> iter) & 1);
						}
						else
						{
							if(!wp.read())
							{
								byte cur_val = sda_in.read();
								mem_buffer |= (cur_val << iter);
							}
						}

						if(iter == 0)
						{
							if (sleep_op)
							{
								if (mem_buffer == 0x86)
								{
									sda_out.write(1);
								}
								else
								{
									sda_out.write(0);
									sleep_op = false;
								}
								opState = stop;
							}
							else
							{
								if (!read_or_write && !wp.read())
								{
									memory[addressBuffer] = mem_buffer;
									tempMemVal = memory[addressBuffer];

									// Writing out the status to the standard output
									SimulationOutput::instance()->printFramWrite(memory[addressBuffer],
											addressBuffer, sc_time_stamp());

									mem_buffer = 0;
								}
								else
								{
									// for synchronisation
									wait();
								}

								addressBuffer++;
							}

							iter = 7;
							ack = true;
						}
						else
						{
							iter--;
						}
					}
				}
				else
				{
					opState = stop;
				}

				break;

			case stop:
				opState = start;

				if (sleep_op)
					enState = sleep;
				else
					enState = stand_by;

				mem_buffer = 0;
				read_or_write = 0;

				iter = 7;
				ack = true;
				cur_val = 0;

				break;

			default:
				opState = start;
				enState = stand_by;
				read_or_write = 0;
		}
	}
}

void fram::calculateEnergy()
{
	while(true)
	{
		wait();

		// The energy is sent in milliamperes
		// Only the "typical" values are being used

		if(enState == sleep)
		{
			energyOut.write(Configuration::instance()->getFramPowerConsumption(pss::sleep_fram));
		}
		else if(enState == stand_by)
		{
			energyOut.write(Configuration::instance()->getFramPowerConsumption(pss::idle_fram));
		}
		else
		{
			if(FREQUENCY_FRAM_SCL == FREQUENCY_STANDARD_MODE)
			{
				energyOut.write(Configuration::instance()->getFramPowerConsumption(pss::op_standard_fram));
			}
			else if(FREQUENCY_FRAM_SCL == FREQUENCY_FAST_MODE_PLUS)
			{
				energyOut.write(Configuration::instance()->getFramPowerConsumption(pss::op_fast_fram));
			}
			else if(FREQUENCY_FRAM_SCL == FREQUENCY_HIGH_SPEED_MODE)
			{
				energyOut.write(Configuration::instance()->getFramPowerConsumption(pss::op_high_fram));
			}
		}

	}
}

byte fram::getDeviceCode()
{
	// 00001010
	return 10;
}

byte fram::checkDeviceAddressValue()
{
	byte deviceNum = (mem_buffer & 240) >> 4;
	byte a2Check = (mem_buffer & 8) >> 3;
	byte a1Check = (mem_buffer & 4) >> 2;

	if (deviceNum == 10 && a2Check == a2.read() && a1Check == a1.read())
	{
		return 1;
	}
	else if (mem_buffer == 0xf8)
	{
		return 2;
	}
	else
	{
		return 0;
	}
}

void fram::checkAddressSize()
{
	if(memSizeType)
	{
		// check if the address if of the upper bound of the smaller memory
		if(addressBuffer == MB85RC1MT_BLOCK_MEMORY_SIZE)
			addressBuffer = 0x01;
	}
	else
	{
		// check if the address if of the upper bound of the bigger (real) memory
		if(addressBuffer == MB85RC1MT_MEMORY_SIZE)
			addressBuffer = 0x01;
	}
}


}
