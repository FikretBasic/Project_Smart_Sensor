/*
 * Memory.h
 *
 * Module based on the FRAM chip MB85RC1MT
 *
 *  Created on: 18 Apr 2018
 *      Author: Fikret Basic
 */

#ifndef MODULES_MEMORY_H_
#define MODULES_MEMORY_H_

//#include "systemc.h"
#include "../Includes.h"

namespace pss
{

// 128K Memory, 131,072 x 8 bit, from the specification
const size_t MB85RC1MT_MEMORY_SIZE = 128 * 1024;

// 4K Memory, block memory, for testing to save performance
const size_t MB85RC1MT_BLOCK_MEMORY_SIZE = 4096;

SC_MODULE(fram)
{
public:

	/*************/
	/*   Ports   */
	/*************/

	sc_in<bool> a1;
	sc_in<bool> a2;
	sc_in<bool> wp;
	sc_in<bool> scl;
	sc_in<bool> enable;
	sc_in<bool> sda_in;
	sc_out<bool> sda_out;
	sc_out<double> energyOut;


	/*************/
	/*  Methods  */
	/*************/

	void i2c();
	void calculateEnergy();

	static byte getDeviceCode();

	SC_CTOR(fram)
	{
		enState = stand_by;
		opState = start;
		mem_buffer = 0;
		read_or_write = 0;
		addressBuffer = 0;
		memSizeType = 1;
		tempMemVal = 0;

		memory[100] = 15;

		SC_THREAD(i2c);
		sensitive << scl.pos();

		SC_THREAD(calculateEnergy);
		sensitive << scl.pos();
	};

public:

	byte checkDeviceAddressValue();
	void checkAddressSize();

	EnergyStates enState;
	OperativeStates opState;

	byte memory[MB85RC1MT_BLOCK_MEMORY_SIZE];
	bool memSizeType;

	byte mem_buffer;
	byte read_or_write;
	int addressBuffer;

	byte tempMemVal;
};

}

#endif /* MODULES_MEMORY_H_ */
