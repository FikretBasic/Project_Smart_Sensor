/*
 * Includes.h
 *
 *  Created on: 18 Apr 2018
 *      Author: its2016
 */

#ifndef MODULES_INCLUDES_H_
#define MODULES_INCLUDES_H_

#include "systemc.h"
#include <stdlib.h>
#include <string>
#include <sstream>
#include <bitset>
#include <queue>
#include <vector>
#include <ctime>

#include "../general/CRCComputation.h"
#include "../general/Configuration.h"
#include "../general/SimulationOutput.h"

typedef unsigned char byte;

extern int n_cycles;
extern sc_core::sc_time_unit unitType;

// 48Mhz
extern const size_t FREQENCY_MAIN_MCU;

/*
 * Memory elements
 * */

// 0.1MHz
extern const size_t FREQUENCY_STANDARD_MODE;

// 1MHz
extern const size_t FREQUENCY_FAST_MODE_PLUS;

// 3.4MHz
extern const size_t FREQUENCY_HIGH_SPEED_MODE;

// The frequency set for FRAM
extern size_t FREQUENCY_FRAM_SCL;

/* --------------
 * Memory elements
 * */


/*
 * NFC elements
 * */

// incoming data with 1/256 pulse coding mode
extern size_t BitRate_1600bit;

// output data with low data rate mode
extern size_t BitRate_6600bit;

// incoming data with 1/4 pulse coding mode (high data rate mode for output)
extern size_t BitRate_26Kbit;

// bit input rate at the current RF (NFC) device
extern size_t RF_BIT_INPUT_RATE;

// bit output rate at the current RF (NFC) device
extern size_t RF_BIT_OUTPUT_RATE;

// function to calculate the bit rate based on the global clock on the runtime
extern void calcBitRate();

/* --------------
 * NFC elements
 * */


sc_core::sc_time_unit returnSimulationUnit(const char* simInput);

template <class unitType>
void transformUnitsStandard(unitType &unit, sc_core::sc_time_unit type)
{
	int unitIter = 0;

	if(type == SC_MS)
	{
		unitIter = 1;
	}
	else if(type == SC_US)
	{
		unitIter = 2;
	}
	else if(type == SC_NS)
	{
		unitIter = 3;
	}
	else if(type == SC_PS)
	{
		unitIter = 4;
	}
	else if(type == SC_FS)
	{
		unitIter = 5;
	}

	for (int i = 0; i < unitIter; i++)
	{
		unit /= 1000;
	}
}

#endif /* MODULES_INCLUDES_H_ */
