/*
 * NFCFrame.h
 *
 *  Created on: 25 Jun 2018
 *      Author: its2016
 */

#ifndef MODULES_NFC_NFCFRAME_H_
#define MODULES_NFC_NFCFRAME_H_

#include "../Includes.h"
#include "../../general/CRCComputation.h"

class NFCFrame {
public:

	virtual ~NFCFrame();

	void inventoryCommand(std::queue<byte> &rfValues, byte flags);
	void readBlockCommand(std::queue<byte> &rfValues, byte flags, byte addr);
	void writeBlockCommand(std::queue<byte> &rfValues, byte flags, byte addr, std::vector<byte> writeVal);
	void readMultipleCommand(std::queue<byte> &rfValues, byte flags, byte addr, byte numOfBlocks);
	void writeMultipleCommand(std::queue<byte> &rfValues, byte flags, byte addr
			, byte numOfBlocks, std::vector<byte> writeVal);

	void readConfigurationCommand(std::queue<byte> &rfValues, byte flags, byte addr);
	void writeConfigurationCommand(std::queue<byte> &rfValues, byte flags, byte addr, byte writeVal);
	void readDynamicCommand(std::queue<byte> &rfValues, byte flags, byte addr);
	void writeDynamicCommand(std::queue<byte> &rfValues, byte flags, byte addr, byte writeVal);

	void writeMessageCommand(std::queue<byte> &rfValues, byte flags, byte numOfBytes, std::vector<byte> writeVal);
	void readMessageLengthCommand(std::queue<byte> &rfValues, byte flags);
	void readMessageCommand(std::queue<byte> &rfValues, byte flags, byte pointer, byte numOfBytes);

	void emptyTheQueue(std::queue<byte> &rfValues);

	static NFCFrame* instance();

private:
	NFCFrame();
	static NFCFrame *instance_t;

	crc::CRC_Computionation crcObject;

};

#endif /* MODULES_NFC_NFCFRAME_H_ */
