/*
 * NFCTestbench.h
 *
 *  Created on: 28 May 2018
 *      Author: its2016
 */

#ifndef MODULES_NFC_NFCTESTBENCH_H_
#define MODULES_NFC_NFCTESTBENCH_H_

#include "../Includes.h"
#include "NFCFrame.h"
#include "nfc_frame/RFFrameResponse.h"

namespace testb {

enum frame_control {F_SOF, F_EOF};

SC_MODULE(nfc_testbench)
{
public:

	sc_in<bool> clk;
	sc_in<bool> sda_in;
	sc_in<bool> gpo;
	sc_in<double> v_eh;
	sc_out<bool> scl;
	sc_out<bool> sda_out;
	sc_out<bool> enable;

	sc_in<bool> rf_clk_in;
	sc_in<bool> rf_clk_out;
	sc_in<bool> enable_rf_in;
	sc_out<bool> enable_rf_out;
	sc_in<bool> antenna_in;
	sc_out<bool> antenna_out;

	bool testVarWrite1;
	bool testVarWrite2;
	bool testVarWriteRead1;
	bool testVarWriteRead2;
	bool testVarSystem;
	bool testVarDynamic;

	bool testVarRFInventory;
	bool testVarRFReadBlock;
	bool testVarRFWriteBlock;
	bool testVarRFReadMultiple;
	bool testVarRFWriteMultiple;
	bool testVarRFReadConfig;
	bool testVarRFWriteConfig;
	bool testVarRFReadDynamic;
	bool testVarRFWriteDynamic;
	bool testVarRFWriteMessage;
	bool testVarRFReadLength;
	bool testVarRFReadMessage;

	bool testVarMailBox1;
	bool testVarMailBox2;
	bool testVarMailBox3;
	bool testVarHarvesting;

	int rfNextValue;

	void sclGenerate();
	void memoryControl();
	void runTest();

	void rfControl();
	void rfInput();
	void rfResendFrame();

	SC_CTOR(nfc_testbench)
	{
		testVarWrite1 = false;
		testVarWrite2 = false;
		testVarWriteRead1 = false;
		testVarWriteRead2 = false;
		testVarSystem = false;
		testVarDynamic = false;
		testVarRFInventory = false;
		testVarRFReadBlock = false;
		testVarRFWriteBlock = false;
		testVarRFReadMultiple = false;
		testVarRFWriteMultiple = false;
		testVarRFReadConfig = false;
		testVarRFWriteConfig = false;
		testVarRFReadDynamic = false;
		testVarRFWriteDynamic = false;
		testVarRFWriteMessage = false;
		testVarRFReadLength = false;
		testVarRFReadMessage = false;
		testVarMailBox1 = false;
		testVarMailBox2 = false;
		testVarMailBox3 = false;
		testVarHarvesting = false;

		rfNextValue = 0;
		numOfTest = 0;

		opStates = start;
		sleepState = false;

		writeOp = false;
		readOp = false;
		randomRead = false;
		sleepOp = false;
		systemMode = false;
		memAddr = 1;
		readValSize = 0;

		rfStates = start_rf;
		rfWrite = false;
		waitForTheResponse = false;

		SC_THREAD(sclGenerate);
		sensitive << clk;

		SC_THREAD(memoryControl);
		sensitive << clk.pos();

		SC_THREAD(runTest);
		sensitive << clk.pos();

		SC_THREAD(rfControl);
		sensitive << rf_clk_out.pos();

		SC_THREAD(rfInput);
		sensitive << rf_clk_out.pos();
	};

private:

	// relic from the past...
	crc::CRC_Computionation crcObject;

	OperativeStates opStates;
	bool sleepState;
	int numOfTest;

	bool writeOp;
	bool readOp;
	bool randomRead;
	bool sleepOp;
	bool systemMode;

	size_t readValSize;
	int memAddr;
	std::queue<byte> memValues;

	RFSendState rfStates;
	std::queue<byte> rfValues;
	bool rfWrite;
	bool waitForTheResponse;
	std::vector<byte> resultRFValues;

	void writeInMemory(byte &memBuffer, int &iter);
	void readFromMemory(byte &memBuffer, int &iter);
	void setMemToSleep(byte &memBuffer, int &iter);

	void send_frame(byte &buffer, int &iter);

	void frameDelay();
	void setNextTest();

	void waitForAction();
	void waitForI2CAction();
};

} /* namespace nfc */

#endif /* MODULES_NFC_NFCTESTBENCH_H_ */
