/*
 * NFCTestbench.cpp
 *
 *  Created on: 28 May 2018
 *      Author: its2016
 */

#include "NFCTestbench.h"

namespace testb {

void nfc_testbench::sclGenerate()
{
	while(true)
	{
		wait();

		scl.write(clk);
	}
}

void nfc_testbench::runTest()
{
	while(true)
	{
		wait();

		if (testVarWrite1)
		{
			//testWritePass1(memBuffer, memAddr, iter);

			memValues.push(40);
			memAddr = 120;
			writeOp = true;

			testVarWrite1 = false;
		}
		else if (testVarWrite2)
		{
			//testWritePass2(memBuffer, memAddr, iter, writeIter);

			for (int i=0; i < 3500; i++)
			{
				memValues.push(i % 255);
			}
			memAddr = 120;
			writeOp = true;

			testVarWrite2 = false;
		}
		else if (testVarWriteRead1)
		{
			for (int i=0; i < 1500; i++)
			{
				memValues.push(i % 255);
			}
			memAddr = 121;
			writeOp = true;
			while(writeOp)
			{
				wait(1, SC_US);
			}
			//wait(70, SC_US);

			randomRead = true;
			memAddr = 121;
			readValSize = 1500;
			readOp = true;

			testVarWriteRead1 = false;
		}
		else if (testVarWriteRead2)
		{
			memValues.push(30);
			memValues.push(41);
			memAddr = 80;
			writeOp = true;

			wait(70, SC_US);

			memValues.push(123);
			memAddr = 124;
			writeOp = true;

			wait(50, SC_US);

			readOp = true;
			randomRead = false;
			readValSize = 1;

			wait(50, SC_US);

			memValues.push(48);
			memValues.push(53);
			memAddr = 82;
			writeOp = true;

			wait(70, SC_US);

			readOp = true;
			randomRead = true;
			memAddr = 81;
			readValSize = 3;

			testVarWriteRead2 = false;
		}
		else if (testVarSystem)
		{
			memValues.push(45);
			memAddr = 15;
			writeOp = true;
			systemMode = true;

			wait(70, SC_US);

			readOp = true;
			randomRead = true;
			memAddr = 15;
			readValSize = 1;

			testVarSystem = false;
		}
		else if (testVarDynamic)
		{
			memValues.push(45);
			memAddr = 0x2003;
			writeOp = true;
			systemMode = false;

			wait(40, SC_US);

			memAddr = 0x2003;
			readValSize = 1;
			readOp = true;
			randomRead = true;

			//wait(20, SC_US);

			testVarDynamic = false;
		}
		// RF Tests
		else if (testVarRFInventory)
		{
			NFCFrame::instance()->inventoryCommand(rfValues, 0x40);
			rfWrite = true;
			waitForTheResponse = true;

			testVarRFInventory = false;
		}
		else if (testVarRFReadBlock)
		{
			NFCFrame::instance()->readBlockCommand(rfValues, 0x40, 0x30);
			rfWrite = true;
			waitForTheResponse = true;

			testVarRFReadBlock = false;
		}
		else if (testVarRFWriteBlock)
		{
			std::vector<byte> inputVal;
			for(int i = 0x50; i < 0x54; i++)
				inputVal.push_back(i);

			NFCFrame::instance()->writeBlockCommand(rfValues, 0x40, 0x30, inputVal);
			rfWrite = true;
			waitForTheResponse = true;

			testVarRFWriteBlock = false;
		}
		else if (testVarRFReadMultiple)
		{
			NFCFrame::instance()->readMultipleCommand(rfValues, 0x40, 0x30, 0x01);
			rfWrite = true;
			waitForTheResponse = true;

			testVarRFReadMultiple = false;
		}
		else if (testVarRFWriteMultiple)
		{
			std::vector<byte> inputVal;
			for(int i = 0x50; i < 0x57; i++)
				inputVal.push_back(i);

			NFCFrame::instance()->writeMultipleCommand(rfValues, 0x40, 0x30, 0x01, inputVal);
			rfWrite = true;
			waitForTheResponse = true;

			testVarRFWriteMultiple = false;
		}
		else if (testVarRFReadConfig)
		{
			NFCFrame::instance()->readConfigurationCommand(rfValues, 0x40, 0x05);
			rfWrite = true;
			waitForTheResponse = true;

			testVarRFReadConfig = false;
		}
		else if (testVarRFWriteConfig)
		{
			NFCFrame::instance()->writeConfigurationCommand(rfValues, 0x40, 0x05, 0x02);
			rfWrite = true;
			waitForTheResponse = true;

			testVarRFWriteConfig = false;
		}
		else if (testVarRFReadDynamic)
		{
			NFCFrame::instance()->readDynamicCommand(rfValues, 0x40, 0x06);
			rfWrite = true;
			waitForTheResponse = true;

			testVarRFReadDynamic = false;
		}
		else if (testVarRFWriteDynamic)
		{
			NFCFrame::instance()->writeDynamicCommand(rfValues, 0x40, 0x06, 0x09);
			rfWrite = true;
			waitForTheResponse = true;

			testVarRFWriteDynamic = false;
		}
		else if (testVarRFWriteMessage)
		{
			std::vector<byte> inputVal;
			for(int i = 0x50; i < 0x80; i++)
				inputVal.push_back(i);

			NFCFrame::instance()->writeMessageCommand(rfValues, 0x40, inputVal.size() - 1, inputVal);
			rfWrite = true;
			waitForTheResponse = true;

			testVarRFWriteMessage = false;
		}
		else if (testVarRFReadLength)
		{
			NFCFrame::instance()->readMessageLengthCommand(rfValues, 0x40);
			rfWrite = true;
			waitForTheResponse = true;
			waitForAction();

			testVarRFReadLength = false;
		}
		else if (testVarRFReadMessage)
		{
			NFCFrame::instance()->readMessageCommand(rfValues, 0x40, 0x00, 0x30);
			rfWrite = true;
			waitForTheResponse = true;
			waitForAction();

			testVarRFReadMessage = false;
		}
		//Mailbox tests
		else if (testVarMailBox1)
		{
			// Configuration command MB_MODE set to 0x01 (normally this would be initialized)
			NFCFrame::instance()->writeConfigurationCommand(rfValues, 0x40, 0x0d, 0x01);
			rfWrite = true;
			waitForTheResponse = true;
			waitForAction();

			// we get the values of the dyn. register EH_CTRL_Dyn and see if the V_ON and RF_ON match
			// here, we assume that they do, in standard application additional steps would be set
			NFCFrame::instance()->readDynamicCommand(rfValues, 0x40, 0x02);
			rfWrite = true;
			waitForTheResponse = true;
			waitForAction();

			// we get the values of the dyn. register MB_CTRL_DYN and see if the if the FTM is reset
			// if it is, then we can write
			NFCFrame::instance()->readDynamicCommand(rfValues, 0x40, 0x06);
			rfWrite = true;
			waitForTheResponse = true;
			waitForAction();

			// write the value inside the register to set it to be FTM
			NFCFrame::instance()->writeDynamicCommand(rfValues, 0x40, 0x06, 0x01);
			rfWrite = true;
			waitForTheResponse = true;
			waitForAction();

			// check if the value has been set correctly
			NFCFrame::instance()->readDynamicCommand(rfValues, 0x40, 0x06);
			rfWrite = true;
			waitForTheResponse = true;
			waitForAction();

			// read message length to check if it is correct
			NFCFrame::instance()->readMessageLengthCommand(rfValues, 0x40);
			rfWrite = true;
			waitForTheResponse = true;
			waitForAction();

			// read message to check if it is empty how it should be
			NFCFrame::instance()->readMessageCommand(rfValues, 0x40, 0x00, 0x80);
			rfWrite = true;
			waitForTheResponse = true;
			waitForAction();

			//cout << "\nTimestamp begin: " << sc_time_stamp() << endl;

			// write from RF to the Mailbox
			std::vector<byte> inputVal;
			for(int i = 0x00; i < 0x20; i++)
				inputVal.push_back(i);

			NFCFrame::instance()->writeMessageCommand(rfValues, 0x40, inputVal.size() - 1, inputVal);
			rfWrite = true;
			waitForTheResponse = true;
			waitForAction();

			// Read if the RF has put a message, if yes, then continue
			memAddr = 0x2006;
			readValSize = 1;
			readOp = true;
			randomRead = true;
			wait(100, SC_US);

			if(memValues.size() > 0)
			{
				if(memValues.front() == 0x05)
					cout << ">>>>> RF HAS PUT ITS MESSAGE" << endl;
				memValues.pop();
			}

			// Read the size of the message that is put
			memAddr = 0x2007;
			readValSize = 1;
			readOp = true;
			randomRead = true;

			wait(100, SC_US);

			byte sizeOfMessage = memValues.front();
			memValues.pop();

			// Read the mailbox message by I2C that is put earlier by RF
			memAddr = 0x2008;
			readValSize = sizeOfMessage;
			readOp = true;
			randomRead = true;

			wait(3000, SC_US);

			cout << "\n>>>>> Values that are received: ";
			for(size_t i = 0; i < sizeOfMessage; i++)
			{
				cout << (int)memValues.front() << " ";
				memValues.pop();
			}
			cout << endl;

			//cout << "TImestamp: " << sc_time_stamp() << endl;

			testVarMailBox1 = false;

		}
		else if (testVarMailBox2)
		{
			// Configuration command MB_MODE set to 0x01 (normally this would be initialized)
			memValues.push(1);
			memAddr = 0x0d;
			writeOp = true;
			systemMode = true;
			//waitForI2CAction();

			wait(100, SC_US);
			//memValues.pop();

			// we get the values of the dyn. register EH_CTRL_Dyn and see if the V_ON and RF_ON match
			// here, we assume that they do, in standard application additional steps would be set
			memAddr = 0x2002;
			readValSize = 1;
			readOp = true;
			randomRead = true;
			systemMode = false;
			//waitForI2CAction();

			wait(100, SC_US);

			memValues.pop();

			// we get the values of the dyn. register MB_CTRL_DYN and see if the if the FTM is reset
			// if it is, then we can write
			memAddr = 0x2006;
			readValSize = 1;
			readOp = true;
			randomRead = true;
			systemMode = false;
			//waitForI2CAction();

			wait(100, SC_US);

			memValues.pop();

			// write the value inside the register to set it to be FTM
			memValues.push(0x01);
			memAddr = 0x2006;
			writeOp = true;
			systemMode = false;
			//waitForI2CAction();

			wait(100, SC_US);

			// check if the value has been set correctly
			memAddr = 0x2006;
			readValSize = 1;
			readOp = true;
			randomRead = true;
			systemMode = false;
			//waitForI2CAction();

			wait(100, SC_US);

			memValues.pop();

			// read message length to check if it is correct
			memAddr = 0x2007;
			readValSize = 1;
			readOp = true;
			randomRead = true;
			systemMode = false;
			//waitForI2CAction();

			wait(100, SC_US);

			memValues.pop();

			// Read the mailbox message by I2C that is put earlier by RF
			memAddr = 0x2008;
			readValSize = 1;
			readOp = true;
			randomRead = true;
			//waitForI2CAction();

			wait(100, SC_US);

			memValues.pop();
			for(int i = 0x00; i < 0x20; i++)
				memValues.push(i);

			memAddr = 0x2008;
			writeOp = true;
			systemMode = false;
			//waitForI2CAction();

			wait(2000, SC_US);

			// Read the mailbox message by I2C that is put earlier by RF
			memAddr = 0x2008;
			readValSize = 0x20;
			readOp = true;
			randomRead = true;
			//waitForI2CAction();

			wait(3000, SC_US);

			cout << "\n>>>>> Values that are received: ";
			for(size_t i = 0; i < 0x20; i++)
			{
				cout << (int)memValues.front() << " ";
				memValues.pop();
			}
			cout << endl;

			// read message length to check if it is correct
			NFCFrame::instance()->readMessageLengthCommand(rfValues, 0x40);
			rfWrite = true;
			waitForTheResponse = true;
			waitForAction();

			//wait(700, SC_US);

			byte sizeOfMessage = resultRFValues.back();
			resultRFValues.pop_back();

			// read message to check if it is empty how it should be
			NFCFrame::instance()->readMessageCommand(rfValues, 0x40, 0x00, sizeOfMessage - 1);
			rfWrite = true;
			waitForTheResponse = true;
			waitForAction();

			//wait(3000, SC_US);

			cout << "\n>>>>> Values that are received: ";
			for(byte i = 0; i < sizeOfMessage; i++)
			{
				cout << (int)resultRFValues.at(i) << " ";
			}
			cout << endl;

			testVarMailBox2 = false;
		}
		else if (testVarMailBox3)
		{

		}
		else if (testVarHarvesting)
		{
			NFCFrame::instance()->writeConfigurationCommand(rfValues, 0x40, 0x02, 0x00);
			rfWrite = true;
			waitForTheResponse = true;
			waitForAction();

			testVarHarvesting = false;
			testVarMailBox2 = true;
		}

	}
}

void nfc_testbench::memoryControl()
{
	byte memBuffer = 0;
	int iter = 7;

	while(true)
	{
		wait();

		if(writeOp)
		{
			// write op
			if(memValues.size() > 0)
			{
				writeInMemory(memBuffer, iter);
			}
		}
		else if(readOp)
		{
			// read op
			readFromMemory(memBuffer, iter);
		}
		else if(sleepOp)
		{
			// sleep op
			setMemToSleep(memBuffer, iter);
		}
	}
}

void nfc_testbench::writeInMemory(byte &memBuffer, int &iter)
{
	switch(opStates)
	{
		case start:
			//wait(10, SC_US);
			enable.write(1);
			opStates = dev_addr;

			if(systemMode)
			{
				memBuffer = 0xae;
			}
			else
			{
				// 10100110 - device address and instruction
				memBuffer = 0xa6;
			}

			break;

		case dev_addr:

			if(iter < 0)
			{
				sda_out.write(1);

				if (sleepState)
				{
					//wait();
					cout << "\nSLEEP STATE WRITE" << endl;
					enable.write(0);
					opStates = start;
					iter = 7;
					sleepState = false;
				}
				else
				{
					opStates = mem_addr;
					memBuffer = 0;
					iter = 15;
				}
			}
			else
			{
				sda_out.write((memBuffer >> iter) & 1);
				iter--;
			}

			break;

		case mem_addr:

			if(iter < 0)
			{
				sda_out.write(1);
				opStates = rd_wr;
				memBuffer = memValues.front();
				iter = 7;
			}
			else
			{
				/*if(iter == 16)
					memAddr = 100;*/

				sda_out.write((memAddr >> iter) & 1);
				iter--;
			}

			break;

		case rd_wr:

			if(iter < 0)
			{
				sda_out.write(1);

				if (memValues.size() == 1)
				{
					opStates = stop;
					memBuffer = 0;
					//memAddr = 0;
					iter = 7;
				}
				else
				{
					//writeIter--;
					memValues.pop();
					iter = 7;
					memBuffer = memValues.front();
				}
			}
			else
			{
				sda_out.write((memBuffer >> iter) & 1);
				iter--;
			}

			break;

		case stop:
			memValues.pop();
			enable.write(0);
			opStates = start;
			writeOp = false;

			break;

		default:
			opStates = start;

	}
}

void nfc_testbench::readFromMemory(byte &memBuffer, int &iter)
{
	switch(opStates)
	{
		case start:
			wait();
			//wait();
			enable.write(1);
			opStates = dev_addr;

			if (randomRead)
			{
				if(systemMode)
				{
					memBuffer = 0xae;
				}
				else
				{
					// 10100110 - device address and instruction
					memBuffer = 0xa6;
				}
			}
			else
			{
				if(systemMode)
				{
					memBuffer = 0xaf;
				}
				else
				{
					// 10100110 - device address and instruction
					memBuffer = 0xa7;
				}
			}

			break;

		case dev_addr:

			if(iter < 0)
			{
				sda_out.write(1);

				if (sleepState)
				{
					//wait();
					cout << "\nSLEEP STATE READ" << endl;
					enable.write(0);
					opStates = start;
					iter = 7;
					sleepState = false;
				}
				else
				{
					memBuffer = 0;

					if(randomRead)
					{
						opStates = mem_addr;
						iter = 15;
					}
					else
					{
						opStates = rd_wr;
						iter = 7;

						// wait one cycle to synchronise
						//wait();
					}
				}
			}
			else
			{
				sda_out.write((memBuffer >> iter) & 1);
				iter--;
			}

			break;

		case mem_addr:

			if(iter < 0)
			{
				sda_out.write(1);

				randomRead = false;
				enable.write(0);
				opStates = start;

				memBuffer = 0;
				iter = 7;

				// wait one cycle to synchronise
				wait();
			}
			else
			{
				//if(iter == 16)
					//memAddr = 100;

				sda_out.write((memAddr >> iter) & 1);
				iter--;
			}

			break;

		case rd_wr:

			if(iter < 0)
			{
				sda_out.write(1);
				cout << "\nMemory buffer: " << (int)memBuffer << " [" << memAddr << "]" << endl;
				memValues.push(memBuffer);

				if (memValues.size() == readValSize)
				{
					opStates = stop;
				}
				else
				{
					memAddr++;
				}

				memBuffer = 0;
				iter = 7;
			}
			else
			{
				byte cur_val = sda_in.read();
				memBuffer |= (cur_val << iter);
				iter--;
			}

			break;

		case stop:
			enable.write(0);
			opStates = start;

			readOp = false;

			break;

		default:
			opStates = start;

	}
}

void nfc_testbench::setMemToSleep(byte &memBuffer, int &iter)
{
	switch(opStates)
	{
		case start:
			wait();
			enable.write(1);
			opStates = dev_addr;
			// 10101100 - device address and instruction
			memBuffer = 0xf8;

			break;

		case dev_addr:

			if(iter < 0)
			{
				sda_out.write(1);
				opStates = rd_wr;
				memBuffer = 0x86;
				iter = 7;
			}
			else
			{
				sda_out.write((memBuffer >> iter) & 1);
				iter--;
			}

			break;

		case rd_wr:

			if(iter < 0)
			{
				sda_out.write(1);

				if (sda_in.read() == 1)
				{
					sleepState = true;
				}

				opStates = stop;
				memBuffer = 0;
				iter = 7;
			}
			else
			{
				sda_out.write((memBuffer >> iter) & 1);
				iter--;
			}

			break;

		case stop:
			enable.write(0);
			opStates = start;

			/*testVarSleepMode = false;
			if(numOfTest == 0)
				testVarWrite2 = true;*/
			sleepOp = false;

			break;

		default:
			opStates = start;

	}
}

void nfc_testbench::rfControl()
{
	byte buffer = 0;
	int iter = 7;

	while(true)
	{
		wait();

		if(rfWrite)
		{
			// write op
			if(rfValues.size() > 0)
			{
				send_frame(buffer, iter);
			}
		}
	}
}


void nfc_testbench::rfInput()
{
	nfc::RF_Frame_Response *frame_resp = NULL;
	std::vector<bool> input;

	int err_flag;

	while(true)
	{
		wait();

		if(enable_rf_in.read())
		{
			if(frame_resp == NULL)
			{
				frame_resp = new nfc::RF_Frame_Response();
				//waitForTheResponse = false;
				input.clear();
			}
			else
			{
				//cout << "Anntena receive: " << antenna_in.read() << endl << endl;
				input.push_back(antenna_in.read());
			}
		}
		else
		{
			if(frame_resp != NULL)
			{
				if(frame_resp->setFrame(input, err_flag))
				{
					// TODO: set a function to process the received frame (make response)

					cout << "\nFrame successfully received!" << endl;

					// Correctly respond depending if it is an error frame or not
					if(frame_resp->getErrChk())
					{
						cout << "A ERROR Frame: " << hex << (int)frame_resp->getErrVal() << endl;
					}
					else
					{
						cout << "A STATUS Frame: ";

						if(frame_resp->getData().size() != 0)
						{
							int j = 0;
							byte tempVal = 0;

							resultRFValues.clear();

							for(size_t k = 0; k < (frame_resp->getData().size() / 8); k++)
							{
								for(int i = 7; i >= 0; i--)
								{
									tempVal |= (frame_resp->getData().at(j) << i);
									j++;
								}

								resultRFValues.push_back(tempVal);
								cout << (int)tempVal << " ";
								tempVal = 0;
							}
							cout << endl;
						}
						else
						{
							cout << " NO ADDITIONAL DATA TO BE SHOWN" << endl;
						}

					}
				}
				else
				{
					// TODO: set a function to process the error (crc or something else...)
					cout << "\nFrame received an error!" << endl;
				}

				frame_resp = NULL;
				waitForTheResponse = false;

				// so-called time t2, before a new request can be sent
				wait(310, SC_US);

				setNextTest();
				cout << "\nTimestamp end: " << sc_time_stamp() << endl;
				//testVarRFReadBlock = true;
			}
		}
	}
}


void nfc_testbench::send_frame(byte &buffer, int &iter)
{
	switch(rfStates)
	{
		case start_rf:

			// inserting delay for SOF
			frameDelay();

			enable_rf_out.write(1);
			buffer = rfValues.front();

			rfStates = operation_rf;

			break;

		case operation_rf:

			if(iter < 0)
			{
				if (rfValues.size() == 1)
				{
					rfStates = stop_rf;
					buffer = 0;
					iter = 7;
				}
				else
				{
					rfValues.pop();
					iter = 7;
					buffer = rfValues.front();
				}
			}
			else
			{
				//cout << "Anntena send: " << (int)((buffer >> iter) & 1) << endl;
				antenna_out.write((buffer >> iter) & 1);
				iter--;
			}

			break;

		case stop_rf:

			rfValues.pop();
			enable_rf_out.write(0);
			rfStates = start_rf;
			rfWrite = false;

			buffer = 0;
			iter = 7;

			// inserting delay for EOF
			frameDelay();

			rfResendFrame();

			break;

		default:
			rfStates = start_rf;
	}
}

void nfc_testbench::rfResendFrame()
{
	// taken for the 100% modulation, t1 + tSOF
	wait(320, SC_US);
	frameDelay();

	if(waitForTheResponse)
	{
		//testVarRFSend1 = true;
	}
}

void nfc_testbench::setNextTest()
{
	switch(rfNextValue)
	{
		case 0:

			break;

		case 1:
			testVarRFReadBlock = true;
			rfNextValue = 0;
			break;

		case 2:
			testVarRFReadMultiple = true;
			rfNextValue = 0;
			break;

		case 3:
			testVarRFReadConfig = true;
			rfNextValue = 0;
			break;

		case 4:
			testVarRFReadDynamic = true;
			rfNextValue = 0;
			break;

		case 5:
			testVarRFReadLength = true;
			rfNextValue = 6;
			break;

		case 6:
			testVarRFReadMessage = true;
			rfNextValue = 0;
			break;
	}
}

// The delay time is the same for SOF and EOF
void nfc_testbench::frameDelay()
{
	if(RF_BIT_OUTPUT_RATE == BitRate_26Kbit)
	{
		wait(76, SC_US);
	}
	else
	{
		wait(302, SC_US);
	}
}

void nfc_testbench::waitForAction()
{
	while(true)
	{
		if(!waitForTheResponse)
			break;
		wait(100, SC_US);
	}
}

void nfc_testbench::waitForI2CAction()
{
	while(true)
	{
		if(!writeOp && !readOp)
			break;
		wait(10, SC_US);
	}
}

} /* namespace nfc */
