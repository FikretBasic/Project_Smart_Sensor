/*
 * NFCMemory.cpp
 *
 *  Created on: 23 May 2018
 *      Author: its2016
 */

#include "NFCMemory.h"

namespace nfc {

NFCMemory::NFCMemory()
{
	// Initialization

	fast_transfer_mode = false;
	currUserAddr = 0x01;
	currMailBoxAddr = 0x00;
	counterBoxAddr = 0x00;

	currSystemAddr = 0x00;
	currDynAddr = 0x00;

	for (uint16_t i = 0; i < 256; i++)
	{
		mailbox[i] = 0xff;
	}

	// initiate dynamic memory of register 0x02 (EH_CTRL_Dyn)
	// set V_ON and RF_ON and change based on the voltage input
	dynamicMemory[2] = 0x0e;

	// setting the EH_MODE to be disabled at start
	systemMemory[2] = 0x01;

	// initiating user memory for testing purposes
	userMemory[124] = 5;
	userMemory[125] = 135;

	userMemory[192] = 10;
	userMemory[193] = 20;
	userMemory[194] = 30;
	userMemory[195] = 45;
	userMemory[196] = 50;
}

NFCMemory::~NFCMemory() {}

bool NFCMemory::writeInMemory(byte data)
{
	if(!fast_transfer_mode && currMailBoxAddr < 256)
	{
		mailbox[currMailBoxAddr] = data;
		currMailBoxAddr++;
		return true;
	}
	else
	{
		return false;
	}
}

// todo: Set which parts can be written with I2C and which not
bool NFCMemory::writeInSystem(byte data)
{
	systemMemory[currSystemAddr] = data;
	return true;
}

bool NFCMemory::writeInDynamic(byte data)
{
	dynamicMemory[currDynAddr] = data;
	return true;
}

bool NFCMemory::writeInMailbox(byte data)
{
	if(fast_transfer_mode)
	{
		mailbox[currMailBoxAddr] = data;
		currMailBoxAddr++;
		return true;
	}
	else
	{
		return false;
	}
}

void NFCMemory::insertUserMemory()
{
	for (uint16_t i = 0; i < currMailBoxAddr; i++)
	{
		userMemory[currUserAddr] = mailbox[i];
		currUserAddr++;
	}

	currMailBoxAddr = 0x00;
}

bool NFCMemory::insertPeriodicallyMemory()
{
	if (counterBoxAddr < currMailBoxAddr)
	{
		userMemory[currUserAddr] = mailbox[counterBoxAddr];
		currUserAddr++;
		counterBoxAddr++;

		return true;
	}
	else
	{
		currMailBoxAddr = 0x00;
		counterBoxAddr = 0x00;

		return false;
	}
}

void NFCMemory::resetMailboxAddr()
{
	this->currMailBoxAddr = 0x00;
}

void NFCMemory::resetMailboxVal()
{
	for(int i = 0; i < 256; i++)
	{
		mailbox[i] = 0x00;
	}
}

byte NFCMemory::readFromMemory()
{
	return userMemory[currUserAddr];
}

byte NFCMemory::readFromMailBox()
{
	return mailbox[currMailBoxAddr];
}

byte NFCMemory::readFromSystem()
{
	return systemMemory[currSystemAddr];
}

byte NFCMemory::readFromDynamic()
{
	return dynamicMemory[currDynAddr];
}

void NFCMemory::incrementAddr()
{
	currUserAddr++;
}

void NFCMemory::incrementMailboxAddr()
{
	this->currMailBoxAddr++;
}

void NFCMemory::setCurrentUserAddress(uint16_t currentUserAddress)
{
	this->currUserAddr = currentUserAddress;

	// Since, we assume that the protocol for User or System Memory writing has started
	if(!fast_transfer_mode)
	{
		currMailBoxAddr = 0x00;
	}
}

void NFCMemory::setCurrentMailBoxAddress(uint16_t currMailBoxAddr)
{
	if(fast_transfer_mode)
	{
		this->currMailBoxAddr = currMailBoxAddr;
	}
}

void NFCMemory::setCurrentSystemAddress(uint8_t currentSysAddr)
{
	if(currentSysAddr >= 35)
		this->currSystemAddr = 34;
	else
		this->currSystemAddr = currentSysAddr;
}

void NFCMemory::setCurrentDynamicAddress(uint8_t currentDynAddr)
{
	if(currentDynAddr >= 8)
		this->currDynAddr = 7;
	else
		this->currDynAddr = currentDynAddr;
}

void NFCMemory::writeOutUserMemory()
{
	cout << "\nUSER MEMORY NFC" << endl << endl;
	for (uint16_t i = 0; i < 8192; i++)
	{
		cout << "[" << i << "]" << " " << (int)userMemory[i] << endl;
	}
}

void NFCMemory::setFTMstatus(bool status)
{
	this->fast_transfer_mode = status;
}

bool NFCMemory::getFTMstatus()
{
	return this->fast_transfer_mode;
}

byte NFCMemory::getHarvestRegisterStatus()
{
	return dynamicMemory[0x02];
}

} /* namespace nfc */
