/*
 * RFFrameResponse.h
 *
 *  Created on: 4 Jun 2018
 *      Author: its2016
 */

#ifndef MODULES_NFC_NFC_FRAME_RFFRAMERESPONSE_H_
#define MODULES_NFC_NFC_FRAME_RFFRAMERESPONSE_H_

#include "RFFrameRequest.h"
#include "request_data/RequestData.h"
#include "request_data/RequestInventory.h"

namespace nfc {

class RF_Frame_Response : public RF_Frame
{
public:
	RF_Frame_Response();
	virtual ~RF_Frame_Response();

	bool setFrame(std::vector<bool> &input, int &err_flag);
	bool defineResponse(std::vector<bool> &input, int err_flag, std::vector<byte> addData);
	void setRequestFrame(nfc::RF_Frame_Request *requestFrame);

	bool getErrChk();
	byte getErrVal();
	std::vector<bool> getData();

private:
	uint8_t response_flags;
	std::vector<bool> data;
	Request_Data *paramAndData;

	nfc::RF_Frame_Request *requestFrame;

	std::vector<byte> processData;

	// /TODO Set UID to change in the initializer constructor
	uint64_t UID;
	uint8_t AFI;
	uint8_t DSFID;

	// false = error is not present, true = error is present
	bool err_chk;
	byte err_val;

	void interpretFlags();
	bool interpretCommands();

	void setErrorData(int err_val);
};

} /* namespace nfc */

#endif /* MODULES_NFC_NFC_FRAME_RFFRAMERESPONSE_H_ */
