/*
 * RFFrameResponse.cpp
 *
 *  Created on: 4 Jun 2018
 *      Author: its2016
 */

#include "RFFrameResponse.h"

namespace nfc {

RF_Frame_Response::RF_Frame_Response()
{
	response_flags = 0;
	paramAndData = NULL;
	requestFrame = NULL;

	err_chk = false;
	err_val = 0x00;

	UID = 0xe002260000000000;
	AFI = 0x00;
	DSFID = 0x00;
}

RF_Frame_Response::~RF_Frame_Response() {}

bool RF_Frame_Response::setFrame(std::vector<bool> &input, int &err_flag)
{
	// DEL
	cout << "Received input frame: ";
	for(size_t i = 0; i < input.size(); i++)
	{
		cout << (int)input.at(i) << " ";
	}
	cout << endl;

	int j = 0;

	for(int i = 7; i >= 0; i--)
	{
		response_flags |= (input.at(j) << i);
		j++;
	}

	interpretFlags();

	for(size_t i = j; i < input.size() - 16; i++)
	{
		data.push_back(input.at(i));
		j++;
	}

	if(this->err_chk)
	{
		int k = 0;

		for(int i = 7; i >= 0; i--)
		{
			err_val |= (input.at(k) << i);
			k++;
		}
	}

	crc = 0;
	for (int i = 15; i >= 0; i--)
	{
		uint16_t tempVal = input.at(j);
		crc |= (tempVal << i);
		j++;
	}

	std::vector<byte> dataForCRC;
	dataForCRC.push_back(response_flags);

	byte tempByte = 0;
	j = 7;
	for (size_t i = 0; i < data.size(); i++)
	{
		tempByte |= (data.at(i) << j);
		j--;

		if(j == -1)
		{
			dataForCRC.push_back(tempByte);
			j = 7;
			tempByte = 0;
		}
	}

	uint16_t tempCRC = calculateCRC(dataForCRC);
	//cout << "CRC Rec: " << crc << endl;
	//cout << "CRC Temp: " << tempCRC << endl;

	return (crc == tempCRC);
}

bool RF_Frame_Response::defineResponse(std::vector<bool> &input, int err_flag, std::vector<byte> addData)
{
	input.clear();
	std::vector<byte> dataForCRC;

	// For flags, there is only error flag of importance, everything else is 0
	for(int i = 0; i < 7; i++)
	{
		input.push_back(0);
	}

	input.push_back((bool)err_flag);

	// if it is an error
	if(err_flag != 0x00)
	{
		dataForCRC.push_back(0x01);
		setErrorData(err_flag);
	}
	else
	{
		dataForCRC.push_back(0x00);
		processData = addData;
		interpretCommands();
	}

	//cout << "Received data: ";
	for(size_t i = 0; i < data.size(); i++)
	{
		input.push_back(data.at(i));
		//cout << data.at(i) << " ";
	}
	//cout << endl;


	// ***** CRC Calculation *****

	byte tempByte = 0;
	int j = 7;
	for (size_t i = 0; i < data.size(); i++)
	{
		tempByte |= (data.at(i) << j);
		j--;

		if(j == -1)
		{
			dataForCRC.push_back(tempByte);
			j = 7;
			tempByte = 0;
		}
	}

	uint16_t tempCRC = this->calculateCRC(dataForCRC);
	int iter = 15;

	for(size_t i = 0; i < 16; i++)
	{
		input.push_back((tempCRC >> iter) & 1);
		iter--;
	}

	return true;
}

void RF_Frame_Response::setRequestFrame(nfc::RF_Frame_Request *requestFrame)
{
	this->requestFrame = requestFrame;
}

void RF_Frame_Response::interpretFlags()
{
	err_chk = response_flags & 0x01;
}

bool RF_Frame_Response::interpretCommands()
{
	if(requestFrame == NULL)
	{
		return false;
	}
	else
	{
		data.clear();

		int iter = 7;

		switch(this->requestFrame->getCommand_code())
		{
			// Inventory
			case 0x01:

				for(int i = 0; i < 8; i++)
				{
					data.push_back((DSFID >> iter) & 1);
					iter--;
				}

				iter = 63;

				for(int i = 0; i < 64; i++)
				{
					data.push_back((UID >> iter) & 1);
					iter--;
				}

				break;

			// Read Single Block
			case 0x20:

				iter = 7;

				for(int i = 0; i < 4; i++)
				{
					byte tempVal = processData.at(i);

					for(int j = 0; j < 8; j++)
					{
						data.push_back((tempVal >> iter) & 1);
						iter--;
					}
					iter = 7;
				}

				break;

			// Write Single Block
			case 0x21:
				// Response does not contain additional data
				break;

			// Read Multiple Blocks
			case 0x23:

				for(size_t i = 0; i < processData.size(); i++)
				{
					iter = 7;
					byte tempVal = processData.at(i);

					for(int j = 0; j < 8; j++)
					{
						data.push_back((tempVal >> iter) & 1);
						iter--;
					}
				}

				break;

			// Write Multiple Blocks
			case 0x24:
				// Response does not contain additional data
				break;

			// Read Configuration
			case 0xa0:

				for(size_t i = 0; i < processData.size(); i++)
				{
					iter = 7;
					byte tempVal = processData.at(0);

					for(int j = 0; j < 8; j++)
					{
						data.push_back((tempVal >> iter) & 1);
						iter--;
					}

				}

				break;

			// Write Configuration
			case 0xa1:
				// Response does not contain additional data
				break;

			// Write Message
			case 0xaa:
				// Response does not contain additional data
				break;

			// Read Message Length
			case 0xab:

				for(size_t i = 0; i < processData.size(); i++)
				{
					iter = 7;
					byte tempVal = processData.at(0);

					for(int j = 0; j < 8; j++)
					{
						data.push_back((tempVal >> iter) & 1);
						iter--;
					}
				}

				break;

			// Read Message
			case 0xac:

				for(size_t i = 0; i < processData.size(); i++)
				{
					iter = 7;
					byte tempVal = processData.at(i);

					for(int j = 0; j < 8; j++)
					{
						data.push_back((tempVal >> iter) & 1);
						iter--;
					}
				}

				break;

			// Read Dynamic Configuration
			case 0xad:

				for(size_t i = 0; i < processData.size(); i++)
				{
					iter = 7;
					byte tempVal = processData.at(0);

					for(int j = 0; j < 8; j++)
					{
						data.push_back((tempVal >> iter) & 1);
						iter--;
					}
				}

				break;

			// Write Dynamic Configuration
			case 0xae:
				// Response does not contain additional data
				break;

			default:
				return false;
		}

		return true;
	}
}

void RF_Frame_Response::setErrorData(int err_val)
{
	data.clear();
	int iter = 7;

	for(int i = 0; i < 8; i++)
	{
		data.push_back((err_val >> iter) & 1);
		iter--;
	}
}

bool RF_Frame_Response::getErrChk()
{
	return this->err_chk;
}

byte RF_Frame_Response::getErrVal()
{
	return this->err_val;
}

std::vector<bool> RF_Frame_Response::getData()
{
	return this->data;
}

} /* namespace nfc */
