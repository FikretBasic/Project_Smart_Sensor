/*
 * RFFrame.h
 *
 *  Created on: 4 Jun 2018
 *      Author: its2016
 */

#ifndef MODULES_NFC_RFFRAME_H_
#define MODULES_NFC_RFFRAME_H_

#include "../../Includes.h"

namespace nfc {

#define CRC_NAME			"CRC-CCITT"
#define POLYNOMIAL			0x1021
#define INITIAL_REMAINDER	0xFFFF
#define FINAL_XOR_VALUE		0x0000
#define CHECK_VALUE			0x29B1

#define WIDTH    (8 * sizeof(unsigned short))
#define TOPBIT   (1 << (WIDTH - 1))

struct primjer
{
	uint8_t njest;
};

class RF_Frame {
public:
	RF_Frame();
	virtual ~RF_Frame();

	uint16_t calculateCRC(std::vector<byte> message);

	virtual bool setFrame(std::vector<bool> &input, int &err_flag) = 0;
	virtual void interpretFlags() = 0;
	virtual bool interpretCommands() = 0;

protected:
	uint16_t crc;

private:
	void initializeCRC();
	unsigned short  crcTable[256];
};

} /* namespace nfc */

#endif /* MODULES_NFC_RFFRAME_H_ */
