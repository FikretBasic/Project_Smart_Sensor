/*
 * RFFrame.cpp
 *
 *  Created on: 4 Jun 2018
 *      Author: its2016
 */

#include "RFFrame.h"

namespace nfc {

RF_Frame::RF_Frame() : crc(0)
{
	initializeCRC();
}

RF_Frame::~RF_Frame() {}

void RF_Frame::initializeCRC()
{
	unsigned short remainder;
	int			   dividend;
	unsigned char  bit;

    /*
     * Compute the remainder of each possible dividend.
     */
    for (dividend = 0; dividend < 256; ++dividend)
    {
        /*
         * Start with the dividend followed by zeros.
         */
        remainder = dividend << (WIDTH - 8);

        /*
         * Perform modulo-2 division, a bit at a time.
         */
        for (bit = 8; bit > 0; --bit)
        {
            /*
             * Try to divide the current data bit.
             */
            if (remainder & TOPBIT)
            {
                remainder = (remainder << 1) ^ POLYNOMIAL;
            }
            else
            {
                remainder = (remainder << 1);
            }
        }

        /*
         * Store the result into the table.
         */
        crcTable[dividend] = remainder;
    }
}

uint16_t RF_Frame::calculateCRC(std::vector<byte> message)
{
    unsigned short remainder = INITIAL_REMAINDER;
    unsigned char  data;

    /*
     * Divide the message by the polynomial, a byte at a time.
     */
    for (size_t i = 0; i < message.size(); ++i)
    {
        data = message.at(i) ^ (remainder >> (WIDTH - 8));
  		remainder = crcTable[data] ^ (remainder << 8);
    }

    /*
     * The final remainder is the CRC.
     */
    return (remainder ^ FINAL_XOR_VALUE);
}

} /* namespace nfc */
