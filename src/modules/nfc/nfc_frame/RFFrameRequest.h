/*
 * RFFrameRequest.h
 *
 *  Created on: 4 Jun 2018
 *      Author: its2016
 */

#ifndef MODULES_NFC_NFC_FRAME_RFFRAMEREQUEST_H_
#define MODULES_NFC_NFC_FRAME_RFFRAMEREQUEST_H_

#include "RFFrame.h"
#include "request_data/RequestData.h"
#include "request_data/RequestInventory.h"
#include "request_data/RequestReadSingleBlock.h"
#include "request_data/RequestWriteSingleBlock.h"
#include "request_data/RequestReadMultipleBlock.h"
#include "request_data/RequestWriteMultipleBlock.h"
#include "request_data/RequestReadConfiguration.h"
#include "request_data/RequestWriteConfiguration.h"
#include "request_data/RequestReadDynamic.h"
#include "request_data/RequestWriteDynamic.h"
#include "request_data/RequestWriteMessage.h"
#include "request_data/RequestMessageLength.h"
#include "request_data/RequestReadMessage.h"

namespace nfc {

class RF_Frame_Request : public RF_Frame
{
public:
	RF_Frame_Request();
	virtual ~RF_Frame_Request();

	bool setFrame(std::vector<bool> &input, int &err_flag);

	uint8_t getRequest_flags();
	uint8_t getCommand_code();

	bool getSubCarrier();
	bool getAfiPresent();
	uint8_t getNbSlots();
	bool getSelectFlag();
	bool getAddressFlag();
	Request_Data * getParamAndData();

private:
	uint8_t request_flags;
	uint8_t command_code;
	std::vector<bool> data;
	Request_Data *paramAndData;

	// false = one, true = two
	bool subcarrier;
	// false = AFI is not presented, true = otherwise
	bool afiPresent;
	// false = 16, true = 1
	uint8_t nbSlots;
	// false = any device regardless of the select feature, true = only the device in select state
	bool selectFlag;
	// false = request is not addressed, there is no UID, request executed by all devices;
	// true = request is addressed, only the device with specific UID will execute the request
	bool addressFlag;

	void interpretFlags();
	bool interpretCommands();
};

} /* namespace nfc */

#endif /* MODULES_NFC_NFC_FRAME_RFFRAMEREQUEST_H_ */
