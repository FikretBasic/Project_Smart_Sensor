/*
 * RFFrameRequest.cpp
 *
 *  Created on: 4 Jun 2018
 *      Author: its2016
 */

#include "RFFrameRequest.h"

namespace nfc {

RF_Frame_Request::RF_Frame_Request()
{
	request_flags = 0;
	command_code = 0;
	paramAndData = NULL;

	subcarrier = false;
	afiPresent = false;
	nbSlots = 16;
	selectFlag = false;
	addressFlag = false;
}

RF_Frame_Request::~RF_Frame_Request() {}

bool RF_Frame_Request::setFrame(std::vector<bool> &input, int &err_flag)
{
	int j = 0;
	err_flag = 0x00;

	for(int i = 7; i >= 0; i--)
	{
		request_flags |= (input.at(j) << i);
		j++;
	}

	//cout << "Request flags: " << hex << (int)request_flags << endl;
	interpretFlags();

	for(int i = 7; i >= 0; i--)
	{
		command_code |= (input.at(j) << i);
		j++;
	}

	//cout << "Command code: " << hex << (int)command_code << endl;

	if(!interpretCommands())
	{
		err_flag = 0x01;
	}

	cout << "Received data: ";
	for(size_t i = j; i < input.size() - 16; i++)
	{
		data.push_back(input.at(i));
		cout << data.back() << " ";
		j++;
	}
	cout << endl;

	crc = 0;
	for (int i = 15; i >= 0; i--)
	{
		uint16_t tempVal = input.at(j);
		crc |= (tempVal << i);
		j++;
	}

	if(paramAndData != NULL)
	{
		if(!paramAndData->setData(data, afiPresent))
		{
			err_flag = 0x0f;
			//return false;
		}
	}

	std::vector<byte> dataForCRC;
	dataForCRC.push_back(request_flags);
	dataForCRC.push_back(command_code);

	byte tempByte = 0;
	j = 7;
	for (size_t i = 0; i < data.size(); i++)
	{
		tempByte |= (data.at(i) << j);
		j--;

		if(j == -1)
		{
			dataForCRC.push_back(tempByte);
			j = 7;
			tempByte = 0;
		}
	}

	uint16_t tempCRC = calculateCRC(dataForCRC);

	//cout << "CRC: " << (int)crc << endl;
	//cout << "Temp CRC: " << (int)tempCRC << endl;

	if (crc != tempCRC)
	{
		err_flag = 0x0f;
		return false;
	}
	else
	{
		return true;
	}
}

// From the documentation, page 104
void RF_Frame_Request::interpretFlags()
{
	// bit 1 - check if there is one or two sub-carriers
	bool bitCheck;
	bitCheck = request_flags & 0x01;
	subcarrier = bitCheck;

	// bit 2 - check the data rate for the output values
	bitCheck = request_flags & 0x02;

	if(bitCheck)
		RF_BIT_OUTPUT_RATE = BitRate_26Kbit;
	else
		RF_BIT_OUTPUT_RATE = BitRate_6600bit;

	// bit 3 - check which type of inventory flag to analyse
	bitCheck = request_flags & 0x04;

	// bit 4 - is reserved for future extensions, no current use

	if(bitCheck)
	{
		// bit 5 - check if the AFI field is present
		bitCheck = request_flags & 0x10;
		afiPresent = bitCheck;

		// bit 6 - check the number of certain slots
		bitCheck = request_flags & 0x20;
		if(bitCheck)
			nbSlots = 1;
		else
			nbSlots = 16;

		// bit 7 and bit 8 do not make a difference
	}
	else
	{
		// bit 5 - check if the request should be executed only by the selected devices
		bitCheck = request_flags & 0x10;
		selectFlag = bitCheck;

		// bit 6 - check if the request should be executed based on the address
		bitCheck = request_flags & 0x20;
		addressFlag = bitCheck;

		// bit 7 and bit 8 do not make a difference
	}
}

bool RF_Frame_Request::interpretCommands()
{
	switch(command_code)
	{
		// Inventory
		case 0x01:
			paramAndData = new Request_Inventory();
			break;

		// Read Single Block
		case 0x20:
			paramAndData = new RequestReadSingleBlock();
			break;

		// Write Single Block
		case 0x21:
			paramAndData = new RequestWriteSingleBlock();
			break;

		// Read Multiple Blocks
		case 0x23:
			paramAndData = new RequestReadMultipleBlock();
			break;

		// Write Multiple Blocks
		case 0x24:
			paramAndData = new RequestWriteMultipleBlock();
			break;

		// Read Configuration
		case 0xa0:
			paramAndData = new RequestReadConfiguration();
			break;

		// Write Configuration
		case 0xa1:
			paramAndData = new RequestWriteConfiguration();
			break;

		// Write Message
		case 0xaa:
			paramAndData = new RequestWriteMessage();
			break;

		// Read Message Length
		case 0xab:
			paramAndData = new RequestMessageLength();
			break;

		// Read Message
		case 0xac:
			paramAndData = new RequestReadMessage();
			break;

		// Read Dynamic Configuration
		case 0xad:
			paramAndData = new RequestReadDynamic();
			break;

		// Write Dynamic Configuration
		case 0xae:
			paramAndData = new RequestWriteDynamic();
			break;

		default:
			paramAndData = NULL;
			cout << "Wrong Command!" << endl;
			return false;
	}

	if(paramAndData != NULL)
		cout << "\nCurrent command: " << paramAndData->returnCommandName() << endl;

	return true;
}


uint8_t RF_Frame_Request::getRequest_flags()
{
	return this->request_flags;
}

uint8_t RF_Frame_Request::getCommand_code()
{
	return this->command_code;
}

bool RF_Frame_Request::getSubCarrier()
{
	return this->subcarrier;
}

bool RF_Frame_Request::getAfiPresent()
{
	return this->afiPresent;
}

uint8_t RF_Frame_Request::getNbSlots()
{
	return this->nbSlots;
}

bool RF_Frame_Request::getSelectFlag()
{
	return this->selectFlag;
}

bool RF_Frame_Request::getAddressFlag()
{
	return this->addressFlag;
}

Request_Data * RF_Frame_Request::getParamAndData()
{
	return this->paramAndData;
}

} /* namespace nfc */
