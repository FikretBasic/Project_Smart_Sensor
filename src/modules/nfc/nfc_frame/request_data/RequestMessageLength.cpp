/*
 * RequestMessageLength.cpp
 *
 *  Created on: 27 Jun 2018
 *      Author: its2016
 */

#include "RequestMessageLength.h"

namespace nfc {

RequestMessageLength::RequestMessageLength() {}

RequestMessageLength::~RequestMessageLength() {}

std::string RequestMessageLength::returnCommandName()
{
	return "Read Message Length";
}

bool RequestMessageLength::setData(std::vector<bool> data, bool optional)
{
	size_t j = 0;
	std::vector<byte> tempVec;
	byte tempVal = 0;

	// IC Mfg Code
	for(int i = 7; i >= 0; i--)
	{
		tempVal |= (data.at(j) << i);
		j++;
	}
	tempVec.push_back(tempVal);
	dataVector.push_back(tempVec);

	tempVec.clear();
	tempVal = 0;

	// optional UID
	if(optional)
	{
		try
		{
			for(int k = 0; k < 8; k++)
			{
				for(int i = 7; i >= 0; i--)
				{
					tempVal |= (data.at(j) << i);
					j++;
				}
				tempVec.push_back(tempVal);
				tempVal = 0;
			}
			dataVector.push_back(tempVec);
		}
		catch(int e)
		{
			return false;
		}

		tempVec.clear();
		tempVal = 0;
	}

	return true;
}

} /* namespace nfc */
