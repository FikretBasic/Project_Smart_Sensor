/*
 * RequestWriteMultipleBlock.cpp
 *
 *  Created on: 23 Jun 2018
 *      Author: its2016
 */

#include "RequestWriteMultipleBlock.h"

namespace nfc {

RequestWriteMultipleBlock::RequestWriteMultipleBlock() {}

RequestWriteMultipleBlock::~RequestWriteMultipleBlock() {}

std::string RequestWriteMultipleBlock::returnCommandName()
{
	return "Write Multiple Blocks";
}

bool RequestWriteMultipleBlock::setData(std::vector<bool> data, bool optional)
{
	size_t j = 0;
	std::vector<byte> tempVec;
	byte tempVal = 0;

	// optional UID
	if(optional)
	{
		try
		{
			for(int k = 0; k < 8; k++)
			{
				for(int i = 7; i >= 0; i--)
				{
					tempVal |= (data.at(j) << i);
					j++;
				}
				tempVec.push_back(tempVal);
				tempVal = 0;
			}
			dataVector.push_back(tempVec);
		}
		catch(int e)
		{
			return false;
		}
	}

	tempVec.clear();
	tempVal = 0;

	// Starting block number
	for(int i = 7; i >= 0; i--)
	{
		tempVal |= (data.at(j) << i);
		j++;
	}
	tempVec.push_back(tempVal);
	dataVector.push_back(tempVec);

	tempVec.clear();
	tempVal = 0;

	// Number of blocks
	for(int i = 7; i >= 0; i--)
	{
		tempVal |= (data.at(j) << i);
		j++;
	}
	tempVec.push_back(tempVal);
	dataVector.push_back(tempVec);

	tempVec.clear();
	int numOfBlocks = 4*(tempVal + 1);

	// N-byte values
	for(int k = 0; k < numOfBlocks; k++)
	{
		tempVal = 0;
		for(int i = 7; i >= 0; i--)
		{
			tempVal |= (data.at(j) << i);
			j++;
		}
		tempVec.push_back(tempVal);
	}
	dataVector.push_back(tempVec);

	return true;
}

} /* namespace nfc */
