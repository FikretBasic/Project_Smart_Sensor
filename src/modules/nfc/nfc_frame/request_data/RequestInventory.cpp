/*
 * RequestInventory.cpp
 *
 *  Created on: 4 Jun 2018
 *      Author: its2016
 */

#include "RequestInventory.h"

namespace nfc {

Request_Inventory::Request_Inventory() {}

Request_Inventory::~Request_Inventory() {}

std::string Request_Inventory::returnCommandName()
{
	return "Inventory";
}

bool Request_Inventory::setData(std::vector<bool> data, bool optional)
{
	size_t j = 0;
	std::vector<byte> tempVec;
	byte tempVal = 0;

	// optional AFI
	if(optional)
	{
		try
		{
			for(int i = 7; i >= 0; i--)
			{
				tempVal |= (data.at(j) << i);
				j++;
			}
			tempVec.push_back(tempVal);
			dataVector.push_back(tempVec);
		}
		catch(int e)
		{
			return false;
		}

	}

	// mask length
	tempVal = 0;
	if(tempVec.size() != 0)
		tempVec.pop_back();

	for(int i = 7; i >= 0; i--)
	{
		tempVal |= (data.at(j) << i);
		j++;
	}
	tempVec.push_back(tempVal);
	dataVector.push_back(tempVec);

	byte masklength = tempVal;
	tempVal = 0;
	tempVec.pop_back();
	byte count = 0;

	// mask value
	for(int i = masklength - 1; i >= 0; i--)
	{
		tempVal |= (data.at(j) << i);
		j++;
		count++;

		if (count == 8)
		{
			count = 0;
			tempVec.push_back(tempVal);
			tempVal = 0;
		}
	}

	if(count != 0)
	{
		tempVec.push_back(tempVal);
	}
	dataVector.push_back(tempVec);

	return true;
}


} /* namespace nfc */
