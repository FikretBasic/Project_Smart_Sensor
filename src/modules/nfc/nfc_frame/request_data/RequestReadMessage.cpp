/*
 * RequestReadMessage.cpp
 *
 *  Created on: 27 Jun 2018
 *      Author: its2016
 */

#include "RequestReadMessage.h"

namespace nfc {

RequestReadMessage::RequestReadMessage() {}

RequestReadMessage::~RequestReadMessage() {}

std::string RequestReadMessage::returnCommandName()
{
	return "Read Message";
}

bool RequestReadMessage::setData(std::vector<bool> data, bool optional)
{
	size_t j = 0;
	std::vector<byte> tempVec;
	byte tempVal = 0;

	byte startPointer = 0x00;
	byte numOfBytes = 0x00;

	if(data.size() != 88 && data.size() != 24)
		return false;

	// IC Mfg Code
	for(int i = 7; i >= 0; i--)
	{
		tempVal |= (data.at(j) << i);
		j++;
	}
	tempVec.push_back(tempVal);
	dataVector.push_back(tempVec);

	tempVec.clear();
	tempVal = 0;

	// optional UID
	if(optional)
	{
		try
		{
			for(int k = 0; k < 8; k++)
			{
				for(int i = 7; i >= 0; i--)
				{
					tempVal |= (data.at(j) << i);
					j++;
				}
				tempVec.push_back(tempVal);
				tempVal = 0;
			}
			dataVector.push_back(tempVec);
		}
		catch(int e)
		{
			return false;
		}

		tempVec.clear();
		tempVal = 0;
	}

	// Starting Pointer
	for(int i = 7; i >= 0; i--)
	{
		tempVal |= (data.at(j) << i);
		j++;
	}
	tempVec.push_back(tempVal);
	dataVector.push_back(tempVec);

	startPointer = tempVal;

	tempVec.clear();
	tempVal = 0;

	// Number of bytes
	for(int i = 7; i >= 0; i--)
	{
		tempVal |= (data.at(j) << i);
		j++;
	}
	tempVec.push_back(tempVal);
	dataVector.push_back(tempVec);

	numOfBytes = tempVal;

	if((startPointer + numOfBytes + 1) > 256)
		return false;
	else
		return true;

}

} /* namespace nfc */
