/*
 * RequestReadMultipleBlock.cpp
 *
 *  Created on: 23 Jun 2018
 *      Author: its2016
 */

#include "RequestReadMultipleBlock.h"

namespace nfc {

RequestReadMultipleBlock::RequestReadMultipleBlock() {}

RequestReadMultipleBlock::~RequestReadMultipleBlock() {}

std::string RequestReadMultipleBlock::returnCommandName()
{
	return "Read Multiple Blocks";
}

bool RequestReadMultipleBlock::setData(std::vector<bool> data, bool optional)
{
	size_t j = 0;
	std::vector<byte> tempVec;
	byte tempVal = 0;

	if(data.size() != 80 && data.size() != 16)
		return false;

	// optional UID
	if(optional)
	{
		try
		{
			for(int k = 0; k < 8; k++)
			{
				for(int i = 7; i >= 0; i--)
				{
					tempVal |= (data.at(j) << i);
					j++;
				}
				tempVec.push_back(tempVal);
				tempVal = 0;
			}
			dataVector.push_back(tempVec);
		}
		catch(int e)
		{
			return false;
		}
	}

	tempVec.clear();
	tempVal = 0;

	// Starting Block number
	for(int i = 7; i >= 0; i--)
	{
		tempVal |= (data.at(j) << i);
		j++;
	}
	tempVec.push_back(tempVal);
	dataVector.push_back(tempVec);

	tempVec.clear();
	tempVal = 0;

	// Number of blocks
	for(int i = 7; i >= 0; i--)
	{
		tempVal |= (data.at(j) << i);
		j++;
	}
	tempVec.push_back(tempVal);
	dataVector.push_back(tempVec);

	return true;
}


} /* namespace nfc */
