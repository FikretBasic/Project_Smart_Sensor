/*
 * RequestReadSingleBlock.h
 *
 *  Created on: 22 Jun 2018
 *      Author: its2016
 */

#ifndef MODULES_NFC_NFC_FRAME_REQUEST_DATA_REQUESTREADSINGLEBLOCK_H_
#define MODULES_NFC_NFC_FRAME_REQUEST_DATA_REQUESTREADSINGLEBLOCK_H_

#include "RequestData.h"

namespace nfc {

class RequestReadSingleBlock : public Request_Data
{
public:
	RequestReadSingleBlock();
	virtual ~RequestReadSingleBlock();

	std::string returnCommandName();
	bool setData(std::vector<bool> data, bool optional);
};

}

#endif /* MODULES_NFC_NFC_FRAME_REQUEST_DATA_REQUESTREADSINGLEBLOCK_H_ */
