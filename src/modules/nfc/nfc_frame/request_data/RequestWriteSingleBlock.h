/*
 * RequestWriteSingleBlock.h
 *
 *  Created on: 22 Jun 2018
 *      Author: its2016
 */

#ifndef MODULES_NFC_NFC_FRAME_REQUEST_DATA_REQUESTWRITESINGLEBLOCK_H_
#define MODULES_NFC_NFC_FRAME_REQUEST_DATA_REQUESTWRITESINGLEBLOCK_H_

#include "RequestData.h"

namespace nfc {

class RequestWriteSingleBlock : public Request_Data {
public:
	RequestWriteSingleBlock();
	virtual ~RequestWriteSingleBlock();

	std::string returnCommandName();
	bool setData(std::vector<bool> data, bool optional);
};

} /* namespace nfc */

#endif /* MODULES_NFC_NFC_FRAME_REQUEST_DATA_REQUESTWRITESINGLEBLOCK_H_ */
