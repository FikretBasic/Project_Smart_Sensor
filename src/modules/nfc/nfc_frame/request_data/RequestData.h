/*
 * RequestData.h
 *
 *  Created on: 4 Jun 2018
 *      Author: its2016
 */

#ifndef MODULES_NFC_NFC_FRAME_REQUEST_DATA_REQUESTDATA_H_
#define MODULES_NFC_NFC_FRAME_REQUEST_DATA_REQUESTDATA_H_

#include "../../../Includes.h"

namespace nfc {

class Request_Data {
public:
	Request_Data();
	virtual ~Request_Data();

	virtual std::string returnCommandName() = 0;
	virtual bool setData(std::vector<bool> data, bool optional = 0) = 0;

	std::vector<std::vector<byte > > returnVector();

protected:
	std::vector<std::vector<byte > > dataVector;
};

} /* namespace nfc */

#endif /* MODULES_NFC_NFC_FRAME_REQUEST_DATA_REQUESTDATA_H_ */
