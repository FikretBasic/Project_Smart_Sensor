/*
 * RequestReadMultipleBlock.h
 *
 *  Created on: 23 Jun 2018
 *      Author: its2016
 */

#ifndef MODULES_NFC_NFC_FRAME_REQUEST_DATA_REQUESTREADMULTIPLEBLOCK_H_
#define MODULES_NFC_NFC_FRAME_REQUEST_DATA_REQUESTREADMULTIPLEBLOCK_H_

#include "RequestData.h"

namespace nfc {

class RequestReadMultipleBlock : public Request_Data
{
public:
	RequestReadMultipleBlock();
	virtual ~RequestReadMultipleBlock();

	std::string returnCommandName();
	bool setData(std::vector<bool> data, bool optional);
};

} /* namespace nfc */

#endif /* MODULES_NFC_NFC_FRAME_REQUEST_DATA_REQUESTREADMULTIPLEBLOCK_H_ */
