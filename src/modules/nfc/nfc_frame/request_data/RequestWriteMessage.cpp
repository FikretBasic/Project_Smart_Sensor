/*
 * RequestWriteMessage.cpp
 *
 *  Created on: 27 Jun 2018
 *      Author: its2016
 */

#include "RequestWriteMessage.h"

namespace nfc {

RequestWriteMessage::RequestWriteMessage() {}

RequestWriteMessage::~RequestWriteMessage() {}

std::string RequestWriteMessage::returnCommandName()
{
	return "Write Message";
}

bool RequestWriteMessage::setData(std::vector<bool> data, bool optional)
{
	size_t j = 0;
	std::vector<byte> tempVec;
	byte tempVal = 0;

	// IC Mfg code
	for(int i = 7; i >= 0; i--)
	{
		tempVal |= (data.at(j) << i);
		j++;
	}
	tempVec.push_back(tempVal);
	dataVector.push_back(tempVec);

	tempVec.clear();
	tempVal = 0;

	// optional UID
	if(optional)
	{
		try
		{
			for(int k = 0; k < 8; k++)
			{
				for(int i = 7; i >= 0; i--)
				{
					tempVal |= (data.at(j) << i);
					j++;
				}
				tempVec.push_back(tempVal);
				tempVal = 0;
			}
			dataVector.push_back(tempVec);
		}
		catch(int e)
		{
			return false;
		}
	}

	tempVec.clear();
	tempVal = 0;

	// Number of bytes
	for(int i = 7; i >= 0; i--)
	{
		tempVal |= (data.at(j) << i);
		j++;
	}
	tempVec.push_back(tempVal);
	dataVector.push_back(tempVec);

	tempVec.clear();
	int numOfBytes = tempVal + 1;

	// N-byte values
	for(int k = 0; k < numOfBytes; k++)
	{
		tempVal = 0;
		for(int i = 7; i >= 0; i--)
		{
			tempVal |= (data.at(j) << i);
			j++;
		}
		tempVec.push_back(tempVal);
	}
	dataVector.push_back(tempVec);

	return true;
}


} /* namespace nfc */
