/*
 * RequestWriteDynamic.cpp
 *
 *  Created on: 25 Jun 2018
 *      Author: its2016
 */

#include "RequestWriteDynamic.h"

namespace nfc
{

RequestWriteDynamic::RequestWriteDynamic() {}

RequestWriteDynamic::~RequestWriteDynamic() {}

std::string RequestWriteDynamic::returnCommandName()
{
	return "Write Dynamic Configuration";
}

bool RequestWriteDynamic::setData(std::vector<bool> data, bool optional)
{
	size_t j = 0;
	std::vector<byte> tempVec;
	byte tempVal = 0;

	// IC Mfg Code
	for(int i = 7; i >= 0; i--)
	{
		tempVal |= (data.at(j) << i);
		j++;
	}
	tempVec.push_back(tempVal);
	dataVector.push_back(tempVec);

	tempVec.clear();
	tempVal = 0;

	// optional UID
	if(optional)
	{
		try
		{
			for(int k = 0; k < 8; k++)
			{
				for(int i = 7; i >= 0; i--)
				{
					tempVal |= (data.at(j) << i);
					j++;
				}
				tempVec.push_back(tempVal);
				tempVal = 0;
			}
			dataVector.push_back(tempVec);
		}
		catch(int e)
		{
			return false;
		}

		tempVec.clear();
		tempVal = 0;
	}

	// Pointer
	for(int i = 7; i >= 0; i--)
	{
		tempVal |= (data.at(j) << i);
		j++;
	}
	tempVec.push_back(tempVal);
	dataVector.push_back(tempVec);

	tempVec.clear();
	tempVal = 0;

	// 8-bit value
	for(int i = 7; i >= 0; i--)
	{
		tempVal |= (data.at(j) << i);
		j++;
	}
	tempVec.push_back(tempVal);
	dataVector.push_back(tempVec);

	return true;
}

}
