/*
 * RequestData.cpp
 *
 *  Created on: 4 Jun 2018
 *      Author: its2016
 */

#include "RequestData.h"

namespace nfc {

Request_Data::Request_Data() {}

Request_Data::~Request_Data() {}

std::vector<std::vector<byte > > Request_Data::returnVector()
{
	return dataVector;
}

} /* namespace nfc */
