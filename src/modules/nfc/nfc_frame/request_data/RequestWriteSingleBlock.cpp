/*
 * RequestWriteSingleBlock.cpp
 *
 *  Created on: 22 Jun 2018
 *      Author: its2016
 */

#include "RequestWriteSingleBlock.h"

namespace nfc {

RequestWriteSingleBlock::RequestWriteSingleBlock() {}

RequestWriteSingleBlock::~RequestWriteSingleBlock() {}

std::string RequestWriteSingleBlock::returnCommandName()
{
	return "Write Single Block";
}

bool RequestWriteSingleBlock::setData(std::vector<bool> data, bool optional)
{
	size_t j = 0;
	std::vector<byte> tempVec;
	byte tempVal = 0;

	// optional UID
	if(optional)
	{
		try
		{
			for(int k = 0; k < 8; k++)
			{
				for(int i = 7; i >= 0; i--)
				{
					tempVal |= (data.at(j) << i);
					j++;
				}
				tempVec.push_back(tempVal);
				tempVal = 0;
			}
			dataVector.push_back(tempVec);
		}
		catch(int e)
		{
			return false;
		}
	}

	tempVec.clear();
	tempVal = 0;

	// Block number
	for(int i = 7; i >= 0; i--)
	{
		tempVal |= (data.at(j) << i);
		j++;
	}
	tempVec.push_back(tempVal);
	dataVector.push_back(tempVec);

	tempVec.clear();

	// 32-bit value
	for(int k = 0; k < 4; k++)
	{
		tempVal = 0;
		for(int i = 7; i >= 0; i--)
		{
			tempVal |= (data.at(j) << i);
			j++;
		}
		tempVec.push_back(tempVal);
	}
	dataVector.push_back(tempVec);

	return true;
}

} /* namespace nfc */
