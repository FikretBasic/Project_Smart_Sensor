/*
 * RequestMessageLength.h
 *
 *  Created on: 27 Jun 2018
 *      Author: its2016
 */

#ifndef MODULES_NFC_NFC_FRAME_REQUEST_DATA_REQUESTMESSAGELENGTH_H_
#define MODULES_NFC_NFC_FRAME_REQUEST_DATA_REQUESTMESSAGELENGTH_H_

#include "RequestData.h"

namespace nfc {

class RequestMessageLength : public Request_Data
{
public:
	RequestMessageLength();
	virtual ~RequestMessageLength();

	std::string returnCommandName();
	bool setData(std::vector<bool> data, bool optional);
};

} /* namespace nfc */

#endif /* MODULES_NFC_NFC_FRAME_REQUEST_DATA_REQUESTMESSAGELENGTH_H_ */
