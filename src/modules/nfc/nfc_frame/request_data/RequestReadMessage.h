/*
 * RequestReadMessage.h
 *
 *  Created on: 27 Jun 2018
 *      Author: its2016
 */

#ifndef MODULES_NFC_NFC_FRAME_REQUEST_DATA_REQUESTREADMESSAGE_H_
#define MODULES_NFC_NFC_FRAME_REQUEST_DATA_REQUESTREADMESSAGE_H_

#include "RequestData.h"

namespace nfc {

class RequestReadMessage : public Request_Data
{
public:
	RequestReadMessage();
	virtual ~RequestReadMessage();

	std::string returnCommandName();
	bool setData(std::vector<bool> data, bool optional);
};

} /* namespace nfc */

#endif /* MODULES_NFC_NFC_FRAME_REQUEST_DATA_REQUESTREADMESSAGE_H_ */
