/*
 * RequestInventory.h
 *
 *  Created on: 4 Jun 2018
 *      Author: its2016
 */

#ifndef MODULES_NFC_NFC_FRAME_REQUEST_DATA_REQUESTINVENTORY_H_
#define MODULES_NFC_NFC_FRAME_REQUEST_DATA_REQUESTINVENTORY_H_

#include "RequestData.h"

namespace nfc {

class Request_Inventory : public Request_Data
{
public:
	Request_Inventory();
	virtual ~Request_Inventory();

	std::string returnCommandName();
	bool setData(std::vector<bool> data, bool optional);
};

} /* namespace nfc */

#endif /* MODULES_NFC_NFC_FRAME_REQUEST_DATA_REQUESTINVENTORY_H_ */
