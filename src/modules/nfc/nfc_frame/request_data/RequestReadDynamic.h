/*
 * RequestReadDynamic.h
 *
 *  Created on: 25 Jun 2018
 *      Author: its2016
 */

#ifndef MODULES_NFC_NFC_FRAME_REQUEST_DATA_REQUESTREADDYNAMIC_H_
#define MODULES_NFC_NFC_FRAME_REQUEST_DATA_REQUESTREADDYNAMIC_H_

#include "RequestData.h"

namespace nfc
{

class RequestReadDynamic : public Request_Data
{
public:
	RequestReadDynamic();
	virtual ~RequestReadDynamic();

	std::string returnCommandName();
	bool setData(std::vector<bool> data, bool optional);
};

}

#endif /* MODULES_NFC_NFC_FRAME_REQUEST_DATA_REQUESTREADDYNAMIC_H_ */
