/*
 * NFCMemory.h
 *
 *  Created on: 23 May 2018
 *      Author: its2016
 */

#ifndef MODULES_NFC_NFCMEMORY_H_
#define MODULES_NFC_NFCMEMORY_H_

#include "../Includes.h"

namespace nfc {

class NFCMemory
{
public:
	NFCMemory();
	virtual ~NFCMemory();

	bool writeInMemory(byte data);
	bool writeInSystem(byte data);
	bool writeInDynamic(byte data);
	bool writeInMailbox(byte data);

	void resetMailboxAddr();
	void resetMailboxVal();
	bool insertPeriodicallyMemory();
	void insertUserMemory();

	byte readFromMemory();
	byte readFromMailBox();
	byte readFromSystem();
	byte readFromDynamic();

	void incrementAddr();
	void incrementMailboxAddr();
	void setCurrentUserAddress(uint16_t currentUserAddress);
	void setCurrentMailBoxAddress(uint16_t currMailBoxAddr);
	void setCurrentSystemAddress(uint8_t currentSysAddr);
	void setCurrentDynamicAddress(uint8_t currentDynAddr);
	void writeOutUserMemory();

	void setFTMstatus(bool status);
	bool getFTMstatus();
	byte getHarvestRegisterStatus();

	byte mailbox[256];		// for the fast-transfer mode
	uint16_t currMailBoxAddr;
	uint16_t counterBoxAddr;


private:
	byte userMemory[8192];
	uint16_t currUserAddr;

	bool fast_transfer_mode;

	// Taken only space for the most important commands
	byte systemMemory[35];
	uint8_t currSystemAddr;

	byte dynamicMemory[8];
	uint8_t currDynAddr;
};

} /* namespace nfc */

#endif /* MODULES_NFC_NFCMEMORY_H_ */
