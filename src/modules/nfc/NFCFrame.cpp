/*
 * NFCFrame.cpp
 *
 *  Created on: 25 Jun 2018
 *      Author: its2016
 */

#include "NFCFrame.h"

NFCFrame* NFCFrame::instance_t = 0;

NFCFrame::NFCFrame() {}

NFCFrame::~NFCFrame() { delete instance_t; }

NFCFrame* NFCFrame::instance()
{
    if (!instance_t)
    	instance_t = new NFCFrame();
    return instance_t;
}

void NFCFrame::emptyTheQueue(std::queue<byte> &rfValues)
{
	while(!rfValues.empty())
	{
		rfValues.pop();
	}
}

void NFCFrame::inventoryCommand(std::queue<byte> &rfValues, byte flags)
{
	emptyTheQueue(rfValues);
	std::vector<byte> crcInput;

	// request flags
	rfValues.push(flags);
	crcInput.push_back(flags);

	// command
	rfValues.push(0x01);
	crcInput.push_back(0x01);

	// mask size (for inventory command, no AFI)
	rfValues.push(0x08);
	crcInput.push_back(0x08);

	// mask value (for inventory command)
	rfValues.push(0x83);
	crcInput.push_back(0x83);

	uint16_t crcResult = crcObject.calculateCRC(crcInput);
	byte msbCrcResult = (uint8_t)(crcResult >> 8);
	byte lsbCrcResult = (uint8_t)crcResult;

	rfValues.push(msbCrcResult);
	rfValues.push(lsbCrcResult);
}

void NFCFrame::readBlockCommand(std::queue<byte> &rfValues, byte flags, byte addr)
{
	emptyTheQueue(rfValues);
	std::vector<byte> crcInput;

	// request flags
	rfValues.push(flags);
	crcInput.push_back(flags);

	// command
	rfValues.push(0x20);
	crcInput.push_back(0x20);

	// address value (for read one block command)
	rfValues.push(addr);
	crcInput.push_back(addr);

	uint16_t crcResult = crcObject.calculateCRC(crcInput);
	byte msbCrcResult = (uint8_t)(crcResult >> 8);
	byte lsbCrcResult = (uint8_t)crcResult;

	rfValues.push(msbCrcResult);
	rfValues.push(lsbCrcResult);
}

void NFCFrame::writeBlockCommand(std::queue<byte> &rfValues, byte flags, byte addr, std::vector<byte> writeVal)
{
	emptyTheQueue(rfValues);
	std::vector<byte> crcInput;

	// request flags
	rfValues.push(flags);
	crcInput.push_back(flags);

	// command
	rfValues.push(0x21);
	crcInput.push_back(0x21);

	// address value (for read one block command)
	rfValues.push(addr);
	crcInput.push_back(addr);

	for(size_t i = 0; i < 4; i++)
	{
		if(i >= writeVal.size())
		{
			rfValues.push(0x00);
			crcInput.push_back(0x00);
		}
		else
		{
			rfValues.push(writeVal.at(i));
			crcInput.push_back(writeVal.at(i));
		}
	}

	uint16_t crcResult = crcObject.calculateCRC(crcInput);
	byte msbCrcResult = (uint8_t)(crcResult >> 8);
	byte lsbCrcResult = (uint8_t)crcResult;

	rfValues.push(msbCrcResult);
	rfValues.push(lsbCrcResult);
}

void NFCFrame::readMultipleCommand(std::queue<byte> &rfValues, byte flags, byte addr, byte numOfBlocks)
{
	emptyTheQueue(rfValues);
	std::vector<byte> crcInput;

	// request flags
	rfValues.push(flags);
	crcInput.push_back(flags);

	// command
	rfValues.push(0x23);
	crcInput.push_back(0x23);

	// address value (for read one block command)
	rfValues.push(addr);
	crcInput.push_back(addr);

	// number of blocks
	rfValues.push(numOfBlocks);
	crcInput.push_back(numOfBlocks);

	uint16_t crcResult = crcObject.calculateCRC(crcInput);
	byte msbCrcResult = (uint8_t)(crcResult >> 8);
	byte lsbCrcResult = (uint8_t)crcResult;

	rfValues.push(msbCrcResult);
	rfValues.push(lsbCrcResult);

}

void NFCFrame::writeMultipleCommand(std::queue<byte> &rfValues, byte flags, byte addr
		, byte numOfBlocks, std::vector<byte> writeVal)
{
	emptyTheQueue(rfValues);
	std::vector<byte> crcInput;

	// request flags
	rfValues.push(flags);
	crcInput.push_back(flags);

	// command
	rfValues.push(0x24);
	crcInput.push_back(0x24);

	// address value (for read one block command)
	rfValues.push(addr);
	crcInput.push_back(addr);

	// number of blocks to write + 1
	rfValues.push(numOfBlocks);
	crcInput.push_back(numOfBlocks);

	size_t numOfBytes = (numOfBlocks + 1) * 4;

	for (size_t i = 0; i < numOfBytes; i++)
	{
		if(i == writeVal.size())
		{
			rfValues.push(0x00);
			crcInput.push_back(0x00);
		}
		else
		{
			rfValues.push(writeVal.at(i));
			crcInput.push_back(writeVal.at(i));
		}
	}

	uint16_t crcResult = crcObject.calculateCRC(crcInput);
	byte msbCrcResult = (uint8_t)(crcResult >> 8);
	byte lsbCrcResult = (uint8_t)crcResult;

	rfValues.push(msbCrcResult);
	rfValues.push(lsbCrcResult);
}

void NFCFrame::readConfigurationCommand(std::queue<byte> &rfValues, byte flags, byte addr)
{
	emptyTheQueue(rfValues);
	std::vector<byte> crcInput;

	// request flags
	rfValues.push(flags);
	crcInput.push_back(flags);

	// command
	rfValues.push(0xa0);
	crcInput.push_back(0xa0);

	// IC Mfg code
	rfValues.push(0x02);
	crcInput.push_back(0x02);

	// Pointer
	rfValues.push(addr);
	crcInput.push_back(addr);

	// CRC
	uint16_t crcResult = crcObject.calculateCRC(crcInput);
	byte msbCrcResult = (uint8_t)(crcResult >> 8);
	byte lsbCrcResult = (uint8_t)crcResult;

	rfValues.push(msbCrcResult);
	rfValues.push(lsbCrcResult);
}

void NFCFrame::writeConfigurationCommand(std::queue<byte> &rfValues, byte flags, byte addr, byte writeVal)
{
	emptyTheQueue(rfValues);
	std::vector<byte> crcInput;

	// request flags
	rfValues.push(flags);
	crcInput.push_back(flags);

	// command
	rfValues.push(0xa1);
	crcInput.push_back(0xa1);

	// IC Mfg code
	rfValues.push(0x02);
	crcInput.push_back(0x02);

	// Pointer
	rfValues.push(addr);
	crcInput.push_back(addr);

	// Register value
	rfValues.push(writeVal);
	crcInput.push_back(writeVal);

	// CRC
	uint16_t crcResult = crcObject.calculateCRC(crcInput);
	byte msbCrcResult = (uint8_t)(crcResult >> 8);
	byte lsbCrcResult = (uint8_t)crcResult;

	rfValues.push(msbCrcResult);
	rfValues.push(lsbCrcResult);
}

void NFCFrame::readDynamicCommand(std::queue<byte> &rfValues, byte flags, byte addr)
{
	emptyTheQueue(rfValues);
	std::vector<byte> crcInput;

	// request flags
	rfValues.push(flags);
	crcInput.push_back(flags);

	// command
	rfValues.push(0xad);
	crcInput.push_back(0xad);

	// IC Mfg code
	rfValues.push(0x02);
	crcInput.push_back(0x02);

	// Pointer
	rfValues.push(addr);
	crcInput.push_back(addr);

	// CRC
	uint16_t crcResult = crcObject.calculateCRC(crcInput);
	byte msbCrcResult = (uint8_t)(crcResult >> 8);
	byte lsbCrcResult = (uint8_t)crcResult;

	rfValues.push(msbCrcResult);
	rfValues.push(lsbCrcResult);
}

void NFCFrame::writeDynamicCommand(std::queue<byte> &rfValues, byte flags, byte addr, byte writeVal)
{
	emptyTheQueue(rfValues);
	std::vector<byte> crcInput;

	// request flags
	rfValues.push(flags);
	crcInput.push_back(flags);

	// command
	rfValues.push(0xae);
	crcInput.push_back(0xae);

	// IC Mfg code
	rfValues.push(0x02);
	crcInput.push_back(0x02);

	// Pointer
	rfValues.push(addr);
	crcInput.push_back(addr);

	// Register value
	rfValues.push(writeVal);
	crcInput.push_back(writeVal);

	// CRC
	uint16_t crcResult = crcObject.calculateCRC(crcInput);
	byte msbCrcResult = (uint8_t)(crcResult >> 8);
	byte lsbCrcResult = (uint8_t)crcResult;

	rfValues.push(msbCrcResult);
	rfValues.push(lsbCrcResult);
}

void NFCFrame::writeMessageCommand(std::queue<byte> &rfValues, byte flags
		, byte numOfBytes, std::vector<byte> writeVal)
{
	emptyTheQueue(rfValues);
	std::vector<byte> crcInput;

	// request flags
	rfValues.push(flags);
	crcInput.push_back(flags);

	// command
	rfValues.push(0xaa);
	crcInput.push_back(0xaa);

	// IC Mfg code
	rfValues.push(0x02);
	crcInput.push_back(0x02);

	// Number of bytes
	rfValues.push(numOfBytes);
	crcInput.push_back(numOfBytes);

	// Write values
	for (byte i = 0; i < numOfBytes + 1; i++)
	{
		if(i == writeVal.size())
		{
			rfValues.push(0x00);
			crcInput.push_back(0x00);
		}
		else
		{
			rfValues.push(writeVal.at(i));
			crcInput.push_back(writeVal.at(i));
		}
	}

	// CRC
	uint16_t crcResult = crcObject.calculateCRC(crcInput);
	byte msbCrcResult = (uint8_t)(crcResult >> 8);
	byte lsbCrcResult = (uint8_t)crcResult;

	cout << "CRC Origin: " << crcResult << endl;

	rfValues.push(msbCrcResult);
	rfValues.push(lsbCrcResult);
}

void NFCFrame::readMessageLengthCommand(std::queue<byte> &rfValues, byte flags)
{
	emptyTheQueue(rfValues);
	std::vector<byte> crcInput;

	// request flags
	rfValues.push(flags);
	crcInput.push_back(flags);

	// command
	rfValues.push(0xab);
	crcInput.push_back(0xab);

	// IC Mfg code
	rfValues.push(0x02);
	crcInput.push_back(0x02);

	// CRC
	uint16_t crcResult = crcObject.calculateCRC(crcInput);
	byte msbCrcResult = (uint8_t)(crcResult >> 8);
	byte lsbCrcResult = (uint8_t)crcResult;

	rfValues.push(msbCrcResult);
	rfValues.push(lsbCrcResult);
}

void NFCFrame::readMessageCommand(std::queue<byte> &rfValues, byte flags, byte pointer, byte numOfBytes)
{
	emptyTheQueue(rfValues);
	std::vector<byte> crcInput;

	// request flags
	rfValues.push(flags);
	crcInput.push_back(flags);

	// command
	rfValues.push(0xac);
	crcInput.push_back(0xac);

	// IC Mfg code
	rfValues.push(0x02);
	crcInput.push_back(0x02);

	// Pointer
	rfValues.push(pointer);
	crcInput.push_back(pointer);

	// Num. of bytes
	rfValues.push(numOfBytes);
	crcInput.push_back(numOfBytes);

	// CRC
	uint16_t crcResult = crcObject.calculateCRC(crcInput);
	byte msbCrcResult = (uint8_t)(crcResult >> 8);
	byte lsbCrcResult = (uint8_t)crcResult;

	rfValues.push(msbCrcResult);
	rfValues.push(lsbCrcResult);
}
