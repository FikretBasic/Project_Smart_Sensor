/*
 * NFC.cpp
 *
 *  Created on: 8 May 2018
 *      Author: its2016
 */

#include "NFC.h"

namespace pss {

void nfc_tag::i2c()
{
	int iter = 7;
	bool ack = true;
	byte cur_val = 0;
	bool sleep_op = false;

	sda_out.write(1);

	while(true)
	{
		wait();

		switch(opState)
		{
			case start:
				if (enable.read())
				{
					opState = dev_addr;
					enState_I2C = active_e2_write_i2c;
					mem_buffer = 0;
				}
				break;

			case dev_addr:
				cur_val = sda_in.read();
				mem_buffer |= (cur_val << iter);

				if(iter == 0)
				{
					cur_val = checkDeviceAddressValue();

					if (cur_val == 1)
					{
						read_or_write = mem_buffer & 1;
						sda_out.write(0);

						if (sleep_op)
						{
							opState = stop;
							iter = 7;
						}
						else
						{
							if(read_or_write)
							{
								opState = rd_wr;
								enState_I2C = active_e2_read_i2c;
								iter = 7;
							}
							else
							{
								opState = mem_addr;
								enState_I2C = active_e2_write_i2c;
								iter = 15;
							}
						}
						sleep_op = false;
					}
					else if (cur_val == 2)
					{
						sda_out.write(0);

						sleep_op = true;
						read_or_write = false;
						opState = rd_wr;
						mem_buffer = 0;
						ack = true;
						iter = 7;
					}
					else
					{
						opState = start;
						enState_I2C = idle_i2c;
					}
				}
				else
				{
					iter--;
				}

				break;

			case mem_addr:
				if (sda_in.read() == 1 && ack)
				{
					ack = false;
					sda_out.write(1);
					addressBuffer = 0;
				}
				else
				{
					int cur_val = sda_in.read();
					addressBuffer |= (cur_val << iter);

					if (iter == 0)
					{
						//checkAddressSize();

						if(E2 == 0)
						{
							if(addressBuffer >= 0x2000 && addressBuffer < 0x2008)
							{
								uint8_t dynMem = addressBuffer & 0x000f;
								memory->setCurrentDynamicAddress(dynMem);
							}
							else if(addressBuffer >= 0x2008)
							{
								memory->resetMailboxAddr();
								if(!read_or_write)
								{
									enState_I2C = active_mb_write_i2c;
									//memory->resetMailboxVal();
								}
								else
								{
									enState_I2C = active_mb_read_i2c;
								}
							}
							else
							{
								memory->setCurrentUserAddress(addressBuffer);
							}
						}
						else
						{
							memory->setCurrentSystemAddress((uint8_t)addressBuffer);
						}

						opState = rd_wr;
						mem_buffer = 0;
						sda_out.write(0);
						ack = true;
						iter = 7;
					}
					else
					{
						iter--;
					}
				}

				break;

			case rd_wr:
				if(enable.read())
				{
					if (sda_in.read() == 1 && ack && !read_or_write)
					{
						ack = false;
						sda_out.write(1);
					}
					else
					{
						if(read_or_write)
						{
							//cout << "Reading from memory: " << (int)memory->readFromMemory() << endl;
							//sda_out.write((memory->readFromMemory() >> iter) & 1);

							if(E2 == 0)
							{
								if(addressBuffer >= 0x2000 && addressBuffer < 0x2008)
								{
									sda_out.write((memory->readFromDynamic() >> iter) & 1);
									if(addressBuffer == 0x2007)
									{
										//cout << "Read from dynamic: " << (int)memory->readFromDynamic();
										//cout << "MIC SEN: " << ((memory->readFromDynamic() >> iter) & 1) << endl;
									}
								}
								else if(addressBuffer >= 0x2008)
								{
									sda_out.write((memory->readFromMailBox() >> iter) & 1);
								}
								else
								{
									sda_out.write((memory->readFromMemory() >> iter) & 1);
								}
							}
							else
							{
								sda_out.write((memory->readFromSystem() >> iter) & 1);
								//cout << "Sda out written: " << (int)((memory->readFromSystem() >> iter) & 1) << endl;
							}

						}
						else
						{
							byte cur_val = sda_in.read();
							mem_buffer |= (cur_val << iter);
						}

						if(iter == 0)
						{
							if (sleep_op)
							{
								if (mem_buffer == 0x86)
								{
									sda_out.write(1);
								}
								else
								{
									sda_out.write(0);
									sleep_op = false;
								}
								opState = stop;
							}
							else
							{
								if (!read_or_write)
								{
									if(E2 == 0)
									{
										if(addressBuffer >= 0x2000 && addressBuffer < 0x2008)
										{
											memory->writeInDynamic(mem_buffer);

											if(addressBuffer == 0x2006)
												memory->setFTMstatus(mem_buffer & 1);
										}
										else if(addressBuffer >= 0x2008)
										{
											memory->setCurrentDynamicAddress(0x06);

											// only if the current value is set can it write
											if(memory->readFromDynamic() != 0x05)
												memory->writeInMailbox(mem_buffer);
										}
										else
										{
											memory->writeInMemory(mem_buffer);
										}
									}
									else
										memory->writeInSystem(mem_buffer);

									cout << "\nMem addr value: " << (int)addressBuffer << endl;
									cout << "Mem value: " << (int)mem_buffer << endl;

									mem_buffer = 0;
								}
								else
								{
									if(addressBuffer < 0x2000 && E2 == 0)
										memory->incrementAddr();
									else if (addressBuffer >= 0x2008)
									{
										memory->incrementMailboxAddr();
									}

									// for synchronisation
									wait();
								}

								addressBuffer++;
							}

							iter = 7;
							ack = true;
						}
						else
						{
							iter--;
						}
					}
				}
				else
				{
					opState = stop;
				}

				break;

			case stop:
				opState = start;

				enState_I2C = idle_i2c;

				if (!read_or_write)
				{
					if(E2 == 0 && addressBuffer <= 0x2000)
						handleMemoryInput();
					else if (addressBuffer >= 0x2008)
					{
						if(memory->currMailBoxAddr > 0)
						{
							// set dynamic address of the current mailbox input
							memory->setCurrentDynamicAddress(0x07);
							memory->writeInDynamic(memory->currMailBoxAddr);
						}

						// set the option that RF has already written
						memory->setCurrentDynamicAddress(0x06);
						memory->writeInDynamic(0x03);
					}
				}
				else
				{
					if(addressBuffer >= 0x2008)
					{
						// set the option that I2C has emptied the mailbox
						memory->setCurrentDynamicAddress(0x06);
						memory->writeInDynamic(0x01);
					}
				}

				mem_buffer = 0;
				read_or_write = 0;

				iter = 7;
				ack = true;
				cur_val = 0;

				break;

			default:
				opState = start;
				enState_I2C = idle_i2c;
				read_or_write = 0;
		}
	}
}

byte nfc_tag::checkDeviceAddressValue()
{
	byte deviceNum = (mem_buffer & 240) >> 4;
	E2 = (mem_buffer & 8) >> 3;
	byte b2Check = (mem_buffer & 4) >> 2;
	byte b1Check = (mem_buffer & 2) >> 1;

	if (deviceNum == 10 && b2Check == 1 && b1Check == 1)
	{
		return 1;
	}
	else if (mem_buffer == 0xf8)
	{
		return 2;
	}
	else
	{
		return 0;
	}
}

void nfc_tag::handleMemoryInput()
{
	int pageCounter = 0;

	cout << "\nStart of writing in memory: " << sc_time_stamp() << endl;

	while(true)
	{
		if(!memory->insertPeriodicallyMemory())
		{
			break;
		}
		else
		{
			// Internal programming which occurs during each writes and lasts for about 5ms
			// for each block (4 bytes)
			if(++pageCounter == 4)
			{
				pageCounter = 0;
				//wait(5, SC_MS);
			}
		}
	}

	cout << "End of writing in memory: " << sc_time_stamp() << endl;
}


// ****** NFC FUNCTIONS ******

// Time delay information taken from the documentation, site: 106 / 107

void nfc_tag::rf_receive()
{
	//bool checkStatus = false;
	nfc::RF_Frame_Request *frame_req = NULL;
	std::vector<bool> input;

	nfc::RF_Frame_Response *frame_resp = NULL;

	short counter = 0;
	int err_flag;

	while(true)
	{
		wait();

		if(enable_rf_in.read())
		{
			if(frame_req == NULL)
			{
				frame_req = new nfc::RF_Frame_Request();
				input.clear();

				enState_RF = active_rf;
			}
			else
			{
				//cout << "Anntena receive: " << antenna_in.read() << endl << endl;
				input.push_back(antenna_in.read());

				if(++counter == 8)
				{
					counter = 0;
					wait();
				}
			}
		}
		else
		{
			if (frame_req != NULL)
			{
				std::vector<byte> proccData;

				if(frame_req->setFrame(input, err_flag))
				{
					cout << "\nFrame successfully set on NFC!" << " " << (int)err_flag << endl;
					proccData = processRfRequest(frame_req);
				}
				else
				{
					cout << "\nFrame received an error on NFC!" << endl;
				}

				frame_resp = new nfc::RF_Frame_Response();
				frame_resp->setRequestFrame(frame_req);
				frame_resp->defineResponse(input, err_flag, proccData);
				startResponse = true;
				vectorToSend = input;

				// DEL!
				/*cout << "Input which is sent: ";
				for (size_t i = 0; i < input.size(); i++)
					cout << (int)input.at(i) << " ";
				cout << "\n";*/

				frame_resp = NULL;
				frame_req = NULL;

				// so-called t1 delay, before a response can be send
				wait(320, SC_US);

				enState_RF = idle_rf;
			}
		}

	}
}

void nfc_tag::rf_send()
{
	size_t iter = 0;

	while(true)
	{
		wait();

		if(startResponse)
		{
			// inserting delay for SOF
			frameDelay();

			enable_rf_out.write(1);
			startResponse = false;
		}
		else
		{
			if(enable_rf_out.read())
			{
				//cout << "Antena send: " << vectorToSend.at(iter) << endl;
				antenna_out.write(vectorToSend.at(iter));
				iter++;

				if(iter == vectorToSend.size())
				{
					wait();
					iter = 0;
					vectorToSend.clear();
					enable_rf_out.write(0);

					// inserting delay for SOF
					frameDelay();
				}
			}
		}

	}
}


std::vector<byte> nfc_tag::processRfRequest(nfc::RF_Frame_Request *frame_req)
{
	std::vector<byte> retVector;
	byte memRFAddr = 0;
	byte memRFVal = 0;

	switch(frame_req->getCommand_code())
	{
		// Read particular 4 bytes (one block) from a part in the NFC memory
		case 0x20:
			memRFAddr = frame_req->getParamAndData()->returnVector()
				.at(frame_req->getParamAndData()->returnVector().size() - 1).at(0);
			cout << "\nMemory address read (Read): " << (int)memRFAddr << endl;
			memRFAddr *= 4;
			memory->setCurrentUserAddress(memRFAddr);

			for(int i = 0; i < 4; i++)
			{
				retVector.push_back(memory->readFromMemory());
				memory->incrementAddr();
			}

			break;

		// Write 4 bytes to a block in the NFC memory
		case 0x21:
			memRFAddr = frame_req->getParamAndData()->returnVector()
				.at(frame_req->getParamAndData()->returnVector().size() - 2).at(0);
			cout << "\nMemory address read (Write): " << (int)memRFAddr << endl;
			memRFAddr *= 4;
			memory->setCurrentUserAddress(memRFAddr);

			for(int i = 0; i < 4; i++)
			{
				memRFVal = frame_req->getParamAndData()->returnVector()
						.at(frame_req->getParamAndData()->returnVector().size() - 1).at(i);
				memory->writeInMemory(memRFVal);
			}

			handleMemoryInput();

			break;

		// Read Multiple Blocks
		case 0x23:
			memRFAddr = frame_req->getParamAndData()->returnVector()
				.at(frame_req->getParamAndData()->returnVector().size() - 2).at(0);
			cout << "\nMemory address read (Read): " << (int)memRFAddr << endl;
			memRFAddr *= 4;
			memory->setCurrentUserAddress(memRFAddr);

			// Number of read values
			memRFAddr = frame_req->getParamAndData()->returnVector()
					.at(frame_req->getParamAndData()->returnVector().size() - 1).at(0);

			for(int i = 0; i < memRFAddr + 1; i++)
			{
				for(int j = 0; j < 4; j++)
				{
					retVector.push_back(memory->readFromMemory());
					memory->incrementAddr();
				}
			}

			break;

		// Write Multiple Blocks
		case 0x24:
			memRFAddr = frame_req->getParamAndData()->returnVector()
				.at(frame_req->getParamAndData()->returnVector().size() - 3).at(0);
			cout << "\nMemory address read (Write): " << (int)memRFAddr << endl;
			memRFAddr *= 4;
			memory->setCurrentUserAddress(memRFAddr);

			// Number of values to write
			memRFAddr = frame_req->getParamAndData()->returnVector()
					.at(frame_req->getParamAndData()->returnVector().size() - 2).at(0);

			for(int i = 0; i < memRFAddr + 1; i++)
			{
				for(int j = 0; j < 4; j++)
				{
					memRFVal = frame_req->getParamAndData()->returnVector()
							.at(frame_req->getParamAndData()->returnVector().size() - 1).at(i*4 + j);
					memory->writeInMemory(memRFVal);
				}
			}

			handleMemoryInput();

			break;

		// Read configuration
		case 0xa0:

			memRFAddr = frame_req->getParamAndData()->returnVector()
				.at(frame_req->getParamAndData()->returnVector().size() - 1).at(0);
			cout << "\nMemory address read (Read): " << (int)memRFAddr << endl;

			memory->setCurrentSystemAddress(memRFAddr);

			retVector.push_back(memory->readFromSystem());

			break;

		// Write configuration
		case 0xa1:

			memRFAddr = frame_req->getParamAndData()->returnVector()
				.at(frame_req->getParamAndData()->returnVector().size() - 2).at(0);
			cout << "\nMemory address read (Write): " << (int)memRFAddr << endl;

			memory->setCurrentSystemAddress(memRFAddr);

			memRFVal = frame_req->getParamAndData()->returnVector()
					.at(frame_req->getParamAndData()->returnVector().size() - 1).at(0);
			memory->writeInSystem(memRFVal);

			// see if it is accessing the EH_MODE register
			if(memRFAddr == 0x02)
			{
				// set the Energy harvesting, or not
				if(memRFVal == 0x00)
				{
					// it is setting EH mode, also change the EH_EN settings
					memory->setCurrentDynamicAddress(0x02);
					memory->writeInDynamic(memory->readFromDynamic() | 0x01);
				}
				else
				{
					// it is setting EH mode, also change the EH_EN settings
					memory->setCurrentDynamicAddress(0x02);
					memory->writeInDynamic(memory->readFromDynamic() & 0xfe);
				}
			}

			// todo: Put handling if the pause is needed
			//handleMemoryInput();

			break;

		// Write Message
		case 0xaa:

			if(memory->getFTMstatus())
			{
				memory->setCurrentDynamicAddress(0x06);

				if(memory->readFromDynamic() != 0x03)
				{
					memRFAddr = frame_req->getParamAndData()->returnVector()
						.at(frame_req->getParamAndData()->returnVector().size() - 2).at(0);
					cout << "\nLength of Write message: " << (int)memRFAddr << endl;

					memory->resetMailboxAddr();
					memory->resetMailboxVal();

					// DEL!
					cout << "Received Mailbox values: " << endl;
					for(int i = 0; i < memRFAddr + 1; i++)
					{
						memRFVal = frame_req->getParamAndData()->returnVector()
								.at(frame_req->getParamAndData()->returnVector().size() - 1).at(i);
						memory->writeInMailbox(memRFVal);
						cout << (int)memRFVal << " ";
					}
					cout << endl;

					// set dynamic address of the current mailbox input
					memory->setCurrentDynamicAddress(0x07);
					memory->writeInDynamic(memRFAddr + 1);

					// set the option that RF has already written
					memory->setCurrentDynamicAddress(0x06);
					memory->writeInDynamic(0x05);
				}
			}

			// todo: add eventual delay for writing in the mailbox

			break;

		// Read Message length
		case 0xab:

			// Read MB_Len_Dyn
			memRFAddr = 0x07;
			cout << "\nMemory address read (Read): " << (int)memRFAddr << endl;

			memory->setCurrentDynamicAddress(memRFAddr);

			retVector.push_back(memory->readFromDynamic());

			break;

		// Read Message
		case 0xac:

			// MB Pointer
			memRFAddr = frame_req->getParamAndData()->returnVector()
				.at(frame_req->getParamAndData()->returnVector().size() - 2).at(0);

			// MB number of blocks
			memRFVal = frame_req->getParamAndData()->returnVector()
						.at(frame_req->getParamAndData()->returnVector().size() - 1).at(0);

			if(memRFAddr == 0 && memRFVal == 0)
			{
				memory->resetMailboxAddr();

				memory->setCurrentDynamicAddress(0x07);

				for(int i = 0; i < memory->readFromDynamic(); i++)
				{
					retVector.push_back(memory->readFromMailBox());
					memory->incrementMailboxAddr();
				}
			}
			else
			{
				memory->setCurrentMailBoxAddress(memRFAddr);

				for(int i = memRFAddr; i < memRFVal + 1; i++)
				{
					retVector.push_back(memory->readFromMailBox());
					memory->incrementMailboxAddr();
				}
			}

			// set the option that RF has emptied the mailbox
			memory->setCurrentDynamicAddress(0x06);
			memory->writeInDynamic(0x01);

			break;

		// Read Dynamic Configuration
		case 0xad:

			memRFAddr = frame_req->getParamAndData()->returnVector()
				.at(frame_req->getParamAndData()->returnVector().size() - 1).at(0);
			cout << "\nMemory address read (Read): " << (int)memRFAddr << endl;

			memory->setCurrentDynamicAddress(memRFAddr);

			retVector.push_back(memory->readFromDynamic());

			break;

		// Write Dynamic Configuration
		case 0xae:

			memRFAddr = frame_req->getParamAndData()->returnVector()
				.at(frame_req->getParamAndData()->returnVector().size() - 2).at(0);
			cout << "\nMemory address read (Write): " << (int)memRFAddr << endl;

			memory->setCurrentDynamicAddress(memRFAddr);

			memRFVal = frame_req->getParamAndData()->returnVector()
					.at(frame_req->getParamAndData()->returnVector().size() - 1).at(0);
			memory->writeInDynamic(memRFVal);

			if(memRFAddr == 0x06)
				memory->setFTMstatus(memRFVal & 1);

			// todo: Put handling if the pause is needed
			//handleMemoryInput();

			break;
	}

	return retVector;
}

// The delay time is the same for SOF and EOF
void nfc_tag::frameDelay()
{
	if(RF_BIT_OUTPUT_RATE == BitRate_26Kbit)
	{
		wait(76, SC_US);
	}
	else
	{
		wait(302, SC_US);
	}
}

void nfc_tag::testMemory()
{
	while(true)
	{
		wait();

		if(memiShiba)
		{
			//cout << "\nMEMI: " << (int)memory->mailbox[5] << endl;
		}
	}
}

void nfc_tag::outputCurrentI2C()
{
	while(true)
	{
		wait();

		// The energy is sent in milliamperes
		// Only the "typical" values are being used

		// TODO: check if it will also send here or not...
		if(harvestStatus)
			currentOutI2C.write(0);
		else
			currentOutI2C.write(Configuration::instance()->getNFCPowerConsumption(enState_I2C));
	}
}

void nfc_tag::outputCurrentRF()
{
	while(true)
	{
		wait();

		// The energy is sent in milliamperes
		// Only the "typical" values are being used

		if(harvestStatus)
			currentOutRF.write(Configuration::instance()->getNFCPowerHarvesting());
		else
			currentOutRF.write(Configuration::instance()->getNFCPowerConsumption(enState_RF));
	}
}

void nfc_tag::setHarvestStatus()
{
	while(true)
	{
		wait();

		// check the dynamic register EH_EN, 0b position, to see if the Energy Harvesting is turned on
		if((memory->getHarvestRegisterStatus() & 0x01) == 1)
		{
			harvestStatus = true;
		}
		else
		{
			harvestStatus = false;
		}
	}
}

} /* namespace pss */
