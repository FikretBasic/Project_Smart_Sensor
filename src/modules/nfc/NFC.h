/*
 * NFC.h
 *
 *  Created on: 8 May 2018
 *      Author: its2016
 */

#ifndef MODULES_NFC_NFC_H_
#define MODULES_NFC_NFC_H_

#include "../Includes.h"
#include "NFCMemory.h"
#include "nfc_frame/RFFrameRequest.h"
#include "nfc_frame/RFFrameResponse.h"

namespace pss {

SC_MODULE (nfc_tag)
{
public:

	/****************
	 * 	Ports and Pins
	 ****************/

	sc_in<bool> scl;
	sc_in<bool> enable;
	sc_in<bool> sda_in;
	sc_out<bool> sda_out;
	sc_out<bool> gpo;
	sc_out<double> v_eh;

	// ****** nfc signals ******

	sc_in<bool> rf_clk_in;
	sc_in<bool> rf_clk_out;
	sc_in<bool> enable_rf_in;
	sc_out<bool> enable_rf_out;
	sc_in<bool> antenna_in;
	sc_out<bool> antenna_out;
	sc_out<double> currentOutI2C;
	sc_out<double> currentOutRF;

	/****************
	 * Thread Functions
	 ****************/

	void i2c();
	void rf_receive();
	void rf_send();

	void outputCurrentI2C();
	void outputCurrentRF();
	void setHarvestStatus();

	void testMemory();
	bool memiShiba;
	int counti;

	// todo: implement a watchdog function which will automatically reset the mailbox

	SC_CTOR(nfc_tag)
	{
		memory = new nfc::NFCMemory();

		opState = start;
		enState_I2C = idle_i2c;
		enState_RF = idle_rf;
		harvestStatus = 0;
		mem_buffer = 0;
		E2 = 0;
		read_or_write = 0;
		addressBuffer = 0;

		startResponse = false;

		memiShiba = true;
		counti = 0;

		SC_THREAD(i2c);
		sensitive << scl.pos();

		SC_THREAD(rf_receive);
		sensitive << rf_clk_out.pos();

		SC_THREAD(rf_send);
		sensitive << rf_clk_out.pos();

		SC_THREAD(testMemory);
		sensitive << scl.pos();

		SC_THREAD(outputCurrentI2C);
		sensitive << scl.pos();

		SC_THREAD(outputCurrentRF);
		sensitive << rf_clk_out.pos();

		SC_THREAD(setHarvestStatus);
		sensitive << rf_clk_out.pos();
	};

private:
	nfc::NFCMemory *memory;

	byte checkDeviceAddressValue();
	void handleMemoryInput();

	void frameDelay();

	std::vector<byte> processRfRequest(nfc::RF_Frame_Request *frame_req);

	OperativeStates opState;
	EnergyStates_NFC enState_I2C;
	EnergyStates_NFC enState_RF;
	bool harvestStatus;

	byte mem_buffer;
	byte E2;
	byte read_or_write;
	int addressBuffer;

	std::vector<bool> vectorToSend;
	bool startResponse;
};

} /* namespace pss */

#endif /* MODULES_NFC_NFC_H_ */
