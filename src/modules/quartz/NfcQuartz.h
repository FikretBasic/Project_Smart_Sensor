/*
 * NfcQuartz.h
 *
 *  Created on: 4 Jun 2018
 *      Author: its2016
 */

#ifndef MODULES_QUARTZ_NFCQUARTZ_H_
#define MODULES_QUARTZ_NFCQUARTZ_H_

#include "../Includes.h"

namespace quartz {

SC_MODULE(nfcQuartz)
{
public:

	/*************/
	/*  Signals  */
	/*************/
	sc_in<bool> clock_in;
	sc_in<bool> enabled;
	sc_out<bool> clock_out_input;
	sc_out<bool> clock_out_output;

	void writeClock();

	SC_CTOR(nfcQuartz)
	{
		edge_input = false;
		edge_output = false;
		ticks_input = 0;
		ticks_output = 0;

		SC_THREAD(writeClock);
		sensitive << clock_in.pos();
	};

private:
	bool edge_input;
	bool edge_output;
	unsigned int ticks_input;
	unsigned int ticks_output;

	void writeClockInput();
	void writeClockOutput();
};


} /* namespace nfc */

#endif /* MODULES_QUARTZ_NFCQUARTZ_H_ */
