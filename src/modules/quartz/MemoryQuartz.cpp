/*
 * MemoryQuartz.cpp
 *
 *  Created on: 17 May 2018
 *      Author: its2016
 */

#include "MemoryQuartz.h"

namespace quartz {

void memoryQuartz::writeClock()
{
	while(true)
	{
		wait();

		if(enabled.read() == true && ticks++ >= FREQUENCY_FRAM_SCL / 2)
		{
			clock_out.write(edge);
			edge = !edge;
			ticks = 0;
		}
	}
}

} /* namespace nfc */
