/*
 * NfcQuartz.cpp
 *
 *  Created on: 4 Jun 2018
 *      Author: its2016
 */

#include "NfcQuartz.h"

namespace quartz {

void nfcQuartz::writeClock()
{
	while(true)
	{
		wait();

		writeClockInput();
		writeClockOutput();
	}
}

void nfcQuartz::writeClockInput()
{
	if(enabled.read() == true && ticks_input++ >= RF_BIT_INPUT_RATE / 2)
	{
		clock_out_input.write(edge_input);
		edge_input = !edge_input;
		ticks_input = 0;
	}
}

void nfcQuartz::writeClockOutput()
{
	if(enabled.read() == true && ticks_output++ >= RF_BIT_OUTPUT_RATE / 2)
	{
		clock_out_output.write(edge_output);
		edge_output = !edge_output;
		ticks_output = 0;
	}
}

} /* namespace nfc */
