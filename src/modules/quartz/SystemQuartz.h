/*
 * SystemQuartz.h
 *
 *  Created on: 17 May 2018
 *      Author: Fikret Basic
 */

#ifndef MODULES_QUARTZ_SYSTEMQUARTZ_H_
#define MODULES_QUARTZ_SYSTEMQUARTZ_H_

#include "../Includes.h"

namespace quartz {

SC_MODULE(systemQuartz)
{
public:

	/*************/
	/*  Signals  */
	/*************/
	sc_in<bool> clock_in;
	sc_in<bool> enabled;
	sc_out<bool> clock_out;

	void writeClock();

	SC_CTOR(systemQuartz)
	{
		edge = false;
		ticks = 0;

		SC_THREAD(writeClock);
		sensitive << clock_in.pos();
	};

private:
	bool edge;
	unsigned int ticks;
};

} /* namespace nfc */

#endif /* MODULES_QUARTZ_SYSTEMQUARTZ_H_ */
