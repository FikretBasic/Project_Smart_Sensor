/*
 * SystemQuartz.cpp
 *
 *  Created on: 17 May 2018
 *      Author: its2016
 */

#include "SystemQuartz.h"

namespace quartz {

void systemQuartz::writeClock()
{
	while(true)
	{
		wait();

		//clock_out.write(clock_in);

		if(enabled.read() == true && ++ticks >= 11)
		{
			clock_out.write(edge);
			edge = !edge;
			ticks = 0;
		}
	}
}

} /* namespace nfc */
