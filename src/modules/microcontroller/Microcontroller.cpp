/*
 * Microcontroller.cpp
 *
 *  Created on: 27 Aug 2018
 *      Author: its2016
 */

#include "Microcontroller.h"

namespace pss {

std::stringstream ss;

// ************************* MICROCONTROLLER INNER FUNCTIONS *************************

void microcontroller::output_current()
{
	while(true)
	{
		wait();

		currentOutUC.write(Configuration::instance()->getUCPowerConsumption(enState));
	}
}

void microcontroller::program_control()
{
	int loopControl = 0;
	int loopLimit = 0;

	cout << "[INFO][" << sc_time_stamp() << "] " << "Currently running the program: "
			<< Configuration::instance()->getUCPRogram() << endl;

	while(true)
	{
		wait();

		if(Configuration::instance()->getUCPRogram() == 0)
		{
			this->basic_nfc_memory(loopControl, loopLimit);
		}
		else if(Configuration::instance()->getUCPRogram() == 1)
		{
			this->adv_nfc_sec_memory(loopControl, loopLimit);
		}
	}
}

void microcontroller::basic_nfc_memory(int &loopControl, int &loopLimit)
{
	bool checkMailboxStatus = false;
	byte dataMem = 0x00;
	bool statusMem = false;

	switch(this->basic_nfc_memory_state)
	{
		// wait until the break signal is received to start checking if the new data is ready
		// to be processed
		case 0:
			switch(inner_0_basic_nfc_memory_state)
			{
				case 0:
					if(gpo.read())
					{
						SimulationOutput::instance()->printSimStatus("GPO active, reading from NFC",
								sc_time_stamp(), SIM_INFO);

						this->basic_nfc_memory_state = 1;
						enState = active_uc;
					}
					else
					{
						if(enState == deep_sleep_uc)
						{
							if(++deepSleepStandyBy >= 10000)
							{
								checkMailboxStatus = true;
								deepSleepStandyBy = 0;
							}
						}
						else
						{
							if(++sleepStandyBy >= 1000)
							{
								checkMailboxStatus = true;
								sleepStandyBy = 0;
							}
						}

						// only check if there has been 100 iterations while in deep sleep state
						// OR check for every iteration if it is in the sleep state
						if(checkMailboxStatus)
						{
							memAddrNFC = 0x2006;
							readValSizeNFC = 1;
							readOpNFC = true;
							randomReadNFC = true;

							this->inner_0_basic_nfc_memory_state = 1;
							// set active energy state for checking the status
							enState = active_uc;
						}
					}
					break;

				case 1:
					if(!readOpNFC)
					{
						if(memValuesNFC.front() == 0x05)
						{
							SimulationOutput::instance()->printSimStatus("RF has put a message to NFC through the mailbox",
									sc_time_stamp(), SIM_INFO);

							this->basic_nfc_memory_state = 1;
							deepSleepIter = 0;
						}
						else
						{
							//cout << "[INFO] There is no new messages in the mailbox. Returning to standby." << endl;
							this->inner_0_basic_nfc_memory_state = 0;

							// Set back to sleep or deep sleep state
							if(deepSleepIter >= 100)
							{
								enState = deep_sleep_uc;
							}
							else
							{
								enState = sleep_uc;
								deepSleepIter++;
							}
						}

						memValuesNFC.pop();
					}
					break;
			}

			break;

		// check to see the size of the memory inside the mailbox
		case 1:
			SimulationOutput::instance()->printSimStatus("Checking the size of the data in the mailbox",
					sc_time_stamp(), SIM_INFO);

			randomReadNFC = true;
			memAddrNFC = 0x2007;
			readValSizeNFC = 1;
			readOpNFC = true;

			loopLimit = 10000;
			this->basic_nfc_memory_state = 2;
			break;

		// wait for the NFC to finish its I2C transfer and then read the data
		case 2:
			if(!readOpNFC)
			{
				if(memValuesNFC.size() > 0)
				{
					ss << (int)memValuesNFC.front();
					SimulationOutput::instance()->printSimStatus("The size of the Mailbox is: " + ss.str(),
							sc_time_stamp(), SIM_INFO);
					ss.clear();

					readValSizeNFC = memValuesNFC.front();
					memValuesNFC.pop();
					this->basic_nfc_memory_state = 3;
				}
				else
				{
					SimulationOutput::instance()->printSimStatus("Wrongly received amount of the NFC mailbox size",
							sc_time_stamp(), SIM_ERROR);

					this->basic_nfc_memory_state = 99;
				}
				loopControl = 0;
			}
			// if the operation is hanging, initiate restart of the program
			else if(++loopControl >= loopLimit)
			{
				this->basic_nfc_memory_state = 99;
				loopControl = 0;
			}
			break;

		// Read the mailbox message by I2C that is put earlier by RF
		case 3:
			SimulationOutput::instance()->printSimStatus("Reading the mailbox message with the I2C protocol",
					sc_time_stamp(), SIM_INFO);
			memAddrNFC = 0x2008;
			randomReadNFC = true;
			readOpNFC = true;

			loopLimit = 10000 * readValSizeNFC;
			this->basic_nfc_memory_state = 4;
			break;

		// read the data from the NFC Tag
		case 4:
			if(!readOpNFC)
			{
				if (memValuesNFC.size() > 0)
				{
					SimulationOutput::instance()->printUCStatus("Values to be saved to RAM: ", sc_time_stamp());

					// Saving to the RAM
					while(memValuesNFC.size() > 0)
					{
						ucRAM->writeInMemory(memValuesNFC.front());
						cout << (int)memValuesNFC.front() << " ";
						memValuesNFC.pop();
					}
					cout << endl;

					this->basic_nfc_memory_state = 5;
				}
				else
				{
					SimulationOutput::instance()->printSimStatus("NO data from the NFC received!",
							sc_time_stamp(), SIM_WARNING);

					this->basic_nfc_memory_state = 99;
				}
				loopControl = 0;
			}
			// if the operation is hanging, initiate restart of the program
			else if(++loopControl >= loopLimit)
			{
				this->basic_nfc_memory_state = 99;
				loopControl = 0;
			}

			break;

		// Transfer the received data to the RAM
		case 5:
			if(memValuesNFC.size() > 0)
			{
				ucRAM->writeInMemory(memValuesNFC.front());
				memValuesNFC.pop();
			}
			else
			{
				SimulationOutput::instance()->printUCStatus("Values finished saving to RAM", sc_time_stamp());
				this->basic_nfc_memory_state = 6;
			}
			break;

		// Initiate the transfer from the Microcontroller RAM to the FRAM chip
		case 6:

			// for each cycle, read the saved data
			statusMem = ucRAM->readFromMemory(dataMem, memValues.size()+1);
			if(statusMem)
			{
				memValues.push(dataMem);
			}
			else
			{
				// save new addr position and start sending the values through I2C to the FRAM
				memAddr = ucRAM->getLastAddrPosition();
				ucRAM->setNewAddrPosition(memValues.size());
				writeOp = true;

				loopLimit = 1000 * memValues.size();
				this->basic_nfc_memory_state = 7;
			}
			break;

		// Finalise the program by getting the confirmation that the transfer is complete
		case 7:

			if(!writeOp)
			{
				SimulationOutput::instance()->printSimStatus("Values from RAM have been written to the FRAM",
						sc_time_stamp(), SIM_INFO);
				SimulationOutput::instance()->printSimStatus("The program \"Basic NFC FRAM\" is finished",
						sc_time_stamp(), SIM_INFO);

				this->basic_nfc_memory_state = 99;
			}
			else if(++loopControl >= loopLimit)
			{
				SimulationOutput::instance()->printSimStatus("The program hangs! The values have not been written to the FRAM",
						sc_time_stamp(), SIM_ERROR);

				this->basic_nfc_memory_state = 99;
				loopControl = 0;
			}

			break;

		// this is the reset case
		default:
			opStatesNFC = start;

			writeToNFC = false;
			readFromNFC = false;

			randomReadNFC = false;
			systemModeNFC = false;

			writeOpNFC = false;
			readOpNFC = false;
			readValSizeNFC = 0;
			memAddrNFC = 0;
			while(!memValuesNFC.empty())
			{
				memValuesNFC.pop();
			}

			loopControl = 0;
			loopLimit = 0;
			this->basic_nfc_memory_state = 0;
			enState = sleep_uc;

			SimulationOutput::instance()->printSimStatus("Initiating program reset!",
					sc_time_stamp(), SIM_INFO);
	}
}

void microcontroller::adv_nfc_sec_memory(int &loopControl, int &loopLimit)
{
	bool checkMailboxStatus = false;
	byte dataMem = 0x00;
	bool statusMem = false;

	switch(this->adv_nfc_sec_memory_state)
	{
		// wait until the break signal is received to start checking if the new data is ready
		// to be processed
		case 0:
			switch(inner_0_adv_nfc_sec_memory_state)
			{
				case 0:
					if(gpo.read())
					{
						SimulationOutput::instance()->printSimStatus("GPO active, reading from NFC",
								sc_time_stamp(), SIM_INFO);

						this->adv_nfc_sec_memory_state = 1;
						enState = active_uc;
					}
					else
					{
						if(enState == deep_sleep_uc)
						{
							if(++deepSleepStandyBy >= 10000)
							{
								checkMailboxStatus = true;
								deepSleepStandyBy = 0;
							}
						}
						else
						{
							if(++sleepStandyBy >= 1000)
							{
								checkMailboxStatus = true;
								sleepStandyBy = 0;
							}
						}

						// only check if there has been 100 iterations while in deep sleep state
						// OR check for every iteration if it is in the sleep state
						if(checkMailboxStatus)
						{
							memAddrNFC = 0x2006;
							readValSizeNFC = 1;
							readOpNFC = true;
							randomReadNFC = true;

							this->inner_0_adv_nfc_sec_memory_state = 1;
							// set active energy state for checking the status
							enState = active_uc;
						}
					}
					break;

				case 1:
					if(!readOpNFC)
					{
						if(memValuesNFC.front() == 0x05)
						{
							SimulationOutput::instance()->printSimStatus("RF has put a message to NFC through the mailbox",
									sc_time_stamp(), SIM_INFO);

							this->adv_nfc_sec_memory_state = 1;
							deepSleepIter = 0;
						}
						else
						{
							//cout << "[INFO] There is no new messages in the mailbox. Returning to standby." << endl;
							this->inner_0_adv_nfc_sec_memory_state = 0;

							// Set back to sleep or deep sleep state
							if(deepSleepIter >= 100)
							{
								enState = deep_sleep_uc;
							}
							else
							{
								enState = sleep_uc;
								deepSleepIter++;
							}
						}

						memValuesNFC.pop();
					}
					break;
			}

			break;

		// check to see the size of the memory inside the mailbox
		case 1:
			SimulationOutput::instance()->printSimStatus("Checking the size of the data in the mailbox",
					sc_time_stamp(), SIM_INFO);

			randomReadNFC = true;
			memAddrNFC = 0x2007;
			readValSizeNFC = 1;
			readOpNFC = true;

			loopLimit = 10000;
			this->adv_nfc_sec_memory_state = 2;
			break;

		// wait for the NFC to finish its I2C transfer and then read the data
		case 2:
			if(!readOpNFC)
			{
				if(memValuesNFC.size() > 0)
				{
					ss << (int)memValuesNFC.front();
					SimulationOutput::instance()->printSimStatus("The size of the Mailbox is: " + ss.str(),
							sc_time_stamp(), SIM_INFO);
					ss.clear();

					readValSizeNFC = memValuesNFC.front();
					memValuesNFC.pop();
					this->adv_nfc_sec_memory_state = 3;
				}
				else
				{
					SimulationOutput::instance()->printSimStatus("Wrongly received amount of the NFC mailbox size",
							sc_time_stamp(), SIM_ERROR);

					this->adv_nfc_sec_memory_state = 99;
				}
				loopControl = 0;
			}
			// if the operation is hanging, initiate restart of the program
			else if(++loopControl >= loopLimit)
			{
				this->adv_nfc_sec_memory_state = 99;
				loopControl = 0;
			}
			break;

		// Read the mailbox message by I2C that is put earlier by RF
		case 3:
			SimulationOutput::instance()->printSimStatus("Reading the mailbox message with the I2C protocol",
					sc_time_stamp(), SIM_INFO);
			memAddrNFC = 0x2008;
			randomReadNFC = true;
			readOpNFC = true;

			cout << "READ VAL SIZE: " << (int)readValSizeNFC << endl;

			loopLimit = 10000 * readValSizeNFC;
			this->adv_nfc_sec_memory_state = 4;
			break;

		// read the data from the NFC Tag
		case 4:
			if(!readOpNFC)
			{
				if (memValuesNFC.size() > 0)
				{
					SimulationOutput::instance()->printUCStatus("Values to be saved to RAM: ", sc_time_stamp());

					// Saving to the RAM
					while(memValuesNFC.size() > 0)
					{
						ucRAM->writeInMemory(memValuesNFC.front());
						cout << (int)memValuesNFC.front() << " ";
						memValuesNFC.pop();
					}
					cout << endl;

					this->adv_nfc_sec_memory_state = 5;
				}
				else
				{
					SimulationOutput::instance()->printSimStatus("NO data from the NFC received!",
							sc_time_stamp(), SIM_WARNING);

					this->adv_nfc_sec_memory_state = 99;
				}
				loopControl = 0;
			}
			// if the operation is hanging, initiate restart of the program
			else if(++loopControl >= loopLimit)
			{
				this->adv_nfc_sec_memory_state = 99;
				loopControl = 0;
			}

			break;

		// Transfer the received data to the RAM
		case 5:
			if(memValuesNFC.size() > 0)
			{
				ucRAM->writeInMemory(memValuesNFC.front());
				memValuesNFC.pop();
			}
			else
			{
				SimulationOutput::instance()->printUCStatus("Values finished saving to RAM", sc_time_stamp());
				inValuesSec.clear();
				outValuesSec.clear();
				this->adv_nfc_sec_memory_state = 6;
			}
			break;

		// Initiate the transfer from the Microcontroller RAM to the Security chip
		case 6:

			// for each cycle, read the saved data
			statusMem = ucRAM->readFromMemory(dataMem, inValuesSec.size()+1);
			if(statusMem)
			{
				inValuesSec.push_back(dataMem);
			}
			else
			{
				// Sends an array of three values to perform the sec. func. with 0x01 code (Mutual Authentication using DTLS)
				// On return it gets an array of result values

				executeSecurityFunction(inValuesSec, outValuesSec, Configuration::instance()->getSecFunction());

				if (!outValuesSec.empty())
				{
					cout << "[Security][" << sc_time_stamp() << "] Received results from the sec. process: ";
					for(size_t i = 0; i < outValuesSec.size(); i++)
					{
						cout << (int)outValuesSec.at(i) << " ";
					}
					cout << endl;
				}
				else
				{
					cout << "[Security][" << sc_time_stamp() << "] NO output received from the security chip!" << endl;
				}

				//loopLimit = 1000 * memValues.size();
				ucRAM->resetRAM();
				this->adv_nfc_sec_memory_state = 7;
			}
			break;

		case 7:
			// Transfer the received data to the RAM
			if(outValuesSec.size() > 0)
			{
				//cout << "DO I Come here..."
				ucRAM->writeInMemory(outValuesSec.front());
				outValuesSec.erase(outValuesSec.begin());
			}
			else
			{
				SimulationOutput::instance()->printUCStatus("Values finished saving to RAM", sc_time_stamp());
				this->adv_nfc_sec_memory_state = 8;
			}
			break;

		// Initiate the transfer from the Microcontroller RAM to the FRAM chip
		case 8:

			// for each cycle, read the saved data
			statusMem = ucRAM->readFromMemory(dataMem, memValues.size()+1);
			if(statusMem)
			{
				memValues.push(dataMem);
			}
			else
			{
				// save new addr position and start sending the values through I2C to the FRAM
				memAddr = ucRAM->getLastAddrPosition();
				ucRAM->setNewAddrPosition(memValues.size());
				writeOp = true;

				loopLimit = 1000 * memValues.size();
				this->adv_nfc_sec_memory_state = 9;
			}
			break;

		// Finalise the program by getting the confirmation that the transfer is complete
		case 9:

			if(!writeOp)
			{
				SimulationOutput::instance()->printSimStatus("Values from RAM have been written to the FRAM",
						sc_time_stamp(), SIM_INFO);
				SimulationOutput::instance()->printSimStatus("The program \"Basic NFC FRAM\" is finished",
						sc_time_stamp(), SIM_INFO);

				this->adv_nfc_sec_memory_state = 99;
			}
			else if(++loopControl >= loopLimit)
			{
				SimulationOutput::instance()->printSimStatus("The program hangs! The values have not been written to the FRAM",
						sc_time_stamp(), SIM_ERROR);

				this->adv_nfc_sec_memory_state = 99;
				loopControl = 0;
			}

			break;

		// this is the reset case
		default:
			opStatesNFC = start;

			writeToNFC = false;
			readFromNFC = false;

			randomReadNFC = false;
			systemModeNFC = false;

			writeOpNFC = false;
			readOpNFC = false;
			readValSizeNFC = 0;
			memAddrNFC = 0;
			while(!memValuesNFC.empty())
			{
				memValuesNFC.pop();
			}

			opStatesSec = start;
			writeOpSec = false;
			readOpSec = false;
			randomReadSec = false;
			systemModeSec = false;
			memAddrSec = 1;
			readValSizeSec = 0;
			while(!memValuesSec.empty())
			{
				memValuesSec.pop();
			}
			inValuesSec.clear();
			outValuesSec.clear();

			loopControl = 0;
			loopLimit = 0;
			this->adv_nfc_sec_memory_state = 0;
			enState = sleep_uc;

			SimulationOutput::instance()->printSimStatus("Initiating program reset!",
					sc_time_stamp(), SIM_INFO);
	}
}


void microcontroller::sclGenerator()
{
	while(true)
	{
		wait();

		if(Configuration::instance()->getClockUC() == 1)
		{
			scl_mem.write(edge_output);
			scl_nfc.write(edge_output);
			scl_sec.write(edge_output);
			edge_output = !edge_output;
		}
		else
		{
			if(ticks_output++ >= 48 / 2)
			{
				scl_mem.write(edge_output);
				scl_nfc.write(edge_output);
				scl_sec.write(edge_output);
				edge_output = !edge_output;
				ticks_output = 0;
			}
		}

	}
}


// ************************* MICROCONTROLLER INNER FUNCTIONS *************************

// ************************* I2C CONTROL FUNCTIONS *************************

void microcontroller::i2c_mem()
{
	byte memBuffer = 0;
	int iter = 7;

	a1_mem.write(1);
	a2_mem.write(1);
	wp_mem.write(0);

	while(true)
	{
		wait();

		if(writeOp)
		{
			// write op
			if(memValues.size() > 0)
			{
				writeInMemory(memBuffer, iter);
			}
		}
		else if(readOp)
		{
			// read op
			readFromMemory(memBuffer, iter);
		}
		else if(sleepOp)
		{
			// sleep op
			setMemToSleep(memBuffer, iter);
		}
	}
}

void microcontroller::i2c_nfc()
{
	byte memBuffer = 0;
	int iter = 7;

	while(true)
	{
		wait();

		if(writeOpNFC)
		{
			// write op
			if(memValuesNFC.size() > 0)
			{
				writeInMemoryNFC(memBuffer, iter);
			}
		}
		else if(readOpNFC)
		{
			// read op
			readFromMemoryNFC(memBuffer, iter);
		}
	}
}

void microcontroller::i2c_sec()
{
	byte memBuffer = 0;
	int iter = 7;

	while(true)
	{
		wait();

		if(writeOpSec)
		{
			// write op
			if(memValuesSec.size() > 0)
			{
				writeInMemorySec(memBuffer, iter);
			}
		}
		else if(readOpSec)
		{
			// read op
			readFromMemorySec(memBuffer, iter);
		}
	}
}

// ************************* I2C CONTROL FUNCTIONS *************************

// ************************* FRAM HELP FUNCTIONS *************************


void microcontroller::writeInMemory(byte &memBuffer, int &iter)
{
	switch(opStatesMem)
	{
		case start:
			//wait(10, SC_US);
			enable_mem.write(1);
			opStatesMem = dev_addr;

			// 10101100 - device address and instruction
			memBuffer = 0xac;

			break;

		case dev_addr:

			if(iter < 0)
			{
				sda_out_mem.write(1);

				if (sleepState)
				{
					//wait();
					cout << "\nSLEEP STATE WRITE" << endl;
					enable_mem.write(0);
					opStatesMem = start;
					iter = 7;
					sleepState = false;
				}
				else
				{
					opStatesMem = mem_addr;
					memBuffer = 0;
					iter = 16;
				}
			}
			else
			{
				sda_out_mem.write((memBuffer >> iter) & 1);
				iter--;
			}

			break;

		case mem_addr:

			if(iter < 0)
			{
				sda_out_mem.write(1);
				opStatesMem = rd_wr;
				memBuffer = memValues.front();
				iter = 7;
			}
			else
			{
				/*if(iter == 16)
					memAddr = 100;*/

				sda_out_mem.write((memAddr >> iter) & 1);
				iter--;
			}

			break;

		case rd_wr:

			if(iter < 0)
			{
				sda_out_mem.write(1);

				if (memValues.size() == 1)
				{
					opStatesMem = stop;
					memBuffer = 0;
					//memAddr = 0;
					iter = 7;
				}
				else
				{
					//writeIter--;
					memValues.pop();
					iter = 7;
					memBuffer = memValues.front();
				}
			}
			else
			{
				sda_out_mem.write((memBuffer >> iter) & 1);
				iter--;
			}

			break;

		case stop:
			memValues.pop();
			enable_mem.write(0);
			opStatesMem = start;
			writeOp = false;

			break;

		default:
			opStatesMem = start;

	}
}

void microcontroller::readFromMemory(byte &memBuffer, int &iter)
{
	switch(opStatesMem)
	{
		case start:
			wait();
			//wait();
			enable_mem.write(1);
			opStatesMem = dev_addr;

			// 10101100 - device address and instruction
			memBuffer = 0xad;

			break;

		case dev_addr:

			if(iter < 0)
			{
				sda_out_mem.write(1);

				if (sleepState)
				{
					//wait();
					cout << "\nSLEEP STATE READ" << endl;
					enable_mem.write(0);
					opStatesMem = start;
					iter = 7;
					sleepState = false;
				}
				else
				{
					opStatesMem = mem_addr;
					memBuffer = 0;
					iter = 16;
				}
			}
			else
			{
				sda_out_mem.write((memBuffer >> iter) & 1);
				iter--;
			}

			break;

		case mem_addr:

			if(iter < 0)
			{
				sda_out_mem.write(1);
				opStatesMem = rd_wr;
				memBuffer = 0;
				iter = 7;

				// wait one cycle to synchronise
				wait();
			}
			else
			{
				//if(iter == 16)
					//memAddr = 100;

				sda_out_mem.write((memAddr >> iter) & 1);
				iter--;
			}

			break;

		case rd_wr:

			if(iter < 0)
			{
				sda_out_mem.write(1);
				cout << "\nMemory buffer: " << (int)memBuffer << " [" << memAddr << "]" << endl;
				memValues.push(memBuffer);

				if (memValues.size() == readValSize)
				{
					opStatesMem = stop;
				}
				else
				{
					memAddr++;
				}

				memBuffer = 0;
				iter = 7;
			}
			else
			{
				byte cur_val = sda_in_mem.read();
				memBuffer |= (cur_val << iter);
				iter--;
			}

			break;

		case stop:
			enable_mem.write(0);
			opStatesMem = start;

			readOp = false;

			break;

		default:
			opStatesMem = start;

	}
}

void microcontroller::setMemToSleep(byte &memBuffer, int &iter)
{
	switch(opStatesMem)
	{
		case start:
			wait();
			enable_mem.write(1);
			opStatesMem = dev_addr;
			// 10101100 - device address and instruction
			memBuffer = 0xf8;

			break;

		case dev_addr:

			if(iter < 0)
			{
				sda_out_mem.write(1);
				opStatesMem = rd_wr;
				memBuffer = 0x86;
				iter = 7;
			}
			else
			{
				sda_out_mem.write((memBuffer >> iter) & 1);
				iter--;
			}

			break;

		case rd_wr:

			if(iter < 0)
			{
				sda_out_mem.write(1);

				if (sda_in_mem.read() == 1)
				{
					sleepState = true;
				}

				opStatesMem = stop;
				memBuffer = 0;
				iter = 7;
			}
			else
			{
				sda_out_mem.write((memBuffer >> iter) & 1);
				iter--;
			}

			break;

		case stop:
			enable_mem.write(0);
			opStatesMem = start;

			sleepOp = false;

			break;

		default:
			opStatesMem = start;

	}
}

// ************************* FRAM HELP FUNCTIONS *************************

// ************************* NFC HELP FUNCTIONS *************************

void microcontroller::writeInMemoryNFC(byte &memBuffer, int &iter)
{
	switch(opStatesNFC)
	{
		case start:
			//wait(10, SC_US);
			enable_nfc.write(1);
			opStatesNFC = dev_addr;

			if(systemModeNFC)
			{
				memBuffer = 0xae;
			}
			else
			{
				// 10100110 - device address and instruction
				memBuffer = 0xa6;
			}

			break;

		case dev_addr:

			if(iter < 0)
			{
				sda_out_nfc.write(1);

				if (sleepState)
				{
					//wait();
					cout << "\nSLEEP STATE WRITE" << endl;
					enable_nfc.write(0);
					opStatesNFC = start;
					iter = 7;
					sleepState = false;
				}
				else
				{
					opStatesNFC = mem_addr;
					memBuffer = 0;
					iter = 15;
				}
			}
			else
			{
				sda_out_nfc.write((memBuffer >> iter) & 1);
				iter--;
			}

			break;

		case mem_addr:

			if(iter < 0)
			{
				sda_out_nfc.write(1);
				opStatesNFC = rd_wr;
				memBuffer = memValuesNFC.front();
				iter = 7;
			}
			else
			{
				/*if(iter == 16)
					memAddrNFC = 100;*/

				sda_out_nfc.write((memAddrNFC >> iter) & 1);
				iter--;
			}

			break;

		case rd_wr:

			if(iter < 0)
			{
				sda_out_nfc.write(1);

				if (memValuesNFC.size() == 1)
				{
					opStatesNFC = stop;
					memBuffer = 0;
					//memAddrNFC = 0;
					iter = 7;
				}
				else
				{
					//writeIter--;
					memValuesNFC.pop();
					iter = 7;
					memBuffer = memValuesNFC.front();
				}
			}
			else
			{
				sda_out_nfc.write((memBuffer >> iter) & 1);
				iter--;
			}

			break;

		case stop:
			memValuesNFC.pop();
			enable_nfc.write(0);
			opStatesNFC = start;
			writeOpNFC = false;

			break;

		default:
			opStatesNFC = start;

	}
}

void microcontroller::readFromMemoryNFC(byte &memBuffer, int &iter)
{
	switch(opStatesNFC)
	{
		case start:
			wait();
			//wait();
			enable_nfc.write(1);
			opStatesNFC = dev_addr;

			if (randomReadNFC)
			{
				if(systemModeNFC)
				{
					memBuffer = 0xae;
				}
				else
				{
					// 10100110 - device address and instruction
					memBuffer = 0xa6;
				}
			}
			else
			{
				if(systemModeNFC)
				{
					memBuffer = 0xaf;
				}
				else
				{
					// 10100110 - device address and instruction
					memBuffer = 0xa7;
				}
			}

			break;

		case dev_addr:

			if(iter < 0)
			{
				sda_out_nfc.write(1);

				if (sleepState)
				{
					//wait();
					cout << "\nSLEEP STATE READ" << endl;
					enable_nfc.write(0);
					opStatesNFC = start;
					iter = 7;
					sleepState = false;
				}
				else
				{
					memBuffer = 0;

					if(randomReadNFC)
					{
						opStatesNFC = mem_addr;
						iter = 15;
					}
					else
					{
						opStatesNFC = rd_wr;
						iter = 7;

						// wait one cycle to synchronise
						//wait();
					}
				}
			}
			else
			{
				sda_out_nfc.write((memBuffer >> iter) & 1);
				iter--;
			}

			break;

		case mem_addr:

			if(iter < 0)
			{
				sda_out_nfc.write(1);

				randomReadNFC = false;
				enable_nfc.write(0);
				opStatesNFC = start;

				memBuffer = 0;
				iter = 7;

				// wait one cycle to synchronise
				//wait();
			}
			else
			{
				//if(iter == 16)
					//memAddrNFC = 100;

				sda_out_nfc.write((memAddrNFC >> iter) & 1);
				iter--;
			}

			break;

		case rd_wr:

			if(iter < 0)
			{
				sda_out_nfc.write(1);
				//cout << "\nMemory buffer: " << (int)memBuffer << " [" << memAddrNFC << "]" << endl;
				memValuesNFC.push(memBuffer);

				if (memValuesNFC.size() == readValSizeNFC)
				{
					opStatesNFC = stop;
				}
				else
				{
					memAddrNFC++;
				}

				memBuffer = 0;
				iter = 7;
			}
			else
			{
				//cout << "MIC REC: " << sda_in_nfc.read() << endl;
				byte cur_val = sda_in_nfc.read();
				memBuffer |= (cur_val << iter);
				iter--;
			}

			break;

		case stop:
			enable_nfc.write(0);
			opStatesNFC = start;
			readOpNFC = false;

			break;

		default:
			opStatesNFC = start;

	}
}


// ************************* NFC HELP FUNCTIONS *************************

// ************************* SECURITY HELP FUNCTIONS *************************

void microcontroller::writeInMemorySec(byte &memBuffer, int &iter)
{
	switch(opStatesSec)
	{
		case start:
			//wait(10, SC_US);
			enable_sec.write(1);
			opStatesSec = dev_addr;

			if(systemModeSec)
			{
				memBuffer = 0xae;
			}
			else
			{
				// 10100110 - device address and instruction
				memBuffer = 0xa6;
			}

			break;

		case dev_addr:

			if(iter < 0)
			{
				sda_out_sec.write(1);

				opStatesSec = mem_addr;
				memBuffer = 0;
				iter = 15;
			}
			else
			{
				sda_out_sec.write((memBuffer >> iter) & 1);
				iter--;
			}

			break;

		case mem_addr:

			if(iter < 0)
			{
				sda_out_sec.write(1);
				opStatesSec = rd_wr;
				memBuffer = memValuesSec.front();
				iter = 7;
			}
			else
			{
				/*if(iter == 16)
					memAddrSec = 100;*/

				sda_out_sec.write((memAddrSec >> iter) & 1);
				iter--;
			}

			break;

		case rd_wr:

			if(iter < 0)
			{
				sda_out_sec.write(1);

				if (memValuesSec.size() == 1)
				{
					opStatesSec = stop;
					memBuffer = 0;
					//memAddrSec = 0;
					iter = 7;
				}
				else
				{
					//writeIter--;
					memValuesSec.pop();
					iter = 7;
					memBuffer = memValuesSec.front();
				}
			}
			else
			{
				sda_out_sec.write((memBuffer >> iter) & 1);
				iter--;
			}

			break;

		case stop:
			memValuesSec.pop();
			enable_sec.write(0);
			opStatesSec = start;
			writeOpSec = false;

			break;

		default:
			opStatesSec = start;

	}
}

void microcontroller::readFromMemorySec(byte &memBuffer, int &iter)
{
	switch(opStatesSec)
	{
		case start:
			wait();
			//wait();
			enable_sec.write(1);
			opStatesSec = dev_addr;

			if (randomReadSec)
			{
				if(systemModeSec)
				{
					memBuffer = 0xae;
				}
				else
				{
					// 10100110 - device address and instruction
					memBuffer = 0xa6;
				}
			}
			else
			{
				if(systemModeSec)
				{
					memBuffer = 0xaf;
				}
				else
				{
					// 10100110 - device address and instruction
					memBuffer = 0xa7;
				}
			}

			break;

		case dev_addr:

			if(iter < 0)
			{
				sda_out_sec.write(1);

				memBuffer = 0;

				if(randomReadSec)
				{
					opStatesSec = mem_addr;
					iter = 15;
				}
				else
				{
					opStatesSec = rd_wr;
					iter = 7;

					// wait one cycle to synchronise
					//wait();
				}
			}
			else
			{
				sda_out_sec.write((memBuffer >> iter) & 1);
				iter--;
			}

			break;

		case mem_addr:

			if(iter < 0)
			{
				sda_out_sec.write(1);

				randomReadSec = false;
				enable_sec.write(0);
				opStatesSec = start;

				memBuffer = 0;
				iter = 7;

				// wait one cycle to synchronise
				wait();
			}
			else
			{
				//if(iter == 16)
					//memAddrSec = 100;

				sda_out_sec.write((memAddrSec >> iter) & 1);
				iter--;
			}

			break;

		case rd_wr:

			if(iter < 0)
			{
				sda_out_sec.write(1);
				cout << "\nSec Memory buffer: " << (int)memBuffer << " [" << memAddrSec << "]" << endl;
				memValuesSec.push(memBuffer);

				if (memValuesSec.size() == readValSizeSec)
				{
					opStatesSec = stop;
				}
				else
				{
					memAddrSec++;
				}

				memBuffer = 0;
				iter = 7;
			}
			else
			{
				byte cur_val = sda_in_sec.read();
				memBuffer |= (cur_val << iter);
				iter--;
			}

			break;

		case stop:
			enable_sec.write(0);
			opStatesSec = start;

			readOpSec = false;

			break;

		default:
			opStatesSec = start;

	}
}

void microcontroller::executeSecurityFunction(std::vector<byte> inValues,
		std::vector<byte> &outValues, uint8_t funcCode)
{
	int testCheckIter = 3;
	bool check = false;

	while(testCheckIter > 0)
	{
		// Write to system address 11 to start the process of writing to the work memory (1)
		memValuesSec.push(0x01);
		memAddrSec = 11;
		writeOpSec = true;
		systemModeSec = true;
		wait(70, SC_US);

		// Start sending data to write to the work memory	(2)
		for(size_t i = 0; i < inValues.size(); i++)
		{
			memValuesSec.push(inValues.at(i));
		}
		memAddrSec = 0;
		systemModeSec = false;
		writeOpSec = true;
		wait(100, SC_US);

		// Check if everything is correctly written	(3)
		readOpSec = true;
		randomReadSec = true;
		memAddrSec = 13;
		readValSizeSec = 1;
		systemModeSec = true;
		wait(70, SC_US);

		if(memValuesSec.front() == 0x00)
		{
			cout << "[Security]" << "[" << sc_time_stamp() << "] Successfully first phase written!" << endl;
			memValuesSec.pop();
			check = true;
			break;
		}
		testCheckIter--;
		memValuesSec.pop();
	}

	if(check)
	{
		wait(100, SC_US);
		// Write to system address 11 to stop the process of writing to the work memory (4)
		memValuesSec.push(0x00);
		memAddrSec = 11;
		writeOpSec = true;
		systemModeSec = true;
		wait(100, SC_US);

		// Write the code for a specific security operation (5)
		memValuesSec.push(funcCode);
		memAddrSec = 10;
		writeOpSec = true;
		systemModeSec = true;
		wait(100, SC_US);

		// Write 0x01 to the system position 12 to indicate the start of the security function (6)
		memValuesSec.push(0x01);
		memAddrSec = 12;
		writeOpSec = true;
		systemModeSec = true;
		wait(100, SC_US);

		check = false;
		testCheckIter = 10;

		while(testCheckIter > 0)
		{
			// Check if the value of the register 12 is now 0 (7)
			readOpSec = true;
			randomReadSec = true;
			memAddrSec = 12;
			readValSizeSec = 1;
			systemModeSec = true;
			wait(70, SC_US);

			if(memValuesSec.front() == 0x00)
			{
				cout << "[Security]" << "[" << sc_time_stamp() << "] Process finished!" << endl;
				memValuesSec.pop();
				check = true;
				break;
			}
			{
				cout << "[Security]" << "[" << sc_time_stamp() << "] Still waiting!" << endl;
				memValuesSec.pop();
			}
			testCheckIter--;
			wait(20, SC_MS);
		}
	}
	else
	{
		return;
	}

	if (check)
	{
		// Check if everything is correctly written	(3)
		readOpSec = true;
		randomReadSec = true;
		memAddrSec = 13;
		readValSizeSec = 1;
		systemModeSec = true;
		wait(70, SC_US);

		check = false;

		if(memValuesSec.front() == 0x00)
		{
			check = true;
		}
		memValuesSec.pop();
	}

	if (check)
	{
		wait(100, SC_US);
		// Write to system address 11 to start the process of writing to the work memory (8)
		memValuesSec.push(0x01);
		memAddrSec = 11;
		writeOpSec = true;
		systemModeSec = true;
		wait(70, SC_US);

		// Read the data from the security process (9)
		readOpSec = true;
		randomReadSec = true;
		memAddrSec = 0x00;
		readValSizeSec = inValues.size();
		systemModeSec = false;
		wait(70*inValues.size(), SC_US);

		size_t numOfElements = memValuesSec.size();
		for(size_t i = 0; i < numOfElements; i++)
		{
			outValues.push_back(memValuesSec.front());
			memValuesSec.pop();
		}

		// Write to system address 11 to stop the process of writing to the work memory (10)
		memValuesSec.push(0x00);
		memAddrSec = 11;
		writeOpSec = true;
		systemModeSec = true;
		wait(70, SC_US);

		cout << "[Security]" << "[" << sc_time_stamp() << "] Finished the Security Protocol!" << endl;
	}

}

// ************************* SECURITY HELP FUNCTIONS *************************

} /* namespace testb */
