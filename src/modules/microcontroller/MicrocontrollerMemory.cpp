/*
 * MicrocontrollerMemory.cpp
 *
 *  Created on: 15 Oct 2018
 *      Author: its2016
 */

#include "MicrocontrollerMemory.h"

namespace ucontroller {

MicrocontrollerMemory::MicrocontrollerMemory()
{
	currRAMAddr = 0x01;
}

MicrocontrollerMemory::~MicrocontrollerMemory() {}

void MicrocontrollerMemory::writeInMemory(byte data)
{
	if (currRAMAddr < 8192)
	{
		ramMemory[currRAMAddr] = data;
		currRAMAddr++;
	}
	else
	{
		currRAMAddr = 0x01;
		this->writeInMemory(data);
	}
}

bool MicrocontrollerMemory::readFromMemory(byte &data, size_t addr)
{
	if(currRAMAddr == 0x01 || !(addr < currRAMAddr))
	{
		data = 0xff;
		return false;
	}
	else
	{
		data = ramMemory[addr];
		return true;
	}
}

void MicrocontrollerMemory::setNewAddrPosition(int addrSize)
{
	if(this->addrPositions.size() == 0)
	{
		this->addrPositions.push_back(addrSize);
	}
	else
	{
		if(this->getLastAddrPosition() + addrSize < 4096)
		{
			this->addrPositions.push_back(addrSize);
		}
		else
		{
			this->addrPositions.push_back(0);
		}
	}
}

std::vector<int> MicrocontrollerMemory::getAddrPositions()
{
	return this->addrPositions;
}

int MicrocontrollerMemory::getLastAddrPosition()
{
	if(this->addrPositions.size() == 0)
		return 0x01;
	else
		return this->addrPositions.at(this->addrPositions.size() - 1);
}

void MicrocontrollerMemory::resetRAM()
{
	for (int i = 0; i < 8192; i++)
	{
		ramMemory[i] = 0x00;
	}

	currRAMAddr = 0x01;
	addrPositions.clear();
}


} /* namespace ucontroller */
