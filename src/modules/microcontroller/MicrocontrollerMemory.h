/*
 * MicrocontrollerMemory.h
 *
 *  Created on: 15 Oct 2018
 *      Author: its2016
 */

#ifndef MODULES_MICROCONTROLLER_MICROCONTROLLERMEMORY_H_
#define MODULES_MICROCONTROLLER_MICROCONTROLLERMEMORY_H_

#include "../Includes.h"

namespace ucontroller {

class MicrocontrollerMemory {
public:
	MicrocontrollerMemory();
	virtual ~MicrocontrollerMemory();

	void writeInMemory(byte data);
	bool readFromMemory(byte &data, size_t addr);

	void setNewAddrPosition(int addrSize);
	std::vector<int> getAddrPositions();
	int getLastAddrPosition();

	void resetRAM();

private:
	// the real model has actually 256KB which is not necessary to be allocated in the simulation
	byte ramMemory[8192];			// 8KB RAM
	uint16_t currRAMAddr;

	std::vector<int> addrPositions;
};

} /* namespace ucontroller */

#endif /* MODULES_MICROCONTROLLER_MICROCONTROLLERMEMORY_H_ */
