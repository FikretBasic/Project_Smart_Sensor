/*
 * Microcontroller.h
 *
 *  Created on: 27 Aug 2018
 *      Author: its2016
 */

#ifndef MODULES_MICROCONTROLLER_MICROCONTROLLER_H_
#define MODULES_MICROCONTROLLER_MICROCONTROLLER_H_

#include "../Includes.h"
#include "MicrocontrollerMemory.h"

namespace pss {

SC_MODULE (microcontroller)
{
public:

	/****************
	 * 	Ports and Pins
	 ****************/

	/****** UC-only ports ******/
	sc_in<bool> clk;
	sc_out<double> currentOutUC;

	/****** I2C with the FRAM ports ******/

	sc_out<bool> scl_mem;
	sc_in<bool> sda_in_mem;
	sc_out<bool> sda_out_mem;
	sc_out<bool> enable_mem;

	sc_out<bool> wp_mem;
	sc_out<bool> a1_mem;
	sc_out<bool> a2_mem;

	/****** I2C with the NFC Module ports ******/

	sc_out<bool> scl_nfc;
	sc_in<bool> sda_in_nfc;
	sc_out<bool> sda_out_nfc;
	sc_out<bool> enable_nfc;
	sc_in<bool> gpo;
	sc_in<double> v_eh;

	/****** I2C with the Security Module ports ******/

	sc_out<bool> scl_sec;
	sc_in<bool> sda_in_sec;
	sc_out<bool> sda_out_sec;
	sc_out<bool> enable_sec;

	/****************
	 * Thread Functions
	 ****************/

	void program_control();
	void sclGenerator();
	void output_current();

	void i2c_mem();
	void i2c_nfc();
	void i2c_sec();

	SC_CTOR(microcontroller)
	{
		// SRAM Initialization
		ucRAM = new ucontroller::MicrocontrollerMemory();

		// Set starting energy state
		enState = pss::sleep_uc;
		deepSleepIter = 0;
		deepSleepStandyBy = 0;
		sleepStandyBy = 0;

		// Clock controll initialization
		ticks_output = 0;
		edge_output = false;

		// FRAM initialization
		opStatesMem = start;
		sleepState = false;

		writeToFRAM = true;
		readFromFRAM = false;

		writeOp = false;
		readOp = false;
		sleepOp = false;
		memAddr = 1;
		readValSize = 0;

		// NFC initialization
		opStatesNFC = start;

		writeToNFC = false;
		readFromNFC = false;

		randomReadNFC = false;
		systemModeNFC = false;

		writeOpNFC = false;
		readOpNFC = false;
		readValSizeNFC = 0;
		memAddrNFC = 0;

		// Security initialization
		opStatesSec = start;
		writeOpSec = false;
		readOpSec = false;
		randomReadSec = false;
		systemModeSec = false;
		memAddrSec = 1;
		readValSizeSec = 0;

		// UC Related
		basic_nfc_memory_state = 0;
		inner_0_basic_nfc_memory_state = 0;

		adv_nfc_sec_memory_state = 0;
		inner_0_adv_nfc_sec_memory_state = 0;

		SC_THREAD(program_control);
		sensitive << clk.pos();

		SC_THREAD(sclGenerator);
		sensitive << clk.pos();

		SC_THREAD(output_current);
		sensitive << clk.pos();

		SC_THREAD(i2c_mem);
		sensitive << scl_mem.pos();

		SC_THREAD(i2c_nfc);
		sensitive << scl_nfc.neg();

		SC_THREAD(i2c_sec);
		sensitive << scl_sec.pos();
	};

private:

	// General UC Programs
	int basic_nfc_memory_state;
	short inner_0_basic_nfc_memory_state;
	int adv_nfc_sec_memory_state;
	short inner_0_adv_nfc_sec_memory_state;
	void basic_nfc_memory(int &loopControl, int &loopLimit);
	void adv_nfc_sec_memory(int &loopControl, int &loopLimit);

	// Energy state
	EnergyStates_MicroController enState;
	short deepSleepIter;
	short deepSleepStandyBy;
	short sleepStandyBy;

	// Inner Microcontroller RAM
	ucontroller::MicrocontrollerMemory *ucRAM;

	// For the output clock control
	unsigned int ticks_output;
	bool edge_output;

	// For the FRAM communication
	OperativeStates opStatesMem;
	bool sleepState;

	bool writeToFRAM;
	bool readFromFRAM;

	bool writeOp;
	bool readOp;
	bool sleepOp;
	size_t readValSize;
	int memAddr;
	std::queue<byte> memValues;

	void writeInMemory(byte &memBuffer, int &iter);
	void readFromMemory(byte &memBuffer, int &iter);
	void setMemToSleep(byte &memBuffer, int &iter);

	// For the NFC communication
	OperativeStates opStatesNFC;

	bool writeToNFC;
	bool readFromNFC;

	bool randomReadNFC;
	bool systemModeNFC;

	bool writeOpNFC;
	bool readOpNFC;
	size_t readValSizeNFC;
	int memAddrNFC;
	std::queue<byte> memValuesNFC;

	void writeInMemoryNFC(byte &memBuffer, int &iter);
	void readFromMemoryNFC(byte &memBuffer, int &iter);

	// For the Security communication
	OperativeStates opStatesSec;

	bool writeOpSec;
	bool readOpSec;
	bool randomReadSec;
	bool systemModeSec;
	size_t readValSizeSec;
	int memAddrSec;
	std::queue<byte> memValuesSec;
	std::vector<byte> inValuesSec;
	std::vector<byte> outValuesSec;


	void executeSecurityFunction(std::vector<byte> inValues,
			std::vector<byte> &outValues, uint8_t funcCode);
	void writeInMemorySec(byte &memBuffer, int &iter);
	void readFromMemorySec(byte &memBuffer, int &iter);
};

} /* namespace testb */

#endif /* MODULES_MICROCONTROLLER_MICROCONTROLLER_H_ */
