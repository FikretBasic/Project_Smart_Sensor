/*
 * SecurityFunction.cpp
 *
 *  Created on: 28 Jul 2018
 *      Author: its2016
 */

#include "SecurityFunction.h"

namespace sec {

SecurityFunction::SecurityFunction()
{
	currScheme = auth1;
	srand(time(NULL));
}

SecurityFunction::~SecurityFunction() {}

byte SecurityFunction::getRandomByte()
{
	return rand() % 255;
}

void SecurityFunction::getDataObject(SecurityMemory *memory,
		std::vector<byte> &dataObject, uint16_t memoryAddr, size_t length)
{
	memory->setCurrentUserAddress(memoryAddr, true);
	dataObject.clear();
	for(size_t i = 0; i < length; i++)
	{
		dataObject.push_back(memory->readFromMemory());
	}
}

void SecurityFunction::setDataObject(SecurityMemory *memory,
		std::vector<byte> dataObject, uint16_t memoryAddr)
{
	memory->setCurrentUserAddress(memoryAddr, false);
	for(size_t i = 0; i < dataObject.size(); i++)
	{
		memory->writeInMemory(dataObject.at(i));
		memory->incrementAddr();
	}
}

void SecurityFunction::getRandom(std::vector<byte> &randomStream)
{
	for(size_t i = 0; i < randomStream.size(); i++)
	{
		randomStream.at(i) = this->getRandomByte();
	}
}

void SecurityFunction::setAuthScheme(authScheme schemeToSet)
{
	this->currScheme = schemeToSet;
}

void SecurityFunction::getAuthMsg(std::vector<byte> &authMsg, size_t pos)
{
	authMsg = this->authMsgs.at(pos);
}

void SecurityFunction::setAuthMsg(std::vector<byte> authMsg, size_t &pos)
{
	this->authMsgs.push_back(authMsg);
	pos = this->authMsgs.size() - 1;
}

void SecurityFunction::propUpLinkMsg(std::vector<byte> &upLinkMsg)
{
	// Processing the upLink Message...
	for(size_t i = 0; i < upLinkMsg.size(); i++)
	{
		upLinkMsg.at(i) = this->getRandomByte();
	}
}

void SecurityFunction::propDownLinkMsg(std::vector<byte> &downLinkMsg)
{
	// Processing the downLink Message...
	for(size_t i = 0; i < downLinkMsg.size(); i++)
	{
		downLinkMsg.at(i) = this->getRandomByte();
	}
}

void SecurityFunction::calcHash(std::vector<byte> msg, std::vector<byte> &hashMsg)
{
	hashMsg.clear();
	for(size_t i = 0; i < msg.size(); i++)
	{
		hashMsg.push_back(this->getRandomByte());
	}
}

void SecurityFunction::calcSign(std::vector<byte> msg, std::vector<byte> &signMsg)
{
	signMsg.clear();
	for(size_t i = 0; i < msg.size(); i++)
	{
		signMsg.push_back(this->getRandomByte());
	}
}

void SecurityFunction::verifySign(std::vector<byte> signMsg, bool &status)
{
	status = true;
}

void SecurityFunction::calcSSec()
{
	// Command to execute a Diffie-Hellmann key agreement
}

void SecurityFunction::deriveKey(std::vector<byte> &key, size_t size)
{
	for(size_t i = 0; i < size; i++)
	{
		key.push_back(this->getRandomByte());
	}
}

void SecurityFunction::genKeyPair(std::vector<byte> &publicKey, std::vector<byte> &privateKey, size_t size)
{
	this->deriveKey(publicKey, size);
	this->deriveKey(privateKey, size);
}

void SecurityFunction::setWorkValues(std::vector<byte> workValues)
{
	this->workValues.clear();

	for(size_t i = 0; i < workValues.size(); i++)
		this->workValues.push_back(workValues.at(i));
}

std::vector<byte> SecurityFunction::getWorkValues()
{
	return this->workValues;
}

void SecurityFunction::openApplication(uint8_t appCode)
{
	// Command to launch an application. The commands were replicated from the documentation.
	size_t authPos;

	switch(appCode)
	{
		// Mutual Authentication using DTLS
		case 0x01:
			this->setAuthScheme(auth1);
			this->propUpLinkMsg(this->workValues);
			this->propDownLinkMsg(this->workValues);
			break;

		// One Way Authentication
		case 0x02:
			this->getRandom(this->workValues);
			this->setAuthScheme(auth2);
			authPos = 0;
			this->setAuthMsg(this->workValues, authPos);
			this->getAuthMsg(this->workValues, authPos);
			break;

		// Crypto Toolbox
		case 0x03:

			this->getRandom(this->workValues);
			this->setAuthScheme(auth3);
			authPos = 0;
			this->setAuthMsg(this->workValues, authPos);
			this->getAuthMsg(this->workValues, authPos);
			this->calcHash(this->workValues, this->workValues);
			this->calcSign(this->workValues, this->workValues);

			bool status;
			this->verifySign(this->workValues, status);
			if(status)
			{
				this->calcSSec();
				std::vector<byte> publicKey;
				std::vector<byte> privateKey;
				this->genKeyPair(publicKey, privateKey, 32);
			}

			break;
	}
}

} /* namespace sec */
