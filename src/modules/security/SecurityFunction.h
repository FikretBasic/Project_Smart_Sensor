/*
 * SecurityFunction.h
 *
 *  Created on: 28 Jul 2018
 *      Author: its2016
 */

#ifndef MODULES_SECURITY_SECURITYFUNCTION_H_
#define MODULES_SECURITY_SECURITYFUNCTION_H_

#include "../Includes.h"
#include "SecurityMemory.h"

namespace sec {

class SecurityFunction {
public:
	SecurityFunction();
	virtual ~SecurityFunction();

	void setWorkValues(std::vector<byte> workValues);
	std::vector<byte> getWorkValues();
	void openApplication(uint8_t appCode);

private:
	authScheme currScheme;

	// Authentification messages
	std::vector<std::vector<byte > > authMsgs;
	std::vector<byte> workValues;

	byte getRandomByte();

	void getDataObject(SecurityMemory *memory,
			std::vector<byte> &dataObject, uint16_t memoryAddr, size_t length);
	void setDataObject(SecurityMemory *memory,
			std::vector<byte> dataObject, uint16_t memoryAddr);
	void getRandom(std::vector<byte> &randomStream);
	void setAuthScheme(authScheme schemeToSet);
	void getAuthMsg(std::vector<byte> &authMsg, size_t pos);
	void setAuthMsg(std::vector<byte> authMsg, size_t &pos);
	void propUpLinkMsg(std::vector<byte> &upLinkMsg);
	void propDownLinkMsg(std::vector<byte> &downLinkMsg);
	void calcHash(std::vector<byte> msg, std::vector<byte> &hashMsg);
	void calcSign(std::vector<byte> msg, std::vector<byte> &signMsg);
	void verifySign(std::vector<byte> signMsg, bool &status);
	void calcSSec();
	void deriveKey(std::vector<byte> &key, size_t size);
	void genKeyPair(std::vector<byte> &publicKey, std::vector<byte> &privateKey, size_t size);
};

} /* namespace sec */

#endif /* MODULES_SECURITY_SECURITYFUNCTION_H_ */
