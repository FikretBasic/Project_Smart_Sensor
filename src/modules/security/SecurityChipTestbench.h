/*
 * SecurityChipTestbench.h
 *
 *  Created on: 4 Aug 2018
 *      Author: its2016
 */

#ifndef MODULES_SECURITY_SECURITYCHIPTESTBENCH_H_
#define MODULES_SECURITY_SECURITYCHIPTESTBENCH_H_

#include "../Includes.h"

namespace testb {

SC_MODULE(security_testbench)
{
public:

	sc_in<bool> clk;
	sc_in<bool> sda_in;

	sc_out<bool> scl;
	sc_out<bool> sda_out;
	sc_out<bool> enable;

	bool testVarWrite1;
	bool testVarWrite2;
	bool testVarWriteRead1;
	bool testVarWriteRead2;
	bool testVarSystem1;
	bool testVarSystem2;
	bool testVarSecurity1;
	bool testVarSecurity2;
	bool testVarSecurity3;

	void sclGenerate();
	void memoryControl();
	void runTest();

	SC_CTOR(security_testbench)
	{
		srand((unsigned)time(NULL));

		testVarWrite1 = false;
		testVarWrite2 = false;
		testVarWriteRead1 = false;
		testVarWriteRead2 = false;
		testVarSystem1 = false;
		testVarSystem2 = false;
		testVarSecurity1 = false;
		testVarSecurity2 = false;
		testVarSecurity3 = false;

		opStates = start;
		writeOp = false;
		readOp = false;
		randomRead = false;
		systemMode = false;
		memAddr = 1;
		readValSize = 0;

		SC_THREAD(sclGenerate);
		sensitive << clk;

		SC_THREAD(memoryControl);
		sensitive << clk.pos();

		SC_THREAD(runTest);
		sensitive << clk.pos();
	};

private:
	OperativeStates opStates;

	bool writeOp;
	bool readOp;
	bool randomRead;
	bool systemMode;
	size_t readValSize;
	int memAddr;

	std::queue<byte> memValues;

	void executeSecurityFunction(std::vector<byte> inValues,
			std::vector<byte> &outValues, uint8_t funcCode);
	void writeInMemory(byte &memBuffer, int &iter);
	void readFromMemory(byte &memBuffer, int &iter);
};

} /* namespace testb */

#endif /* MODULES_SECURITY_SECURITYCHIPTESTBENCH_H_ */
