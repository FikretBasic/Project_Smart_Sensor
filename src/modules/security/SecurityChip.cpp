/*
 * SecurityChip.cpp
 *
 *  Created on: 14 Jul 2018
 *      Author: its2016
 */

#include "SecurityChip.h"

namespace pss {

void security::i2c()
{
	int iter = 7;
	bool ack = true;
	byte cur_val = 0;
	bool sleep_op = false;

	sda_out.write(1);

	while(true)
	{
		wait();

		if(operate.read())
		{
			switch(opState)
			{
				case start:
					if (enable.read())
					{
						opState = dev_addr;
						enState = idle_sec;
						mem_buffer = 0;
					}
					break;

				case dev_addr:
					cur_val = sda_in.read();
					mem_buffer |= (cur_val << iter);

					if(iter == 0)
					{
						cur_val = checkDeviceAddressValue();

						if (cur_val == 1)
						{
							read_or_write = mem_buffer & 1;
							sda_out.write(0);

							if (sleep_op)
							{
								opState = stop;
								iter = 7;
							}
							else
							{
								enState = active_sec;
								if(read_or_write)
								{
									opState = rd_wr;
									iter = 7;
								}
								else
								{
									opState = mem_addr;
									iter = 15;
								}
							}
							sleep_op = false;
						}
						else if (cur_val == 2)
						{
							sda_out.write(0);

							sleep_op = true;
							read_or_write = false;
							opState = rd_wr;
							mem_buffer = 0;
							ack = true;
							iter = 7;
						}
						else
						{
							opState = start;
							enState = idle_sec;
						}
					}
					else
					{
						iter--;
					}

					break;

				case mem_addr:
					if (sda_in.read() == 1 && ack)
					{
						ack = false;
						sda_out.write(1);
						addressBuffer = 0;
					}
					else
					{
						int cur_val = sda_in.read();
						addressBuffer |= (cur_val << iter);

						if (iter == 0)
						{
							if(E2 == 0)
							{
								memory->setCurrentUserAddress(addressBuffer, read_or_write);
							}
							else
							{
								memory->setCurrentSystemAddress((uint8_t)addressBuffer);
							}

							opState = rd_wr;
							mem_buffer = 0;
							sda_out.write(0);
							ack = true;
							iter = 7;
						}
						else
						{
							iter--;
						}
					}

					break;

				case rd_wr:
					if(enable.read())
					{
						if (sda_in.read() == 1 && ack && !read_or_write)
						{
							ack = false;
							sda_out.write(1);
						}
						else
						{
							if(read_or_write)
							{
								if(E2 == 0)
								{
									sda_out.write((memory->readFromMemory() >> iter) & 1);
								}
								else
								{
									sda_out.write((memory->readFromSystem() >> iter) & 1);
								}
							}
							else
							{
								byte cur_val = sda_in.read();
								mem_buffer |= (cur_val << iter);
							}

							if(iter == 0)
							{
								if (!read_or_write)
								{
									if(E2 == 0)
									{
										memory->setCurrentUserAddress(addressBuffer, read_or_write);
										memory->writeInMemory(mem_buffer);
									}
									else
										memory->writeInSystem(mem_buffer);

									cout << "\nSec Chip. Mem addr value written: " << (int)addressBuffer << endl;
									cout << "Sec Chip. Mem value written: " << (int)mem_buffer << endl;

									mem_buffer = 0;
								}
								else
								{
									if(E2 == 0)
										memory->incrementAddr();

									// for synchronisation
									wait();
								}

								addressBuffer++;

								iter = 7;
								ack = true;
							}
							else
							{
								iter--;
							}
						}
					}
					else
					{
						opState = stop;
					}

					break;

				case stop:
					opState = start;

					if (!read_or_write)
					{
						if(E2 == 1)
							handleSecureFunction();
					}
					else
					{
						// if the reading process is finished, reset the iterator for the next one
						memory->resetWorkMemoryIterator();
					}

					mem_buffer = 0;
					read_or_write = 0;
					enState = idle_sec;

					iter = 7;
					ack = true;
					cur_val = 0;

					break;

				default:
					opState = start;
					enState = idle_sec;
					read_or_write = 0;
			}
		}
	}
}

void security::handleSecureFunction()
{
	if(this->memory->getCurrentSystemAddress() == 12)
	{
		if(this->memory->readFromSystem() == 0x01)
		{
			this->functions->setWorkValues(this->memory->getSystemWorkMemory());
			switch(this->memory->getSystemRegisterVal(10))
			{
				// The three main use cases of the security chip
				case 0x01:
					wait(30, SC_MS);
					this->functions->openApplication(0x01);
					break;

				case 0x02:
					wait(40, SC_MS);
					this->functions->openApplication(0x02);
					break;

				case 0x03:
					wait(80, SC_MS);
					this->functions->openApplication(0x03);
					break;
			}
			this->memory->setSystemWorkMemory(this->functions->getWorkValues());
			this->memory->writeInSystem(0x00);
		}
	}
}

byte security::checkDeviceAddressValue()
{
	byte deviceNum = (mem_buffer & 240) >> 4;
	E2 = (mem_buffer & 8) >> 3;
	byte b2Check = (mem_buffer & 4) >> 2;
	byte b1Check = (mem_buffer & 2) >> 1;

	if (deviceNum == 10 && b2Check == 1 && b1Check == 1)
	{
		return 1;
	}
	else if (mem_buffer == 0xf8)
	{
		return 2;
	}
	else
	{
		return 0;
	}
}

void security::setSleepState()
{
	while(true)
	{
		wait();

		if(enState == idle_sec)
		{
			if(++idleCounter >= 50)
			{
				enState = sleep_sec;
			}
		}
		else
		{
			idleCounter = 0;
		}
	}
}

void security::outputCurrent()
{
	while(true)
	{
		wait();

		currentOut.write(Configuration::instance()->getSecPowerConsumption(enState));
	}
}

} /* namespace pss */
