/*
 * SecurityMemory.cpp
 *
 *  Created on: 28 Jul 2018
 *      Author: its2016
 */

#include "SecurityMemory.h"

namespace sec {

SecurityMemory::SecurityMemory() {
	// Initialization

	currUserAddr = 0x01;
	currSystemAddr = 0x00;

	// setting up the elements related to the starting system memory
	// RW - read & write, W - write, R - read

	// data to be written, length up to that written in register 0x01, RW
	systemMemory[0x00] = 0x00;
	// length of the data, can go up to 0xffff, RW
	systemMemory[0x01] = 0xffff;
	// I2C state, bits 15:0 represent the response length, 31:24 various states, R
	systemMemory[0x02] = systemMemory[0x01];
	// base address, W
	systemMemory[0x03] = 0x0020;
	// maximum scl frequency, either 400kHz or 1MHz, R
	systemMemory[0x04] = 1000;
	// guard time, minimal waiting time, in microseconds, R
	systemMemory[0x05] = 50;
	// trans timeout, how long to wait if a response is not received, in miliseconds, R
	systemMemory[0x06] = 20;
	// soft reset, W
	systemMemory[0x08] = 0x00;
	// current I2C mode, default is Sm & Fm, RW
	systemMemory[0x09] = 0x03;
	// holds the code which indicates a specific security function
	systemMemory[10] = 0x00;
	// if 0x00 write to user memory, else if 0x01 write to system work memory
	systemMemory[11] = 0x00;
	// holds the codes on if the security process should start, 0x01 is to indicate start
	systemMemory[12] = 0x00;
	// for error reporting, each code is a specific error
	// currently, only if the system work memory exceeds the limit, a value of 0x01 will be written
	systemMemory[13] = 0x00;

	// initiating user memory for testing purposes
	userMemory[124] = 5;
	userMemory[125] = 135;

	userMemory[192] = 10;
	userMemory[193] = 20;
	userMemory[194] = 30;
	userMemory[195] = 45;
	userMemory[196] = 50;

	workMemoryIterator = 0;
}

SecurityMemory::~SecurityMemory() {}

bool SecurityMemory::writeInMemory(byte data)
{
	// Check if it is writing in the user memory or the work memory
	if(systemMemory[11] == 0x01)
	{
		systemWorkMemory.push_back(data);
		// if the memory is exceeded, write to indicate an error
		if(systemWorkMemory.size() > systemMemory[0x01])
			systemMemory[13] = 0x01;
		else
			systemMemory[13] = 0x00;
	}
	else
	{
		userMemory[currUserAddr] = data;
	}

	return true;
}

bool SecurityMemory::writeInSystem(uint32_t data)
{
	if(currSystemAddr == 0x00 || currSystemAddr == 0x01 || currSystemAddr == 0x03
			|| currSystemAddr == 0x08 || currSystemAddr == 0x09 || currSystemAddr == 10
			|| currSystemAddr == 11 || currSystemAddr == 12 || currSystemAddr == 13)
			systemMemory[currSystemAddr] = data;
	else
		return false;

	return true;
}

byte SecurityMemory::readFromMemory()
{
	if(systemMemory[11] == 0x01)
	{
		if(systemWorkMemory.size() > 0 && systemWorkMemory.size() > workMemoryIterator)
		{
			return systemWorkMemory.at(workMemoryIterator);
		}
		else
		{
			return 0xff;
		}
	}
	else
	{
		return userMemory[currUserAddr];
	}
}

uint32_t SecurityMemory::readFromSystem()
{
	//cout << "System addr: " << (int)currSystemAddr << endl;
	if(currSystemAddr == 0x00 || currSystemAddr == 0x01 || currSystemAddr == 0x02
			|| currSystemAddr == 0x04 || currSystemAddr == 0x05 || currSystemAddr == 0x06
			|| currSystemAddr == 0x09 || currSystemAddr == 10
			|| currSystemAddr == 11 || currSystemAddr == 12 || currSystemAddr == 13)
		return systemMemory[currSystemAddr];
	else
		return 0xffffffff;
}

void SecurityMemory::incrementAddr()
{
	if(systemMemory[11] == 0x01)
		workMemoryIterator++;
	else
		currUserAddr++;
}

void SecurityMemory::setCurrentUserAddress(uint16_t currentUserAddress, bool readWrite)
{
	if(systemMemory[11] == 0x11 && !readWrite)
	{
		this->systemWorkMemory.clear();
	}
	else
	{
		this->currUserAddr = currentUserAddress;
	}
}

void SecurityMemory::setCurrentSystemAddress(uint8_t currentSysAddr)
{
	if(currentSysAddr >= 14)
		this->currSystemAddr = 0xff;
	else
		this->currSystemAddr = currentSysAddr;
}

uint16_t SecurityMemory::getCurrentUserAddress()
{
	return this->currUserAddr;
}

uint8_t SecurityMemory::getCurrentSystemAddress()
{
	return this->currSystemAddr;
}

void SecurityMemory::writeOutUserMemory()
{
	cout << "\nUSER MEMORY SECURITY" << endl << endl;
	for (uint16_t i = 0; i < 10240; i++)
	{
		cout << "[" << i << "]" << " " << (int)userMemory[i] << endl;
	}
}

void SecurityMemory::setSystemWorkMemory(std::vector<byte> systemWorkMemory)
{
	this->systemWorkMemory.clear();

	for(size_t i = 0; i < systemWorkMemory.size(); i++)
		this->systemWorkMemory.push_back(systemWorkMemory.at(i));
}

uint32_t SecurityMemory::getSystemRegisterVal(uint8_t reg)
{
	if(reg < 14)
		return this->systemMemory[reg];
	else
		return 0xffff;
}

std::vector<byte> SecurityMemory::getSystemWorkMemory()
{
	return this->systemWorkMemory;
}

void SecurityMemory::resetWorkMemoryIterator()
{
	this->workMemoryIterator = 0;
}

} /* namespace sec */
