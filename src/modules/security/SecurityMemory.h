/*
 * SecurityMemory.h
 *
 *  Created on: 28 Jul 2018
 *      Author: its2016
 */

#ifndef MODULES_SECURITY_SECURITYMEMORY_H_
#define MODULES_SECURITY_SECURITYMEMORY_H_

#include "../Includes.h"

namespace sec {

class SecurityMemory {
public:
	SecurityMemory();
	virtual ~SecurityMemory();

	bool writeInMemory(byte data);
	bool writeInSystem(uint32_t data);
	byte readFromMemory();
	uint32_t readFromSystem();

	void incrementAddr();

	void setCurrentUserAddress(uint16_t currentUserAddress, bool readWrite);
	void setCurrentSystemAddress(uint8_t currentSysAddr);
	uint16_t getCurrentUserAddress();
	uint8_t getCurrentSystemAddress();
	void writeOutUserMemory();

	uint32_t getSystemRegisterVal(uint8_t reg);
	void setSystemWorkMemory(std::vector<byte> systemWorkMemory);
	std::vector<byte> getSystemWorkMemory();
	void resetWorkMemoryIterator();

private:
	byte userMemory[10240];			// 10kB User Memory
	uint16_t currUserAddr;

	// Taken only space for the known commands
	uint32_t systemMemory[14];
	uint8_t currSystemAddr;

	// This memory is the memory represented as the systemMemory[0], since
	// the value that this memory holds can go up to 0xffff, making it sensible to store in a vector
	std::vector<byte> systemWorkMemory;
	uint16_t workMemoryIterator;
};

} /* namespace sec */

#endif /* MODULES_SECURITY_SECURITYMEMORY_H_ */
