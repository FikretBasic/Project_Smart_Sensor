/*
 * SecurityChip.h
 *
 *  Created on: 14 Jul 2018
 *      Author: its2016
 */

#ifndef MODULES_SECURITY_SECURITYCHIP_H_
#define MODULES_SECURITY_SECURITYCHIP_H_

#include "../Includes.h"
#include "SecurityMemory.h"
#include "SecurityFunction.h"

// Implementation of the Infineon Optiga TrustX  Security Chip

namespace pss {

SC_MODULE(security)
{
public:

	/****************
	 * 	Ports and Pins
	 ****************/
	sc_in<bool> scl;				// input scl signal
	sc_in<bool> enable;				// signal to start the I2C process
	sc_in<bool> sda_in;			// input signal from I2C
	sc_out<bool> sda_out;			// output signal for I2C
	sc_out<double> currentOut;		// current consumption based on the operation
	sc_in<bool> operate;			// should the device operate, i.e. does it have enough charge

	/****************
	 * 	Threads
	 ****************/

	void i2c();
	void setSleepState();
	void outputCurrent();

	SC_CTOR(security)
	{
		memory = new sec::SecurityMemory();
		functions = new sec::SecurityFunction();

		opState = start;
		enState = idle_sec;

		idleCounter = 0;

		mem_buffer = 0;
		E2 = 0;
		read_or_write = 0;
		addressBuffer = 0;

		SC_THREAD(i2c);
		sensitive << scl.pos();

		SC_THREAD(setSleepState);
		sensitive << scl.pos();

		SC_THREAD(outputCurrent);
		sensitive << scl.pos();
	};

private:
	// both the user and the system (registers) memory
	sec::SecurityMemory *memory;

	// functionality class for the module
	sec::SecurityFunction *functions;

	// during the I2C, to check the validity of the device code
	byte checkDeviceAddressValue();

	// provide the security functionality
	void handleSecureFunction();

	// defining the operational states of the I2C protocol
	OperativeStates opState;
	// defining the energy state
	EnergyStates_Security enState;

	// for checking when to turn to sleep state
	int idleCounter;

	// necessary variables for the I2C
	byte mem_buffer;
	byte E2;
	byte read_or_write;
	int addressBuffer;
};

} /* namespace pss */

#endif /* MODULES_SECURITY_SECURITYCHIP_H_ */
