/*
 * SecurityChipTestbench.cpp
 *
 *  Created on: 4 Aug 2018
 *      Author: its2016
 */

#include "SecurityChipTestbench.h"

namespace testb {

void security_testbench::sclGenerate()
{
	while(true)
	{
		wait();

		scl.write(clk);
	}
}

void security_testbench::runTest()
{
	while(true)
	{
		wait();

		if (testVarWrite1)
		{
			memValues.push(30);
			memAddr = 150;
			writeOp = true;

			testVarWrite1 = false;
		}
		else if (testVarWrite2)
		{
			memValues.push(31);
			memValues.push(33);
			memValues.push(35);
			memValues.push(245);
			memAddr = 140;
			writeOp = true;

			testVarWrite2 = false;
		}
		else if (testVarWriteRead1)
		{
			randomRead = true;
			memAddr = 192;
			readValSize = 5;
			readOp = true;

			testVarWriteRead1 = false;
		}
		else if (testVarWriteRead2)
		{
			memValues.push(30);
			memValues.push(41);
			memAddr = 80;
			writeOp = true;

			wait(70, SC_US);

			memValues.push(123);
			memAddr = 124;
			writeOp = true;

			wait(50, SC_US);

			readOp = true;
			randomRead = false;
			readValSize = 1;

			wait(50, SC_US);

			memValues.push(48);
			memValues.push(53);
			memAddr = 82;
			writeOp = true;

			wait(70, SC_US);

			readOp = true;
			randomRead = true;
			memAddr = 81;
			readValSize = 3;

			testVarWriteRead2 = false;
		}
		else if (testVarSystem1)
		{
			// Write changes to the data in the system memory placed on the position 0x01

			memValues.push(25);
			memAddr = 0x01;
			writeOp = true;
			systemMode = true;

			wait(70, SC_US);

			readOp = true;
			randomRead = true;
			memAddr = 0x01;
			readValSize = 1;

			testVarSystem1 = false;
		}
		else if (testVarSystem2)
		{
			// Tries to access memory address not existing, operation fails, returns 255 as default

			memValues.push(25);
			memAddr = 0x20;
			writeOp = true;
			systemMode = true;

			wait(70, SC_US);

			readOp = true;
			randomRead = true;
			memAddr = 0x20;
			readValSize = 1;

			testVarSystem2 = false;
		}
		else if (testVarSecurity1)
		{
			// Sends an array of three values to perform the sec. func. with 0x01 code (Mutual Authentication using DTLS)
			// On return it gets an array of result values

			std::vector<byte> inValues;
			inValues.push_back(0);
			inValues.push_back(2);
			inValues.push_back(4);
			std::vector<byte> outValues;

			executeSecurityFunction(inValues, outValues, 0x01);

			if (!outValues.empty())
			{
				cout << "[Testbench] Received results from the sec. process: ";
				for(size_t i = 0; i < outValues.size(); i++)
				{
					cout << (int)outValues.at(i) << " ";
				}
				cout << endl;
			}

			testVarSecurity1 = false;
		}
		else if (testVarSecurity2)
		{
			// Sends an array of three values to perform the sec. func. first with 0x02 (One Way Authentication) ...
			// ... and then with 0x03 (Crypto Toolbox)
			// On return it gets an array of result values

			std::vector<byte> inValues;
			inValues.push_back(25);
			inValues.push_back(30);
			inValues.push_back(35);
			std::vector<byte> outValues;

			//executeSecurityFunction(inValues, outValues, 0x02);
			executeSecurityFunction(inValues, outValues, 0x03);

			if (!outValues.empty())
			{
				cout << "[Testbench] Received results from the security process: ";
				for(size_t i = 0; i < outValues.size(); i++)
				{
					cout << (int)outValues.at(i) << " ";
				}
				cout << endl;
			}

			testVarSecurity2 = false;
		}
		else if (testVarSecurity3)
		{
			// Sends an array of three values to perform the sec. func. but with a non-existing code
			// On return it gets the same array that is sent

			std::vector<byte> inValues;
			inValues.push_back(25);
			inValues.push_back(30);
			inValues.push_back(35);
			std::vector<byte> outValues;

			executeSecurityFunction(inValues, outValues, 0x04);

			if (!outValues.empty())
			{
				cout << "[Testbench] Received results from the sec. process: ";
				for(size_t i = 0; i < outValues.size(); i++)
				{
					cout << (int)outValues.at(i) << " ";
				}
				cout << endl;
			}

			testVarSecurity3 = false;
		}
	}
}

void security_testbench::executeSecurityFunction(std::vector<byte> inValues,
		std::vector<byte> &outValues, uint8_t funcCode)
{
	int testCheckIter = 3;
	bool check = false;

	while(testCheckIter > 0)
	{
		// Write to system address 11 to start the process of writing to the work memory (1)
		memValues.push(0x01);
		memAddr = 11;
		writeOp = true;
		systemMode = true;
		wait(70, SC_US);

		// Start sending data to write to the work memory	(2)
		for(size_t i = 0; i < inValues.size(); i++)
		{
			memValues.push(inValues.at(i));
		}
		memAddr = 0;
		systemMode = false;
		writeOp = true;
		wait(100, SC_US);

		// Check if everything is correctly written	(3)
		readOp = true;
		randomRead = true;
		memAddr = 13;
		readValSize = 1;
		systemMode = true;
		wait(50, SC_US);

		if(memValues.front() == 0x00)
		{
			cout << "[Testbench] Successfully first phase written!" << endl;
			memValues.pop();
			check = true;
			break;
		}
		testCheckIter--;
		memValues.pop();
	}

	if(check)
	{
		wait(100, SC_US);
		// Write to system address 11 to stop the process of writing to the work memory (4)
		memValues.push(0x00);
		memAddr = 11;
		writeOp = true;
		systemMode = true;
		wait(70, SC_US);

		// Write the code for a specific security operation (5)
		memValues.push(funcCode);
		memAddr = 10;
		writeOp = true;
		systemMode = true;
		wait(70, SC_US);

		// Write 0x01 to the system position 12 to indicate the start of the security function (6)
		memValues.push(0x01);
		memAddr = 12;
		writeOp = true;
		systemMode = true;
		wait(70, SC_US);
		/* if(funcCode == 0x01 || funcCode == 0x02)
			wait(50, SC_MS);
		else if (funcCode == 0x03)
			wait(90, SC_MS);
		else
			wait(20, SC_US); */

		check = false;
		testCheckIter = 10;

		while(testCheckIter > 0)
		{
			// Check if the value of the register 12 now 0 is (7)
			readOp = true;
			randomRead = true;
			memAddr = 12;
			readValSize = 1;
			systemMode = true;
			wait(50, SC_US);

			if(memValues.front() == 0x00)
			{
				cout << "[Testbench] Process finished!" << endl;
				memValues.pop();
				check = true;
				break;
			}
			{
				cout << "[Testbench] Still waiting!" << endl;
				memValues.pop();
			}
			testCheckIter--;
			wait(20, SC_MS);
		}
	}
	else
	{
		return;
	}

	if (check)
	{
		// Check if everything is correctly written	(3)
		readOp = true;
		randomRead = true;
		memAddr = 13;
		readValSize = 1;
		systemMode = true;
		wait(50, SC_US);

		check = false;

		if(memValues.front() == 0x00)
		{
			check = true;
		}
		memValues.pop();
	}

	if (check)
	{
		wait(100, SC_US);
		// Write to system address 11 to start the process of writing to the work memory (8)
		memValues.push(0x01);
		memAddr = 11;
		writeOp = true;
		systemMode = true;
		wait(70, SC_US);

		// Read the data from the security process (9)
		readOp = true;
		randomRead = true;
		memAddr = 0x00;
		readValSize = inValues.size();
		systemMode = false;
		wait(70*inValues.size(), SC_US);

		size_t numOfElements = memValues.size();
		for(size_t i = 0; i < numOfElements; i++)
		{
			outValues.push_back(memValues.front());
			memValues.pop();
		}

		// Write to system address 11 to stop the process of writing to the work memory (10)
		memValues.push(0x00);
		memAddr = 11;
		writeOp = true;
		systemMode = true;
		wait(70, SC_US);
	}

}

void security_testbench::memoryControl()
{
	byte memBuffer = 0;
	int iter = 7;

	while(true)
	{
		wait();

		if(writeOp)
		{
			// write op
			if(memValues.size() > 0)
			{
				writeInMemory(memBuffer, iter);
			}
		}
		else if(readOp)
		{
			// read op
			readFromMemory(memBuffer, iter);
		}
	}
}

void security_testbench::writeInMemory(byte &memBuffer, int &iter)
{
	switch(opStates)
	{
		case start:
			//wait(10, SC_US);
			enable.write(1);
			opStates = dev_addr;

			if(systemMode)
			{
				memBuffer = 0xae;
			}
			else
			{
				// 10100110 - device address and instruction
				memBuffer = 0xa6;
			}

			break;

		case dev_addr:

			if(iter < 0)
			{
				sda_out.write(1);

				opStates = mem_addr;
				memBuffer = 0;
				iter = 15;
			}
			else
			{
				sda_out.write((memBuffer >> iter) & 1);
				iter--;
			}

			break;

		case mem_addr:

			if(iter < 0)
			{
				sda_out.write(1);
				opStates = rd_wr;
				memBuffer = memValues.front();
				iter = 7;
			}
			else
			{
				/*if(iter == 16)
					memAddr = 100;*/

				sda_out.write((memAddr >> iter) & 1);
				iter--;
			}

			break;

		case rd_wr:

			if(iter < 0)
			{
				sda_out.write(1);

				if (memValues.size() == 1)
				{
					opStates = stop;
					memBuffer = 0;
					//memAddr = 0;
					iter = 7;
				}
				else
				{
					//writeIter--;
					memValues.pop();
					iter = 7;
					memBuffer = memValues.front();
				}
			}
			else
			{
				sda_out.write((memBuffer >> iter) & 1);
				iter--;
			}

			break;

		case stop:
			memValues.pop();
			enable.write(0);
			opStates = start;
			writeOp = false;

			break;

		default:
			opStates = start;

	}
}

void security_testbench::readFromMemory(byte &memBuffer, int &iter)
{
	switch(opStates)
	{
		case start:
			wait();
			//wait();
			enable.write(1);
			opStates = dev_addr;

			if (randomRead)
			{
				if(systemMode)
				{
					memBuffer = 0xae;
				}
				else
				{
					// 10100110 - device address and instruction
					memBuffer = 0xa6;
				}
			}
			else
			{
				if(systemMode)
				{
					memBuffer = 0xaf;
				}
				else
				{
					// 10100110 - device address and instruction
					memBuffer = 0xa7;
				}
			}

			break;

		case dev_addr:

			if(iter < 0)
			{
				sda_out.write(1);

				memBuffer = 0;

				if(randomRead)
				{
					opStates = mem_addr;
					iter = 15;
				}
				else
				{
					opStates = rd_wr;
					iter = 7;

					// wait one cycle to synchronise
					//wait();
				}
			}
			else
			{
				sda_out.write((memBuffer >> iter) & 1);
				iter--;
			}

			break;

		case mem_addr:

			if(iter < 0)
			{
				sda_out.write(1);

				randomRead = false;
				enable.write(0);
				opStates = start;

				memBuffer = 0;
				iter = 7;

				// wait one cycle to synchronise
				wait();
			}
			else
			{
				//if(iter == 16)
					//memAddr = 100;

				sda_out.write((memAddr >> iter) & 1);
				iter--;
			}

			break;

		case rd_wr:

			if(iter < 0)
			{
				sda_out.write(1);
				cout << "\nMemory buffer: " << (int)memBuffer << " [" << memAddr << "]" << endl;
				memValues.push(memBuffer);

				if (memValues.size() == readValSize)
				{
					opStates = stop;
				}
				else
				{
					memAddr++;
				}

				memBuffer = 0;
				iter = 7;
			}
			else
			{
				byte cur_val = sda_in.read();
				memBuffer |= (cur_val << iter);
				iter--;
			}

			break;

		case stop:
			enable.write(0);
			opStates = start;

			readOp = false;

			break;

		default:
			opStates = start;

	}
}

} /* namespace testb */
