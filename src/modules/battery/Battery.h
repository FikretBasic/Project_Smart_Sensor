/*
 * Battery.h
 *
 *  Created on: 16 May 2018
 *      Author: its2016
 */

#ifndef MODULES_BATTERY_BATTERY_H_
#define MODULES_BATTERY_BATTERY_H_

#include "../Includes.h"

namespace pss {

const double MAX_BATTERY_VOLTAGE = 4.5;
const double BATTERY_CAPACITY = 0.1; //Farad
const double BATTERY_RESISTANCE = 800; //Ohm
const double MAX_BATTERY_CHARGE = MAX_BATTERY_VOLTAGE * BATTERY_CAPACITY; // Q Culomb
const double INIT_BATTERY_VOLTAGE = 2.2;
const double INIT_BATTERY_CHARGE = INIT_BATTERY_VOLTAGE * BATTERY_CAPACITY;

SC_MODULE(battery)
{
public:

	/*************/
	/*  Signals  */
	/*************/
	sc_in<bool> clk;
	sc_in<double> current;
	sc_out<bool> enable;
	sc_out<double> voltage;

	double currCharge;

	void work();

	SC_CTOR(battery)
	{
		//currCharge = INIT_BATTERY_CHARGE;
		currCharge = Configuration::instance()->getVCC() * BATTERY_CAPACITY;
		internalVoltage = 0;

		SC_THREAD(work);
		sensitive << clk.pos();
	};

private:

	void charge(double &currentLoad);
	void discharge(double &currentLoad);
	void updateState(double &currentLoad);

	double internalVoltage;
};

} /* namespace nfc */

#endif /* MODULES_BATTERY_BATTERY_H_ */
