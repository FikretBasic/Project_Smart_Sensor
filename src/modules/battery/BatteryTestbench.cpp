/*
 * BatteryTestbench.cpp
 *
 *  Created on: 16 May 2018
 *      Author: its2016
 */

#include "BatteryTestbench.h"

namespace testb {

void battery_testbench::runTest()
{
	while(true)
	{
		wait();

		if(batteryTest1)
		{
			runBatteryTest1();
		}
	}
}

void battery_testbench::runBatteryTest1()
{
	if(internalCounter++ < (300000 - 2) && enable.read() == true)
	{
		float loadVal = randomDouble(1.3, 1.4);
		load.write(loadVal);

		//cout << "Written current: " << loadVal << endl;
		//cout << "Voltage of the battery: " << voltage.read() << endl;
	}
	else
	{
		cout << "Time stamp: " << sc_time_stamp() << endl;
		int aVar;
		cin >> aVar;
		cout << "Voltage of the battery: " << voltage.read() << endl;
	}
}

double battery_testbench::randomDouble(double a, double b)
{
	double random = ((double) rand()) / (double) RAND_MAX;
	double diff = b - a;
	double r = random * diff;
    return a + r;
}

} /* namespace nfc */
