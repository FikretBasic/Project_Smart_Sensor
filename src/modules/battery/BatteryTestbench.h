/*
 * BatteryTestbench.h
 *
 *  Created on: 16 May 2018
 *      Author: its2016
 */

#ifndef MODULES_BATTERY_BATTERYTESTBENCH_H_
#define MODULES_BATTERY_BATTERYTESTBENCH_H_

#include "../Includes.h"

namespace testb {

SC_MODULE(battery_testbench)
{
public:

	sc_in<bool> clk;
	sc_in<bool> enable;
	sc_in<double> voltage;
	sc_out<double> load;

	bool batteryTest1;

	void runTest();

	SC_CTOR(battery_testbench)
	{
		internalCounter = 0;
		batteryTest1 = false;

		SC_THREAD(runTest);
		sensitive << clk.pos();
	};

private:
	double randomDouble(double a, double b);

	void runBatteryTest1();

	int internalCounter;
};


} /* namespace nfc */

#endif /* MODULES_BATTERY_BATTERYTESTBENCH_H_ */
