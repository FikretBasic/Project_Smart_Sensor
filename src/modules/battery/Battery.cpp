/*
 * Battery.cpp
 *
 *  Created on: 16 May 2018
 *      Author: its2016
 */

#include "Battery.h"

namespace pss {

void battery::work()
{
	enable.write(true);

	while(true)
	{
		wait();

		// nA -> A (Q/s)
		double currentLoad = current.read();
		transformUnitsStandard(currentLoad, SC_US);

		// first if charging, second if discharging
		if(currentLoad < 0)
		{
			charge(currentLoad);
		}
		else
		{
			discharge(currentLoad);
		}

		voltage.write(internalVoltage);
		// Q/s -> Q/ns
		transformUnitsStandard(currentLoad, SC_NS);

		updateState(currentLoad);
	}
}

void battery::charge(double &currentLoad)
{
	internalVoltage = 1.01 * MAX_BATTERY_VOLTAGE;
	double maxL = ((1.01 * MAX_BATTERY_VOLTAGE) - (currCharge / BATTERY_CAPACITY)) / BATTERY_RESISTANCE; // A
	if(currentLoad < -maxL)
		currentLoad = -maxL;
}

void battery::discharge(double &currentLoad)
{
	//cout << "Current Load: " << currentLoad << endl;
	internalVoltage = (currCharge / BATTERY_CAPACITY) - (currentLoad * BATTERY_RESISTANCE);
}

void battery::updateState(double &currentLoad)
{
	currCharge -= currentLoad;
	//lastload = l;

	//cout << "\nCurrent Charge: " << currCharge << " and load " << currentLoad << endl;

	if(currCharge < 0)
		currCharge = 0;

	if(currCharge > MAX_BATTERY_CHARGE)
		currCharge = MAX_BATTERY_CHARGE;

	if(internalVoltage <= 1.5)
		enable.write(false);
	else if(internalVoltage >= 2)
		enable.write(true);
}

} /* namespace nfc */
