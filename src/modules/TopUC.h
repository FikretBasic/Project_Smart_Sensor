/*
 * TopUC.h
 *
 *  Created on: 6 Oct 2018
 *      Author: its2016
 */

#ifndef MODULES_TOPUC_H_
#define MODULES_TOPUC_H_

#include "Includes.h"
#include "fram/Memory.h"
#include "nfc/NFC.h"
#include "security/SecurityChip.h"
#include "battery/Battery.h"
#include "battery/BatteryTestbench.h"
#include "quartz/NfcQuartz.h"
#include "quartz/SystemQuartz.h"
#include "microcontroller/Microcontroller.h"
#include "rf_scanner/RFScanner.h"

namespace pss {

struct topUC : public sc_module
{
public:

	SC_HAS_PROCESS(topUC);

	sc_in<bool> clock;

	void handleProgram();
	void calculateEnergy();

	void setTraceOptions(sc_trace_file *fp, std::queue<std::string> trace_options);

    topUC(const sc_module_name& name) : sc_module(name)
	{
		totalCurrentUsed = 0;

		// ------- Battery -------

		bat_module = new battery("BATTERY");
		bat_module->clk(clock);
		bat_module->enable(enable_bat);
		bat_module->current(load_bat);
		bat_module->voltage(voltage_bat);

		// ------- Quartz System-------

		sq = new quartz::systemQuartz("SYSTEM_QUARTZ");
		sq->enabled(enable_bat);
		sq->clock_in(clock);
		sq->clock_out(clk_out);

		// ------- Quartz NFC-------

		nfcClk = new quartz::nfcQuartz("NFC_Quartz");
		nfcClk->clock_in(clock);
		nfcClk->enabled(enable_bat);
		nfcClk->clock_out_input(signal_rf_clk_in);
		nfcClk->clock_out_output(signal_rf_clk_out);

		// ------- FRAM -------

		mem_module = new fram("FRAM");
		mem_module->scl(scl_mem);
		mem_module->sda_in(sda_in_mem);
		mem_module->sda_out(sda_out_mem);
		mem_module->a1(a1_mem);
		mem_module->a2(a2_mem);
		mem_module->wp(wp_mem);
		mem_module->enable(enable_mem);
		mem_module->energyOut(energyMem);

		// ------- RF Scanner Card -------

		rf_module = new rfscanner("RF_SCANNER");
		rf_module->rf_clk_in(signal_rf_clk_in);
		rf_module->rf_clk_out(signal_rf_clk_out);
		rf_module->enable_rf_in(enable_rf_out);
		rf_module->enable_rf_out(enable_rf_in);
		rf_module->antenna_in(antenna_out);
		rf_module->antenna_out(antenna_in);

		// ------- NFC -------
		nfc_module = new nfc_tag("NFC_TAG");
		nfc_module->scl(scl_nfc);
		nfc_module->sda_in(sda_in_nfc);
		nfc_module->sda_out(sda_out_nfc);
		nfc_module->enable(enable_nfc);
		nfc_module->gpo(gpo_nfc);
		nfc_module->v_eh(v_eh_nfc);
		nfc_module->rf_clk_in(signal_rf_clk_in);
		nfc_module->rf_clk_out(signal_rf_clk_out);
		nfc_module->enable_rf_in(enable_rf_in);
		nfc_module->enable_rf_out(enable_rf_out);
		nfc_module->antenna_in(antenna_in);
		nfc_module->antenna_out(antenna_out);
		nfc_module->currentOutI2C(nfc_currentOut_I2C);
		nfc_module->currentOutRF(nfc_currentOut_RF);

		// Security
		sec_module = new security("SECURITY_CHIP");
		sec_module->scl(scl_sec);
		sec_module->sda_in(sda_in_sec);
		sec_module->sda_out(sda_out_sec);
		sec_module->enable(enable_sec);
		sec_module->currentOut(sec_currentOut);
		sec_module->operate(enable_bat);

		// ------- Microcontoller -------

		uc = new microcontroller("MICROCONTROLLER");
		uc->clk(clk_out);
		uc->currentOutUC(currentOutUC);

		uc->a1_mem(a1_mem);
		uc->a2_mem(a2_mem);
		uc->wp_mem(wp_mem);
		uc->scl_mem(scl_mem);
		uc->sda_in_mem(sda_out_mem);
		uc->sda_out_mem(sda_in_mem);
		uc->enable_mem(enable_mem);

		uc->scl_nfc(scl_nfc);
		uc->sda_in_nfc(sda_out_nfc);
		uc->sda_out_nfc(sda_in_nfc);
		uc->enable_nfc(enable_nfc);
		uc->gpo(gpo_nfc);
		uc->v_eh(v_eh_nfc);

		uc->scl_sec(scl_sec);
		uc->sda_in_sec(sda_out_sec);
		uc->sda_out_sec(sda_in_sec);
		uc->enable_sec(enable_sec);

		// ------- THREADS -------

		SC_THREAD(handleProgram);
		sensitive << clock.pos();

		SC_THREAD(calculateEnergy);
		sensitive << clock.pos();
	};

//private:

	// Battery
	battery *bat_module;

	sc_signal<bool> enable_bat;
	sc_signal<double> load_bat;
	sc_signal<double> voltage_bat;

	// Clock-Rate control
	quartz::systemQuartz *sq;

	sc_signal<bool> clk_out;
	sc_signal<bool> enb;

	// Microcontroller
	microcontroller *uc;

	sc_signal<double> currentOutUC;

	// Memory
	fram *mem_module;

	sc_signal<bool> scl_mem;
	sc_signal<bool> sda_in_mem;
	sc_signal<bool> sda_out_mem;
	sc_signal<bool> a1_mem;
	sc_signal<bool> a2_mem;
	sc_signal<bool> wp_mem;
	sc_signal<bool> enable_mem;
	sc_signal<double> energyMem;

	// NFC - Clock-Rate controll
	quartz::nfcQuartz *nfcClk;
	sc_signal<bool> signal_rf_clk_in;
	sc_signal<bool> signal_rf_clk_out;

	// RF Scanner Card
	rfscanner *rf_module;

    // NFC
	nfc_tag *nfc_module;

	sc_signal<bool> scl_nfc;
	sc_signal<bool> sda_in_nfc;
	sc_signal<bool> sda_out_nfc;
	sc_signal<bool> enable_nfc;
	sc_signal<bool> gpo_nfc;
	sc_signal<double> v_eh_nfc;
	sc_signal<double> energyNFC;
	sc_signal<bool> enable_rf_in;
	sc_signal<bool> enable_rf_out;
	sc_signal<bool> antenna_in;
	sc_signal<bool> antenna_out;
	sc_signal<double> nfc_currentOut_I2C;
	sc_signal<double> nfc_currentOut_RF;

	// Security
	security *sec_module;

	sc_signal<bool> scl_sec;
	sc_signal<bool> sda_in_sec;
	sc_signal<bool> sda_out_sec;
	sc_signal<bool> enable_sec;
	sc_signal<double> sec_currentOut;

	// Overall Current Used
	double totalCurrentUsed;
};

} /* namespace testb */

#endif /* MODULES_TOPUC_H_ */
