/*
 * Includes.cpp
 *
 *  Created on: 18 Apr 2018
 *      Author: its2016
 */

#include "Includes.h"

int n_cycles;
sc_core::sc_time_unit unitType;

// 48Mhz
const size_t FREQENCY_MAIN_MCU = 48 * 1000000;

// 100kHz
const size_t FREQUENCY_STANDARD_MODE = 100 * 1000;

// 1MHz
const size_t FREQUENCY_FAST_MODE_PLUS = FREQUENCY_STANDARD_MODE/10;

// 3.4MHz
const size_t FREQUENCY_HIGH_SPEED_MODE = FREQUENCY_FAST_MODE_PLUS/3.4;

// The frequency set for FRAM
size_t FREQUENCY_FRAM_SCL = FREQUENCY_FAST_MODE_PLUS;


/*
 * NFC elements
 * */

// incoming data with 1/256 pulse coding mode
size_t BitRate_1600bit;

// output data with low data rate mode
size_t BitRate_6600bit;

// incoming data with 1/4 pulse coding mode (high data rate mode for output)
size_t BitRate_26Kbit;

// bit input rate at the current RF (NFC) device
size_t RF_BIT_INPUT_RATE;

// bit output rate at the current RF (NFC) device
size_t RF_BIT_OUTPUT_RATE;

// function to calculate the bit rate based on the global clock on the runtime
void calcBitRate()
{
	size_t divider;

	switch(unitType)
	{
		case sc_core::SC_MS:
			divider = 1000;
			break;

		case sc_core::SC_US:
			divider = 1000000;
			break;

		case sc_core::SC_NS:
			divider = 1000000000;
			break;

		case sc_core::SC_PS:
			divider = 1000000000000;
			break;

		default:
			divider = 1000000000;
	}

	BitRate_1600bit = divider/1600;
	BitRate_6600bit = divider/6600;
	BitRate_26Kbit = divider/26000;

	// for testing and synchronisation purposes, they can be changed
	RF_BIT_INPUT_RATE = BitRate_26Kbit;
	RF_BIT_OUTPUT_RATE = BitRate_26Kbit;
}

/* --------------
 * NFC elements
 * */


/*
 * Functions
 * */

sc_core::sc_time_unit returnSimulationUnit(const char* simInput)
{
	if (strcmp(simInput, "mili") != 0)
	{
		unitType = sc_core::SC_MS;
	}
	else if (strcmp(simInput, "micro") != 0)
	{
		unitType = sc_core::SC_US;
	}
	else if (strcmp(simInput, "nano") != 0)
	{
		unitType = sc_core::SC_NS;
	}
	else if (strcmp(simInput, "pico") != 0)
	{
		unitType = sc_core::SC_PS;
	}
	else
	{
		unitType = sc_core::SC_NS;
	}

	return unitType;
}


/* --------------
 * Functions
 * */
