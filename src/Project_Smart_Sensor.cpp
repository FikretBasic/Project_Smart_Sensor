//============================================================================
// Name        : Project_Smart_Sensor.cpp
// Author      : Fikret
// Version     :
// Copyright   : &copy Fikret Basic - TU Graz All Rights Reserved
// Description : Main function of the Smart Sensor SymPar simulation model
//============================================================================

#include "modules/Includes.h"
#include "modules/Top.h"
#include "modules/TopUC.h"

int sc_main(int argc, char *argv[])
{
	//--------------------------------------------------
	//	definition of cycle number
	//--------------------------------------------------
	n_cycles = 1000*100000;

	if (argc == 2)
	{
		unitType = returnSimulationUnit(argv[1]);
		cout << "default cycles = " << n_cycles << "\n";
	}
	else if(argc == 3)
	{
		unitType = returnSimulationUnit(argv[1]);
		n_cycles = atoi(argv[2]);
	}
	else
	{
		unitType = SC_NS;
		cout << "default cycles = " << n_cycles << "\n";
	}

	//--------------------------------------------------
	//	Calculates the current bit rate based on the input clock
	//--------------------------------------------------
	calcBitRate();

	//--------------------------------------------------
	// Defines the main input clock
	//--------------------------------------------------
	sc_clock TestClk("TestClock", 1, unitType, 0.5);

	//--------------------------------------------------
	// Defining the "turn-off" signal for the control of the simulation type
	//--------------------------------------------------
	sc_signal<bool> falseSignal;
	falseSignal.write(false);

	//--------------------------------------------------
	// Creating the instance of the top modules
	//--------------------------------------------------
	pss::top *tp_test = new pss::top("SENSOR_TOP_TEST");
	pss::topUC *tp = new pss::topUC("SENSOR_TOP_APP");

	//--------------------------------------------------
	// Defining which simulation type (test bench or the standard program) should be run
	//--------------------------------------------------
	if(Configuration::instance()->getSimulation_type() == 1)
	{
		tp->clock(falseSignal);
		tp_test->clock(TestClk);
		tp_test->setTestSettings(Configuration::instance()->getAct_test_benches());
		tp_test->setTraceOptions(SimulationOutput::instance()->getTraceFile(),
				Configuration::instance()->getTest_bench_trace());
	}
	else
	{
		tp_test->clock(falseSignal);
		tp->clock(TestClk);
		SimulationOutput::instance()->setSimOutputFile(2, Configuration::instance()->getUCPRogram());
		tp->setTraceOptions(SimulationOutput::instance()->getTraceFile(),
				Configuration::instance()->getUC_program_trace());
	}

	//--------------------------------------------------
	// starting the simulation
	//--------------------------------------------------
	sc_start(n_cycles, unitType);

	//--------------------------------------------------
	// displaying some of the simulation metrics
	//--------------------------------------------------
	if(Configuration::instance()->getSimulation_type() == 1)
	{
		cout << "\nCurrent charge of the battery: " << tp_test->bat_module->currCharge << endl;
		cout << "Battery voltage: " << tp_test->voltage_bat << " V" << endl;
	}
	else
	{
		cout << "\nCurrent charge of the battery: " << tp->bat_module->currCharge << endl;
		cout << "Battery voltage: " << tp->voltage_bat << " V" << endl;
	}
	cout << "Simulation time: " << sc_time_stamp() << endl;

	//--------------------------------------------------
	// code and program finalisation
	//--------------------------------------------------
	cout << "\nPress the key to end the simulation window..." << endl;
	cin.get();

	delete tp;
	delete tp_test;

	return 0;
}
